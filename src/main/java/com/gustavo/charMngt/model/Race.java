package com.gustavo.charMngt.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Entity representing the Race of a character
 * 
 * @author Gustavo
 *
 */
@Entity
@Table(name = "race")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Race extends IndexEntity {
  /**
   * The race name is the natural ID of the entity
   */
  @NotBlank(message = "{name.not.blank}")
  @Column(name = "race_type")
  private String race;

  public Race() { /* JPA */ }

  public Race(Integer id, String race) {
    super.setId(id);
    this.race = race;
  }

  public String getRace() {
    return race;
  }

  public void setRace(String race) {
    this.race = race;
  }

  @Override
  public String toString() {
    return "[" + super.getId() + " " + getRace() + "]";
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(race);
  }

  @Override
  public boolean equals(Object other) {
    if (other == null) {
      return false;
    }
    if (!(other instanceof Race)) {
      return false;
    }
    Race that = (Race) other;
    for (int i = 0; i < getSignificantFields().length; i++) {
      if (!Objects.equals(this.getSignificantFields()[i], that.getSignificantFields()[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   * @return an array with all significant fields
   */
  private Object[] getSignificantFields() {
    return new Object[] {race};
  }

}
