package com.gustavo.charMngt.model;

import java.util.Objects;

import javax.persistence.Embeddable;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Embeddable Bean that represents the six attributes of all {@link Character}s
 * 
 * @author Gustavo
 *
 */
@Embeddable
public class Attribute {

  @Min(value = 0)
  @Max(value = 30)
  @NotNull(message = "{number.not.null}")
  private Integer strength;

  @Min(value = 0)
  @Max(value = 30)
  @NotNull(message = "{number.not.null}")
  private Integer dexterity;

  @Min(value = 0)
  @Max(value = 30)
  @NotNull(message = "{number.not.null}")
  private Integer constitution;

  @Min(value = 0)
  @Max(value = 30)
  @NotNull(message = "{number.not.null}")
  private Integer intelligence;

  @NotNull(message = "{number.not.null}")
  @Min(value = 0)
  @Max(value = 30)
  private Integer wisdom;

  @NotNull(message = "{number.not.null}")
  @Min(value = 0)
  @Max(value = 30)
  private Integer charisma;

  protected Attribute() { /* JPA Constructor */ }

  public Attribute(Integer strength, Integer dexterity, Integer constitution, Integer intelligence,
      Integer wisdom, Integer charisma) {
    this.strength = strength;
    this.dexterity = dexterity;
    this.constitution = constitution;
    this.intelligence = intelligence;
    this.wisdom = wisdom;
    this.charisma = charisma;
  }

  public Integer getStrength() {
    return strength;
  }

  public void setStrength(Integer strength) {
    this.strength = strength;
  }

  public Integer getDexterity() {
    return dexterity;
  }

  public void setDexterity(Integer dexterity) {
    this.dexterity = dexterity;
  }

  public Integer getConstitution() {
    return constitution;
  }

  public void setConstitution(Integer constitution) {
    this.constitution = constitution;
  }

  public Integer getIntelligence() {
    return intelligence;
  }

  public void setIntelligence(Integer inteligence) {
    this.intelligence = inteligence;
  }

  public Integer getWisdom() {
    return wisdom;
  }

  public void setWisdom(Integer wisdom) {
    this.wisdom = wisdom;
  }

  public Integer getCharisma() {
    return charisma;
  }

  public void setCharisma(Integer charisma) {
    this.charisma = charisma;
  }

  @Override
  public String toString() {
    StringBuffer builder = new StringBuffer();
    builder.append("Char [");
    builder.append("STR " + strength); // str
    builder.append(", DEX " + dexterity); // dex
    builder.append(", CON " + constitution); // con
    builder.append(", INT " + intelligence); // int
    builder.append(", WIS " + wisdom); // wis
    builder.append(", CHAR " + charisma); // char
    builder.append("]");
    return builder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(strength, dexterity, constitution, intelligence, wisdom, charisma);
  }

  @Override
  public boolean equals(Object other) {
    if (other == null) {
      return false;
    }
    if (!(other instanceof Attribute)) {
      return false;
    }
    Attribute that = (Attribute) other;
    for (int i = 0; i < getSignificantFields().length; i++) {
      if (!Objects.equals(this.getSignificantFields()[i], that.getSignificantFields()[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   * @return an array with all significant fields
   */
  private Object[] getSignificantFields() {
    return new Object[] {strength, dexterity, constitution, intelligence, wisdom, charisma};
  }
}
