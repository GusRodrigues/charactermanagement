package com.gustavo.charMngt.model;

import java.util.Optional;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gustavo.charMngt.model.validation.OnCreate;
import com.gustavo.charMngt.model.validation.OnUpdate;

/**
 * Class representing an entity that uses {@link Integer} as index
 * 
 * @author Gustavo
 *
 */
@MappedSuperclass
public class IndexEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  @Null(groups = OnCreate.class, message = "{id.create}")
  @NotNull(groups = OnUpdate.class, message = "{id.update}")
  private Integer id;

  public Integer getId() {
    if (Optional.ofNullable(id).isEmpty()) {
      return null;
    }
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

}
