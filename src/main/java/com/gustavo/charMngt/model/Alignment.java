package com.gustavo.charMngt.model;

import java.util.Objects;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

/**
 * Represents the Alignment of a character There are 9 possible combinations for the Alignment
 * 
 * @author Gustavo
 *
 */
@Entity
@Table(name = "alignment")
public class Alignment extends IndexEntity {

  @Column(name = "lawful", nullable = true, columnDefinition = "TINYINT(1)")
  private Boolean lawful;

  @Column(name = "good", nullable = true, columnDefinition = "TINYINT(1)")
  private Boolean good;

  /**
   * The name of the alignment The name is the natural ID of an Alignment
   */
  @NotBlank(message = "{name.not.blank}")
  @Column(name = "nickname")
  private String name;

  protected Alignment() {/* Jpa */ }

  public Alignment(Integer id, Boolean lawful, Boolean good, String name) {
    super.setId(id);
    this.lawful = lawful;
    this.good = good;
    this.name = name;
  }

  public Boolean getLawful() {
    if (Optional.ofNullable(lawful).isEmpty()) {
      return null;
    } else {
      return lawful;
    }
  }

  public void setLawfood(Boolean lawful) {
    this.lawful = lawful;
  }

  public Boolean getGood() {
    if (Optional.ofNullable(good).isEmpty()) {
      return null;
    } else {
      return good;
    }
  }

  public void setGood(Boolean good) {
    this.good = good;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    String isLawful = (this.lawful == null) ? "Neutral" : (this.lawful) ? "Lawfull" : "Chaotic";
    String isGood = (this.good == null) ? "Neutral" : (this.good) ? "Good" : "Evil";

    return "[" + this.name + ": " + isLawful + " " + isGood + "]";
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(name);
  }

  @Override
  public boolean equals(Object other) {
    if (other == null) {
      return false;
    }
    if (!(other instanceof Alignment)) {
      return false;
    }
    Alignment that = (Alignment) other;
    for (int i = 0; i < getSignificantFields().length; i++) {
      if (!Objects.equals(this.getSignificantFields()[i], that.getSignificantFields()[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   * @return an array with all significant fields
   */
  private Object[] getSignificantFields() {
    return new Object[] {lawful, good, name};
  }
}
