package com.gustavo.charMngt.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.springframework.hateoas.server.core.Relation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Entity representing the Job of a character
 * 
 * @author Gustavo
 *
 */
@Entity
@Relation(value = "class", itemRelation = "class", collectionRelation = "classes")
@Table(name = "job")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Job extends IndexEntity {

  /**
   * Name is also the natural ID of the class
   */
  @NotBlank(message = "{name.not.blank}")
  @Column(name = "description")
  private String name;

  protected Job() {/* JPA */}

  public Job(Integer id, String name) {
    super.setId(id);
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "[#" + super.getId() + " " + getName() + "]";
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(name);
  }

  @Override
  public boolean equals(Object other) {
    if (other == null) {
      return false;
    }
    if (!(other instanceof Job)) {
      return false;
    }
    Job that = (Job) other;
    for (int i = 0; i < getSignificantFields().length; i++) {
      if (!Objects.equals(this.getSignificantFields()[i], that.getSignificantFields()[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   * @return an array with all significant fields
   */
  private Object[] getSignificantFields() {
    return new Object[] {name};
  }
}
