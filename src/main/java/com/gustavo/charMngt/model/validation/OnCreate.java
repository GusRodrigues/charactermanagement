package com.gustavo.charMngt.model.validation;

/**
 * Marker interface for ID Validation on creation
 * 
 * @author Gustavo
 *
 */
public interface OnCreate {
}
