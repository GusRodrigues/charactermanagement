package com.gustavo.charMngt.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Main Entity of the project
 * 
 * @author Gustavo
 *
 */
@Entity
@Table(name = "game_char")
public class Character extends IndexEntity {

  /*
   * Name, gender, and race are the Natural ID for identification of a character.
   */

  @NotBlank(message = "{name.not.blank}")
  @Column(name = "char_name")
  private String name;

  @NotNull(message = "{param.not.null}")
  @Column(name = "gender", columnDefinition = "TINYINT(1)")
  private Boolean gender;

  @NotNull(message = "{param.not.null}")
  @Column(name = "playable", columnDefinition = "TINYINT(1)")
  private Boolean playable;

  @NotNull(message = "{param.not.null}")
  @Column(name = "experience_point")
  private Integer experiencePoint;

  @Embedded
  @Valid
  @NotNull(message = "{param.not.null}")
  private Attribute attributes;

  @ManyToOne
  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  @NotNull(message = "{param.not.null}")
  @JoinColumn(name = "race_id", nullable = false,
      foreignKey = @ForeignKey(name = "fk_character_race"))
  private Race race;

  @ManyToOne
  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  @NotNull(message = "{param.not.null}")
  @JoinColumn(name = "alignment_id", nullable = false,
      foreignKey = @ForeignKey(name = "fk_character_alignment"))
  private Alignment alignment;

  @ManyToOne
  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  @NotNull(message = "{param.not.null}")
  @JoinColumn(name = "job_id", nullable = false,
      foreignKey = @ForeignKey(name = "fk_character_job"))
  private Job job;

  @ManyToOne
  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  @NotNull(message = "{param.not.null}")
  @JoinColumn(name = "size_id", nullable = false,
      foreignKey = @ForeignKey(name = "fk_character_size"))
  private Size size;


  public Character() {}

  public Race getRace() {
    return race;
  }

  public void setRace(Race race) {
    this.race = race;
  }

  public Alignment getAlignment() {
    return alignment;
  }

  public void setAlignment(Alignment alignment) {
    this.alignment = alignment;
  }

  public Job getJob() {
    return job;
  }

  public void setJob(Job job) {
    this.job = job;
  }

  public Size getSize() {
    return size;
  }

  public void setSize(Size size) {
    this.size = size;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Boolean getPlayable() {
    return playable;
  }

  public void setPlayable(Boolean playable) {
    this.playable = playable;
  }

  public Integer getExperiencePoint() {
    return experiencePoint;
  }

  public void setExperiencePoint(Integer experiencePoint) {
    this.experiencePoint = experiencePoint;
  }

  public Boolean getGender() {
    return gender;
  }

  public void setGender(Boolean gender) {
    this.gender = gender;
  }

  public @Valid Attribute getAttributes() {
    return attributes;
  }

  public void setAttributes(@Valid Attribute attribute) {
    this.attributes = attribute;
  }

  @Override
  public String toString() {
    StringBuffer builder = new StringBuffer();
    builder.append("Char [");
    builder.append("#" + super.getId() + ", ");
    builder.append(name + ", "); // name
    builder.append(race.getRace() + ", "); // race
    builder.append((gender) ? "Female, " : "Male, "); // gender
    builder.append((playable) ? "Playable, " : "NPC, "); // playable
    builder.append(job.getName() + ", "); // the class
    builder.append(experiencePoint.toString() + ", "); // the xp
    builder.append(alignment.getName() + ", "); // the alignment
    builder.append(size.getName()); // the size
    builder.append("]\n");
    builder.append(attributes);
    return builder.toString();
  }


  @Override
  public int hashCode() {
    return Objects.hash(name, gender, race);
  }

  @Override
  public boolean equals(Object other) {
    if (other == null) {
      return false;
    }
    if (!(other instanceof Character)) {
      return false;
    }
    Character that = (Character) other;
    for (int i = 0; i < getSignificantFields().length; i++) {
      if (!Objects.equals(this.getSignificantFields()[i], that.getSignificantFields()[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   * @return an array with all significant fields
   */
  private Object[] getSignificantFields() {
    return new Object[] {name, gender, race};
  }
}
