package com.gustavo.charMngt.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Entity representing the Size of a character
 * 
 * @author Gustavo
 *
 */
@Entity
@Table(name = "size")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Size extends IndexEntity {
  /**
   * Race is the natural ID of the entity
   */
  @NotBlank(message = "{name.not.blank}")
  @Column(name = "description")
  private String name;

  @Min(value = 3)
  @NotNull(message = "{number.not.null}")
  @Column(name = "hit_dice")
  private Integer hitDice;

  protected Size() { /* JPA Constructor */ }

  public Size(Integer id, String name, Integer dice) {
    super.setId(id);
    this.name = name;
    this.hitDice = dice;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getHitDice() {
    return hitDice;
  }

  public void setHitDice(Integer hitDice) {
    this.hitDice = hitDice;
  }

  @Override
  public int hashCode() {
    return Objects.hash(hitDice, name);
  }

  @Override
  public boolean equals(Object other) {
    if (other == null) {
      return false;
    }
    if (!(other instanceof Size)) {
      return false;
    }
    Size that = (Size) other;
    for (int i = 0; i < getSignificantFields().length; i++) {
      if (!Objects.equals(this.getSignificantFields()[i], that.getSignificantFields()[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   * @return an array with all significant fields
   */
  private Object[] getSignificantFields() {
    return new Object[] {hitDice, name};
  }
}
