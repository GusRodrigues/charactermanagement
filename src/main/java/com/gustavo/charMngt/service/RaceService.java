package com.gustavo.charMngt.service;

import org.springframework.stereotype.Service;

import com.gustavo.charMngt.model.Race;
import com.gustavo.charMngt.repository.RaceRepository;
import com.gustavo.charMngt.service.exceptions.model.EntityNotFoundException;

/**
 * Bean responsible to expose all methods available to operate in the {@link Race} in the
 * persistence unit
 * 
 * @author Gustavo
 *
 */
@Service
public final class RaceService extends CRUDService<Race> {

  private final RaceRepository repository;

  public RaceService(RaceRepository repository) {
    super(repository);
    this.repository = repository;
  }

  /**
   * 
   * @param race name of the {@link Race} to be searched
   * @return the {@link Race} with that name, if any.
   */
  @Override
  public Race findByName(String race) {
    return repository.findByRaceIgnoreCase(race).orElseThrow(() -> new EntityNotFoundException(race));
  }
}
