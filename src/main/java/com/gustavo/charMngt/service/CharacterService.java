package com.gustavo.charMngt.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.gustavo.charMngt.model.Alignment;
import com.gustavo.charMngt.model.Character;
import com.gustavo.charMngt.model.Job;
import com.gustavo.charMngt.model.Race;
import com.gustavo.charMngt.model.Size;
import com.gustavo.charMngt.repository.CharacterRepository;
import com.gustavo.charMngt.service.exceptions.InvalidSearchException;

/**
 * Bean exposing the repository methods available to use.
 * 
 * @author Gustavo
 *
 */
@Service
public final class CharacterService extends CRUDService<Character> {

  private final CharacterRepository repository;

  public CharacterService(CharacterRepository repository) {
    super(repository);
    this.repository = repository;
  }

  // TODO implement get the Alignment, Job, Race and Size of An ID

  /**
   * Search for {@link Character} with that {@link Race} ID or Name The search will always use ID
   * before name
   * 
   * @param id to be searched
   * @param name to be searched
   * @return the List of {@link Character} holding that ID or name
   */
  public List<Character> findByRace(Optional<Integer> id, Optional<String> name) {
    if (id.isEmpty() && name.isEmpty()) {
      throw new InvalidSearchException("Character by race");
    }
    if (id.isPresent()) {
      return repository.findAllByRaceId(id.get());
    } else {
      return repository.findAllByRaceRace(name.get());
    }
  }

  /**
   * Search for {@link Character} with that {@link Job} ID or Name The search will always use ID
   * before name
   * 
   * @param id of the {@link Job} to be searched
   * @param name of the {@link Job} to be searched
   * @return the List of {@link Character} holding that ID or name
   */
  public List<Character> findByJob(Optional<Integer> id, Optional<String> name) {
    if (id.isEmpty() && name.isEmpty()) {
      throw new InvalidSearchException("Character by class");
    }
    if (id.isPresent()) {
      return repository.findAllByJobId(id.get());
    } else {
      return repository.findAllByJobNameIgnoreCase(name.get());
    }
  }

  /**
   * Search for {@link Character} with that {@link Size} ID or Name The search will always use ID
   * before name
   * 
   * @param id of the {@link Size} to be searched
   * @param name of the {@link Size} to be searched
   * @param hitDice of the {@link Size} to be searched
   * @return the List of {@link Character} holding that ID, or name, or hitDice
   */
  public List<Character> findBySize(Optional<Integer> id, Optional<String> name,
      Optional<Integer> hitDice) {
    if (id.isEmpty() && name.isEmpty() && hitDice.isEmpty()) {
      throw new InvalidSearchException("Character by size");
    }
    if (id.isPresent()) {
      return repository.findAllBySizeId(id.get());
    } else if (name.isPresent()) {
      return repository.findAllBySizeNameIgnoreCase(name.get());
    } else {
      return repository.findAllBySizeHitDice(hitDice.get());
    }
  }

  /**
   * Search for {@link Character} with that {@link Alignment} ID or Name the {@link Alignment} or if
   * it is Lawful or Good.
   * 
   * @param id of the {@link Alignment} to be searched
   * @param name of the {@link Alignment} to be searched
   * @return the List of {@link Character} holding that ID, or name, or if lawful, or if good
   */
  public List<Character> findByAlignment(Optional<Integer> id, Optional<String> name) {
    if (id.isEmpty() && name.isEmpty()) {
      throw new InvalidSearchException("Character by alignment");
    }
    if (id.isPresent()) {
      return repository.findAllByAlignmentId(id.get());
    } else {
      return repository.findAllByAlignmentNameIgnoreCase(name.get());
    }
  }

  @Override
  public Character findByName(String name) {
    throw new RuntimeException("Not implemented");
  }

}
