package com.gustavo.charMngt.service;

import org.springframework.stereotype.Service;

import com.gustavo.charMngt.model.Size;
import com.gustavo.charMngt.repository.SizeRepository;
import com.gustavo.charMngt.service.exceptions.model.EntityNotFoundException;

/**
 * Bean responsible to expose all methods available to operate on the {@link Size} entity in the
 * persistence unit.
 * 
 * @author Gustavo
 *
 */
@Service
public final class SizeService extends CRUDService<Size> {

  private final SizeRepository sizeRepository;

  public SizeService(SizeRepository repository) {
    super(repository);
    this.sizeRepository = repository;
  }

  /**
   * Finds an size by its name
   * 
   * @param name of the size
   * @return {@link Size} with that name
   * @throws SizeNotFoundException if no Size has the name supplied
   */
  @Override
  public Size findByName(String name) {
    return sizeRepository.findByNameIgnoreCase(name)
        .orElseThrow(() -> new EntityNotFoundException(name));
  }
}
