package com.gustavo.charMngt.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.gustavo.charMngt.service.exceptions.model.EntityNotFoundException;

abstract class CRUDService<T> {

  public final JpaRepository<T, Integer> repository;

  public CRUDService(JpaRepository<T, Integer> repository) {
    this.repository = repository;
  }

  /**
   * 
   * @return a {@link Page} of all instances of the type.
   */
  public Page<T> findAll(Pageable pageable) {
    return repository.findAll(pageable);
  }

  /**
   * Finds an Entity by its ID
   * 
   * @param id to be searched
   * @return the type with that Id
   * 
   */
  public T findById(int id) {
    return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(id));
  }

  /**
   * Finds an entity by its name
   * 
   * @param name as in the parameter of the Type
   * @return An entity with that type name with that name
   * 
   */
  public abstract T findByName(String name);

  /**
   * 
   * @return the number of entities saved
   */
  public int count() {
    return (int) repository.count();
  }

  /**
   * Saves a new entity
   * 
   * @param entity to be saved
   * @return the saved version of the supplied entity
   */
  public T save(T entity) {
    return repository.save(entity);
  }

  /**
   * Update or create a new entity in the persistence unit.
   * 
   * @param entity to be updated
   * @return the updated version of the entity
   */
  public T update(T entity) {
    return repository.save(entity);
  }

  /**
   * Check if entity exists
   * 
   * @param id to check if exists
   * @return true iff exists.
   */
  public boolean contains(Integer id) {
    return repository.existsById(id);
  }

  /**
   * Deletes an entity. Cannot be null.
   * 
   * @param entity to be deleted
   */
  public void delete(T entity) {
    repository.delete(entity);
  }
}
