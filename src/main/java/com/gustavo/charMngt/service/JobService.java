package com.gustavo.charMngt.service;

import org.springframework.stereotype.Service;

import com.gustavo.charMngt.model.Job;
import com.gustavo.charMngt.repository.JobRepository;
import com.gustavo.charMngt.service.exceptions.model.EntityNotFoundException;

/**
 * Bean responsible to expose the {@link Job} Repository methods available
 * 
 * @author Gustavo
 *
 */
@Service
public final class JobService extends CRUDService<Job> {

  private final JobRepository repository;

  public JobService(JobRepository repository) {
    super(repository);
    this.repository = repository;
  }

  /**
   * 
   * @param name of the {@link Job} to be searched. Cannot be null or whitespace
   * @return the {@link Job} with that name.
   */
  @Override
  public Job findByName(String name) {
    return repository.findByNameIgnoreCase(name)
        .orElseThrow(() -> new EntityNotFoundException(name));
  }
}
