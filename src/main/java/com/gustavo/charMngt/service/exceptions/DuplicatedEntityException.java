package com.gustavo.charMngt.service.exceptions;

public class DuplicatedEntityException extends RuntimeException {
  private String entity;

  public DuplicatedEntityException(String entity) {
    super(String.format("A representation of this %s already exists.", entity));
    this.entity = entity;
  }

  public String getEntity() {
    return this.entity;
  }
}
