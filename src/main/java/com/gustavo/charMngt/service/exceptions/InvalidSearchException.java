package com.gustavo.charMngt.service.exceptions;

public class InvalidSearchException extends RuntimeException {
  /**
   * What was the search that is invalid
   * 
   * @param search
   */
  public InvalidSearchException(String search) {
    super(String.format("Invalid arguments provided to search for %s.", search));
  }

}
