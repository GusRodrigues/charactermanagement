package com.gustavo.charMngt.service.exceptions.model;

public class EntityNotFoundException extends RuntimeException {
  public EntityNotFoundException(Integer id) {
    super(String.format("Couldn't find the entity of id #%d", id));
  }

  public EntityNotFoundException(String entity) {
    super(String.format("Couldn't find the entity with name %s", entity));
  }
}
