package com.gustavo.charMngt.service;

import org.springframework.stereotype.Service;

import com.gustavo.charMngt.model.Alignment;
import com.gustavo.charMngt.repository.AlignmentRepository;
import com.gustavo.charMngt.service.exceptions.model.EntityNotFoundException;

/**
 * Bean exposing methods associated with the JPA Repository of the entity Aligment
 * 
 * @author Gustavo
 *
 */
@Service
public final class AlignmentService extends CRUDService<Alignment> {

  private final AlignmentRepository repository;

  public AlignmentService(AlignmentRepository repository) {
    super(repository);
    this.repository = repository;
  }

  /**
   * Fetch an Alignment by name, if existing. Throws {@link AlignmentNotFoundException} if nothing
   * is found
   * 
   * @param name of the alignment
   * @return the alignment, if existing
   */
  @Override
  public Alignment findByName(String name) {
    return repository.findByNameIgnoreCase(name)
        .orElseThrow(() -> new EntityNotFoundException(name));
  }
}
