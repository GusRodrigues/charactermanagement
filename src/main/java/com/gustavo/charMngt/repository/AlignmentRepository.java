package com.gustavo.charMngt.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gustavo.charMngt.model.Alignment;

/**
 * Spring Data bean for Aligment entity
 * 
 * @author Gustavo
 *
 */
@Repository
public interface AlignmentRepository extends JpaRepository<Alignment, Integer> {
  /**
   * Search for an {@link Alignment} by Name ignoring case
   * 
   * @param name of the {@link Alignment} to be searched
   * @return {@link Optional} with the {@link Alignment}, if any.
   */
  Optional<Alignment> findByNameIgnoreCase(String name);

}
