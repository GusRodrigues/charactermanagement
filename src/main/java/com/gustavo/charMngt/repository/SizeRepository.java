package com.gustavo.charMngt.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gustavo.charMngt.model.Size;

/**
 * SPRING DATA Repository for the Size entity
 * 
 * @author Gustavo
 *
 */
@Repository
public interface SizeRepository extends JpaRepository<Size, Integer> {
  /**
   * @param name of the {@link Size} to be searched
   * @return {@link Optional} with the {@link Size} with that name, if any.
   */
  Optional<Size> findByNameIgnoreCase(String name);
}
