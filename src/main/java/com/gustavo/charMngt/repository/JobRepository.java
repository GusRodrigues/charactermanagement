package com.gustavo.charMngt.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gustavo.charMngt.model.Job;

/**
 * SPRING DATA JPARepository managing the {@link Job} in the persistence unit.
 * 
 * @author Gustavo
 *
 */
@Repository
public interface JobRepository extends JpaRepository<Job, Integer> {
  /**
   * 
   * @param name of the {@link Job} to be searched
   * @return {@link Optional} holding the {@link Job} with that name, if any
   */
  Optional<Job> findByNameIgnoreCase(String name);
}
