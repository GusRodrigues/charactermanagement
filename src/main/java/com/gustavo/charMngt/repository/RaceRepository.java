package com.gustavo.charMngt.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gustavo.charMngt.model.Race;

@Repository
public interface RaceRepository extends JpaRepository<Race, Integer> {
  /**
   * 
   * @param race name of the {@link Race}. Cannot be null or whitespace
   * @return {@link Optional} with the given race with that name, if any.
   */
  Optional<Race> findByRaceIgnoreCase(String race);
}
