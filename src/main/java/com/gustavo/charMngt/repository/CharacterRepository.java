package com.gustavo.charMngt.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.gustavo.charMngt.model.Character;

/**
 * Spring Data Repository for all characters in the persistence unit
 * 
 * @author Gustavo
 *
 */
@Repository
public interface CharacterRepository extends JpaRepository<Character, Integer> {

  @Override
  @Query("SELECT c FROM Character c JOIN FETCH c.race race JOIN FETCH c.alignment alignment "
      + "JOIN FETCH c.size size JOIN FETCH c.job job WHERE c.id = ?1")
  Optional<Character> findById(Integer id);

  /**
   * Find all the characters with the desired gender
   * 
   * @param gender to be searched. True is Female
   * @return a list holding all the characters found with that gender
   */
  List<Character> findAllByGender(Boolean gender);

  /**
   * Find all the characters with the desired playable. False is NPC
   * 
   * @param playable status of the character
   * @return a list holding all the characters found that are Playable or not
   */
  List<Character> findAllByPlayable(Boolean playable);

  /**
   * Find all the characters with the desired Alignment id.
   * 
   * @param id of the Alignment to be searched.
   * @return a list holding all the characters found with that job
   */
  List<Character> findAllByAlignmentId(Integer id);

  /**
   * Find all the characters with the desired Alignment name.
   * 
   * @param name of the alignment to be searched.
   * @return a list holding all the characters found with that job
   */
  List<Character> findAllByAlignmentNameIgnoreCase(String name);

  /**
   * Find all the characters with the desired Alignment (lawful, neutral, chaotic).
   * 
   * @param lawful if the alignment is lawful, neutral or chaotic to be searched.
   * @return a list holding all the characters found with that alignment
   */
  List<Character> findAllByAlignmentLawful(Boolean lawful);

  /**
   * Find all the characters with the desired Alignment (good, neutral or evil)
   * 
   * @param good if the alignment is good, neutral or evil to be searched.
   * @return a list holding all the characters found with that alignment
   */
  List<Character> findAllByAlignmentGood(Boolean good);

  /**
   * Find all the characters with the desired job id.
   * 
   * @param id of the job to be searched.
   * @return a list holding all the characters found with that job
   */
  List<Character> findAllByJobId(Integer id);

  /**
   * Find all the characters with the desired job name.
   * 
   * @param name of the job to be searched.
   * @return a list holding all the characters found with that job
   */
  List<Character> findAllByJobNameIgnoreCase(String name);

  /**
   * Find all the characters with the desired size id.
   * 
   * @param id of the size to be searched.
   * @return a list holding all the characters found with that job
   */
  List<Character> findAllBySizeId(Integer id);

  /**
   * Find all the characters with the desired size name.
   * 
   * @param name of the size to be searched.
   * @return a list holding all the characters found with that job
   */
  List<Character> findAllBySizeNameIgnoreCase(String name);

  /**
   * Find all the characters with the desired size hit dice.
   * 
   * @param hitDice of the size to be searched.
   * @return a list holding all the characters found with that job
   */
  List<Character> findAllBySizeHitDice(Integer hitDice);

  /**
   * Find all the characters with the desired race id.
   * 
   * @param race to be searched.
   * @return a list holding all the characters found with that race.
   */
  @Query("SELECT c FROM #{#entityName} c WHERE c.race.id = ?1")
  List<Character> findAllByRaceId(Integer id);

  /**
   * Find all the characters with the desired race name.
   * 
   * @param race to be searched.
   * @return a list holding all the characters found with that race.
   */
  // @Query(nativeQuery = true, value = "SELECT * FROM personas INNER JOIN race ON race_type = ?1")
  @Query("SELECT c FROM #{#entityName} c WHERE c.race.race = ?1")
  List<Character> findAllByRaceRace(String race);

}
