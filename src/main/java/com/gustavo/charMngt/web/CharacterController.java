package com.gustavo.charMngt.web;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.PagedModel.PageMetadata;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gustavo.charMngt.model.Alignment;
import com.gustavo.charMngt.model.Character;
import com.gustavo.charMngt.model.Job;
import com.gustavo.charMngt.model.Race;
import com.gustavo.charMngt.model.Size;
import com.gustavo.charMngt.model.validation.OnCreate;
import com.gustavo.charMngt.model.validation.OnUpdate;
import com.gustavo.charMngt.service.CharacterService;
import com.gustavo.charMngt.service.exceptions.InvalidSearchException;

/**
 * Controller managing all access to the Characters entity
 * 
 * @author Gustavo
 *
 */
@RestController
@Validated
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/api/characters", produces = {MediaType.APPLICATION_JSON_VALUE})
public class CharacterController {

  private static final String INDEX_ROUTE = "";
  private static final String GET_ROUTE = "/{id}";
  private static final String TOTALS_ROUTE = "/totals";

  private CharacterService service;

  public CharacterController(CharacterService service) {
    this.service = service;
  }

  @GetMapping(value = INDEX_ROUTE)
  public ResponseEntity<PagedModel<EntityModel<Character>>> getAll(
      @RequestParam(value = "page", defaultValue = "0", required = false) int page,
      @RequestParam(value = "size", defaultValue = "20", required = false) int size,
      @RequestParam(value = "order", defaultValue = "ASC",
          required = false) Sort.Direction direction,
      @RequestParam(value = "sort", defaultValue = "id", required = false) String sortProperty) {

    Page<Character> slice =
        service.findAll(PageRequest.of(page, size, Sort.by(direction, sortProperty)));
    PagedModel<EntityModel<Character>> wrapped =
        PagedModel.wrap(slice, new PageMetadata(slice.getSize(), slice.getNumber(),
            slice.getNumberOfElements(), slice.getTotalPages()));

    wrapped.forEach(
        e -> e.add(linkTo(CharacterController.class).slash(e.getContent().getId()).withSelfRel(),
            linkTo(CharacterController.class).slash(e.getContent().getId()).slash("alignment")
                .withRel("alignment"),
            linkTo(CharacterController.class).slash(e.getContent().getId()).slash("class")
                .withRel("class"),
            linkTo(CharacterController.class).slash(e.getContent().getId()).slash("race")
                .withRel("race"),
            linkTo(CharacterController.class).slash(e.getContent().getId()).slash("size")
                .withRel("size")));


    wrapped.add(linkTo(CharacterController.class).withSelfRel(),
        linkTo(EntryPointController.class).withRel("home"));

    return ResponseEntity.ok(wrapped);
  }

  /**
   * 
   * @return A {@link ResponseEntity} mapped with the total of entities {@link Character}s saved on
   *         the persistence unit
   */
  @GetMapping(value = TOTALS_ROUTE)
  public ResponseEntity<Map<String, Integer>> totals() {
    return ResponseEntity.ok(Map.of("total", service.count()));
  }

  /**
   * Search for a {@link Character} with given ID
   * 
   * @param id of the {@link Character} to be searched for
   * @return A {@link ResponseEntity} mapped with the {@link Character} with that ID on a
   *         {@link EntityModel}
   */
  @GetMapping(value = GET_ROUTE)
  public ResponseEntity<EntityModel<Character>> getById(@PathVariable int id) {
    if (id <= 0) {
      throw new InvalidSearchException("Character id");
    } else {
      var item = service.findById(id);
      return ResponseEntity
          .ok(EntityModel.of(item, linkTo(CharacterController.class).withRel("characters"),
              linkTo(methodOn(CharacterController.class).getById(id)).withSelfRel(),
              linkTo(CharacterController.class).slash(id).slash("alignment").withRel("alignment"),
              linkTo(CharacterController.class).slash(id).slash("class").withRel("class"),
              linkTo(CharacterController.class).slash(id).slash("race").withRel("race"),
              linkTo(CharacterController.class).slash(id).slash("size").withRel("size")));
    }
  }

  /**
   * Get the {@link Alignment} of a {@link Character}
   * 
   * @param id of the {@link Character}
   * @return {@link ResponseEntity} with the {@link Alignment}
   */
  @GetMapping(value = GET_ROUTE + "/alignment")
  public ResponseEntity<EntityModel<Alignment>> getAlignmentByCharId(@PathVariable Integer id) {
    Alignment alignment = service.findById(id).getAlignment();
    return ResponseEntity.ok(EntityModel.of((alignment),
        linkTo(methodOn(CharacterController.class).getById(id)).withRel("character"),
        linkTo(CharacterController.class).withRel("characters"),
        linkTo(AlignmentController.class).slash(alignment.getId()).withSelfRel(),
        linkTo(AlignmentController.class).withRel("alignments")));
  }

  /**
   * Get the {@link Job} of a {@link Character}
   * 
   * @param id of the {@link Character}
   * @return {@link ResponseEntity} with the {@link Job}
   */
  @GetMapping(value = GET_ROUTE + "/class")
  public ResponseEntity<EntityModel<Job>> getJobByCharId(@PathVariable Integer id) {
    Job job = service.findById(id).getJob();
    return ResponseEntity.ok(EntityModel.of((job),
        linkTo(methodOn(CharacterController.class).getById(id)).withRel("character"),
        linkTo(CharacterController.class).withRel("characters"),
        linkTo(JobController.class).slash(job.getId()).withSelfRel(),
        linkTo(JobController.class).withRel("classes")));
  }

  /**
   * Get the {@link Race} of a {@link Character}
   * 
   * @param id of the {@link Character}
   * @return {@link ResponseEntity} with the {@link Race}
   */
  @GetMapping(value = GET_ROUTE + "/race")
  public ResponseEntity<EntityModel<Race>> getRaceByCharId(@PathVariable Integer id) {
    Race race = service.findById(id).getRace();
    return ResponseEntity.ok(EntityModel.of((race),
        linkTo(methodOn(CharacterController.class).getById(id)).withRel("character"),
        linkTo(CharacterController.class).withRel("characters"),
        linkTo(RaceController.class).slash(race.getId()).withSelfRel(),
        linkTo(RaceController.class).withRel("races")));
  }

  /**
   * Get the {@link Size} of a {@link Character}
   * 
   * @param id of the {@link Character}
   * @return {@link ResponseEntity} with the {@link Size}
   */
  @GetMapping(value = GET_ROUTE + "/size")
  public ResponseEntity<EntityModel<Size>> getSizeByCharId(@PathVariable Integer id) {
    Size size = service.findById(id).getSize();
    return ResponseEntity.ok(EntityModel.of((size),
        linkTo(methodOn(CharacterController.class).getById(id)).withRel("character"),
        linkTo(CharacterController.class).withRel("characters"),
        linkTo(methodOn(SizeController.class).getById(size.getId())).withSelfRel(),
        linkTo(SizeController.class).withRel("sizes")));
  }

  /**
   * Saves a new {@link Character}
   * 
   * @param character to be saved
   * @return an {@link ResponseEntity} with the saved version of the supplied {@link Character}.
   */
  @PostMapping(value = INDEX_ROUTE)
  @Validated(OnCreate.class)
  public ResponseEntity<EntityModel<Character>> postNewChar(
      @RequestBody @Valid Character character) {
    var action = service.save(character);
    return ResponseEntity
        .created(linkTo(CharacterController.class).slash(action.getId()).withSelfRel().toUri())
        .body(EntityModel.of(action).add(linkTo(CharacterController.class).withRel("characteres")));
  }

  /**
   * Updates a new {@link Character}. If ID is null, it will cause a bad response.
   * 
   * @param character to be updated or created
   * @return {@link ResponseEntity} with the version saved.
   */
  @PutMapping(value = INDEX_ROUTE)
  @Validated(OnUpdate.class)
  public ResponseEntity<EntityModel<Character>> put(@RequestBody @Valid Character character) {
    // return message is dependent of having an ID or not.
    if (service.contains(character.getId())) {

      var response = service.update(character);
      return ResponseEntity.ok()
          .body(EntityModel.of(response,
              linkTo(CharacterController.class).slash(response.getId()).withSelfRel(),
              linkTo(CharacterController.class).withRel("characters")));
    } else {
      character.setId(null);

      var response = service.update(character);
      return ResponseEntity
          .created(linkTo(CharacterController.class).slash(response.getId()).withSelfRel().toUri())
          .body(EntityModel.of(response)
              .add(linkTo(CharacterController.class).withRel("characters")));
    }
  }

  /**
   * Deletes an {@link Character}
   * 
   * @param id of the alignment to be deleted
   * @return {@link ResponseEntity} if successful
   * @throws IllegalArgumentException - in case the given entity is null.
   */
  @DeleteMapping(value = GET_ROUTE)
  public ResponseEntity<?> delete(@PathVariable(value = "id") Integer id) {
    var delete = service.findById(id);
    service.delete(delete);

    return ResponseEntity
        .ok(EntityModel.of("Deleted!", linkTo(CharacterController.class).withRel("characters"),
            linkTo(EntryPointController.class).withRel("home")));
  }
}
