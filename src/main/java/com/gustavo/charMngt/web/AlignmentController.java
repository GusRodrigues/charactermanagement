package com.gustavo.charMngt.web;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.Collections;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.PagedModel.PageMetadata;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gustavo.charMngt.model.Alignment;
import com.gustavo.charMngt.model.validation.OnCreate;
import com.gustavo.charMngt.model.validation.OnUpdate;
import com.gustavo.charMngt.service.AlignmentService;
import com.gustavo.charMngt.service.exceptions.InvalidSearchException;

/**
 * Controller that manages the access to the Alignment entity. Calls must come from /api/alignments*
 * 
 * @author Gustavo
 *
 */
@RestController
@Validated
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/api/alignments", produces = MediaType.APPLICATION_JSON_VALUE)
public class AlignmentController {

  private static final String INDEX_ROUTE = "";
  private static final String GET_ROUTE = "/{id}";
  private static final String SEARCH_ROUTE = "/search";
  private static final String TOTALS_ROUTE = "/totals";

  private AlignmentService service;

  public AlignmentController(AlignmentService service) {
    this.service = service;
  }

  /**
   * @param page the page number
   * @param size how many items per page
   * @param direction ASCending or DESCending
   * @param sortProperty how to sort
   * @return {@link ResponseEntity} with a list of all alignments
   */
  @GetMapping(value = INDEX_ROUTE)
  public ResponseEntity<?> getAll(
      @RequestParam(value = "page", defaultValue = "0", required = false) int page,
      @RequestParam(value = "size", defaultValue = "20", required = false) int size,
      @RequestParam(value = "order", defaultValue = "ASC",
          required = false) Sort.Direction direction,
      @RequestParam(value = "sort", defaultValue = "id", required = false) String sortProperty) {

    // wrap the response in the Colection Model of Entity Model of Alignment Domain
    Page<Alignment> slice =
        service.findAll(PageRequest.of(page, size, Sort.by(direction, sortProperty)));
    PagedModel<EntityModel<Alignment>> wrapped =
        PagedModel.wrap(slice, new PageMetadata(slice.getSize(), slice.getNumber(),
            slice.getTotalElements(), slice.getTotalPages()));

    wrapped.forEach(e -> e.add(
        linkTo(methodOn(AlignmentController.class).getById(e.getContent().getId())).withSelfRel()));

    wrapped.add(linkTo(AlignmentController.class).withSelfRel(),
        linkTo(EntryPointController.class).withRel("home"));

    return ResponseEntity.ok(wrapped);
  }

  /**
   * Search for an {@link Alignment} using the ID
   * 
   * @param id to be searched
   * @return {@link ResponseEntity} with the ID, if found.
   */
  @GetMapping(value = GET_ROUTE)
  public ResponseEntity<?> getById(@PathVariable Integer id) {
    if (id == null) {
      throw new InvalidSearchException("Alignment id");
    }
    return new ResponseEntity<>(EntityModel.of(service.findById(id),
        linkTo(AlignmentController.class).withRel("alignments")), HttpStatus.OK);
  }

  /**
   * Search for an {@link Alignment} using the name
   * 
   * @param name the name of the {@link Alignment}
   * @return {@link ResponseEntity} with the Alignment, if found.
   */
  @GetMapping(value = SEARCH_ROUTE)
  public ResponseEntity<?> getByName(@RequestParam(value = "name", required = true) String name) {
    return new ResponseEntity<>(EntityModel.of(service.findByName(name),
        linkTo(AlignmentController.class).withRel("alignments")), HttpStatus.OK);
  }

  /**
   * Return the total of {@link Alignment}s saved.
   * 
   * @return {@link ResponseEntity} with the total of {@link Alignment}s
   */
  @GetMapping(value = TOTALS_ROUTE)
  public ResponseEntity<Map<String, Integer>> totals() {
    return new ResponseEntity<>(Collections.singletonMap("total", service.count()), HttpStatus.OK);
  }

  /**
   * Saves a new {@link Alignment}. Requires the ID to be null.
   * 
   * @param alignment to be saved
   * @return created {@link ResponseEntity} with the saved version of the {@link Alignment}
   */
  @PostMapping(value = INDEX_ROUTE)
  @Validated(OnCreate.class)
  public ResponseEntity<?> postNewEntity(@RequestBody @Valid Alignment alignment) {

    Alignment action = service.save(alignment);
    return ResponseEntity
        .created(linkTo(AlignmentController.class).slash(action.getId()).withSelfRel().toUri())
        .body(EntityModel.of(action).add(linkTo(AlignmentController.class).withRel("aligments")));

  }

  /**
   * Updates a new {@link Alignment}. Requires ID, or it will cause a bad response.
   * 
   * @param alignment to be updated or created
   * @return {@link ResponseEntity} with the version saved.
   */
  @PutMapping(value = INDEX_ROUTE)
  @Validated(OnUpdate.class)
  public ResponseEntity<EntityModel<Alignment>> put(@RequestBody @Valid Alignment alignment) {
    // return message is dependent of having an ID or not.
    if (service.contains(alignment.getId())) {

      Alignment response = service.update(alignment);
      return ResponseEntity.ok()
          .body(EntityModel.of(response,
              linkTo(AlignmentController.class).slash(response.getId()).withSelfRel(),
              linkTo(AlignmentController.class).withRel("alignments")));
    } else {
      alignment.setId(null);

      var response = service.update(alignment);
      return ResponseEntity
          .created(linkTo(AlignmentController.class).slash(response.getId()).withSelfRel().toUri())
          .body(
              EntityModel.of(response).add(linkTo(AlignmentController.class).withRel("aligments")));
    }
  }

  /**
   * Deletes an {@link Alignment}. Requires the ID to be removed.
   * 
   * @param id of the alignment to be deleted
   * @return {@link ResponseEntity} if successful
   */
  @DeleteMapping(value = GET_ROUTE)
  public ResponseEntity<?> delete(@PathVariable(value = "id") Integer id) {
    var delete = service.findById(id);
    service.delete(delete);

    return ResponseEntity
        .ok(EntityModel.of("Deleted!", linkTo(AlignmentController.class).withRel("alignments"),
            linkTo(EntryPointController.class).withRel("home")));
  }
}
