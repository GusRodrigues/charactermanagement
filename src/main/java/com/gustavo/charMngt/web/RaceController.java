package com.gustavo.charMngt.web;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.Collections;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.PagedModel.PageMetadata;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gustavo.charMngt.model.Race;
import com.gustavo.charMngt.model.validation.OnCreate;
import com.gustavo.charMngt.model.validation.OnUpdate;
import com.gustavo.charMngt.service.RaceService;
import com.gustavo.charMngt.service.exceptions.InvalidSearchException;

/**
 * Controller managing the Race Entity
 * 
 * Calls must come from /api/*
 * 
 * @author Gustavo
 *
 */
@RestController
@Validated
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/api/races", produces = {MediaType.APPLICATION_JSON_VALUE})
public class RaceController {

  private static final String INDEX_ROUTE = "";
  private static final String GET_ROUTE = "/{id}";
  private static final String SEARCH_ROUTE = "/search";
  private static final String TOTALS_ROUTE = "/totals";

  private RaceService service;

  public RaceController(RaceService service) {
    this.service = service;
  }

  /**
   * @param page the page number
   * @param size how many items per page
   * @param direction ASCending or DESCending
   * @param sortProperty how to sort
   * @return {@link ResponseEntity} with a list of all {@link Race}s.
   */
  @GetMapping(value = INDEX_ROUTE)
  public ResponseEntity<?> getRaces(
      @RequestParam(value = "page", defaultValue = "0", required = false) int page,
      @RequestParam(value = "size", defaultValue = "20", required = false) int size,
      @RequestParam(value = "order", defaultValue = "ASC",
          required = false) Sort.Direction direction,
      @RequestParam(value = "sort", defaultValue = "id", required = false) String sortProperty) {

    Page<Race> slice =
        service.findAll(PageRequest.of(page, size, Sort.by(direction, sortProperty)));

    PagedModel<EntityModel<Race>> wrapped = PagedModel.wrap(slice, new PageMetadata(slice.getSize(),
        slice.getNumber(), slice.getTotalElements(), slice.getTotalPages()));

    wrapped.forEach(e -> e
        .add(linkTo(methodOn(RaceController.class).getById(e.getContent().getId())).withSelfRel()));

    wrapped.add(linkTo(RaceController.class).withSelfRel(),
        linkTo(EntryPointController.class).withRel("home"));

    return new ResponseEntity<>(wrapped, HttpStatus.OK);
  }

  /**
   * Return a {@link Race} if that given ID, if exists.
   * 
   * @param id of the race
   * @return {@link ResponseEntity} mapped with the race
   */
  @GetMapping(value = GET_ROUTE)
  public ResponseEntity<?> getById(@PathVariable Integer id) {

    if (id == null) {
      throw new InvalidSearchException("Race id");
    }

    return ResponseEntity
        .ok(EntityModel.of(service.findById(id), linkTo(RaceController.class).withRel("races")));
  }

  /**
   * Return a {@link Race} if that given name, if exists.
   * 
   * @param race name of the race
   * @return {@link ResponseEntity} mapped with the race
   */
  @GetMapping(value = SEARCH_ROUTE)
  public ResponseEntity<?> getByName(@RequestParam(value = "race", required = true) String race) {

    if (race == null) {
      throw new InvalidSearchException("Race name");
    }

    return ResponseEntity.ok(
        EntityModel.of(service.findByName(race), linkTo(RaceController.class).withRel("races")));
  }

  /**
   * Returns the total of {@link Race}s
   * 
   * @return {@link ResponseEntity} with the total of races.
   */
  @GetMapping(value = TOTALS_ROUTE)
  public ResponseEntity<?> totals() {
    return ResponseEntity.ok(EntityModel.of(Collections.singletonMap("total", service.count()),
        linkTo(RaceController.class).withRel("races"),
        linkTo(EntryPointController.class).withRel("home")));
  }

  /**
   * Saves a new {@link Race} to the persistence unit. Requires an Id
   * 
   * @param race to be saved
   * @return {@link ResponseEntity} with the persisted version of the {@link Race}
   */
  @PostMapping(value = INDEX_ROUTE)
  @Validated(value = OnCreate.class)
  public ResponseEntity<?> postNewRace(@RequestBody @Valid Race race) {
    Race action = service.save(race);
    return ResponseEntity
        .created(linkTo(RaceController.class).slash(action.getId()).withSelfRel().toUri())
        .body(EntityModel.of(action).add(linkTo(RaceController.class).withRel("races")));

  }

  /**
   * Updates a new {@link Race}. Requires an Id, or it will cause a bad response.
   * 
   * @param alignment to be updated or created
   * @return {@link ResponseEntity} with the version saved.
   */
  @PutMapping(value = INDEX_ROUTE)
  @Validated(OnUpdate.class)
  public ResponseEntity<EntityModel<Race>> put(@RequestBody @Valid Race race) {
    // return message is dependent of having an ID or not.
    if (service.contains(race.getId())) {

      var response = service.update(race);
      return ResponseEntity.ok()
          .body(EntityModel.of(response,
              linkTo(RaceController.class).slash(response.getId()).withSelfRel(),
              linkTo(RaceController.class).withRel("races")));
    } else {
      race.setId(null);

      var response = service.update(race);
      return ResponseEntity
          .created(linkTo(RaceController.class).slash(response.getId()).withSelfRel().toUri())
          .body(EntityModel.of(response).add(linkTo(RaceController.class).withRel("races")));
    }
  }

  /**
   * Deletes an {@link Race}. Requires an Id
   * 
   * @param id of the alignment to be deleted
   * @return {@link ResponseEntity} if successful
   */
  @DeleteMapping(value = GET_ROUTE)
  public ResponseEntity<?> delete(@PathVariable(value = "id") Integer id) {
    var delete = service.findById(id);
    service.delete(delete);

    return ResponseEntity
        .ok(EntityModel.of("Deleted!", linkTo(RaceController.class).withRel("races"),
            linkTo(EntryPointController.class).withRel("home")));
  }
}
