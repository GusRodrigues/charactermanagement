package com.gustavo.charMngt.web;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.Collections;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.PagedModel.PageMetadata;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gustavo.charMngt.model.Job;
import com.gustavo.charMngt.model.validation.OnCreate;
import com.gustavo.charMngt.model.validation.OnUpdate;
import com.gustavo.charMngt.service.JobService;
import com.gustavo.charMngt.service.exceptions.InvalidSearchException;

/**
 * API entry point for queries related to the character class or jobs. All calls must come from
 * /api/classes.
 * 
 * Named as Job to prevent use of restricted word class.
 * 
 * @author Gustavo
 *
 */
@RestController
@Validated
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/api/classes", produces = {MediaType.APPLICATION_JSON_VALUE})
public class JobController {

  private static final String INDEX_ROUTE = "";
  private static final String GET_ROUTE = "/{id}";
  private static final String SEARCH_ROUTE = "/search";
  private static final String TOTALS_ROUTE = "/totals";

  private JobService service;

  public JobController(JobService service) {
    this.service = service;
  }

  /**
   * @param page the page number
   * @param size how many items per page
   * @param direction ASCending or DESCending
   * @param sortProperty how to sort
   * @return {@link ResponseEntity} holding all classes
   */
  @GetMapping(value = INDEX_ROUTE)
  public ResponseEntity<?> getClasses(
      @RequestParam(value = "page", defaultValue = "0", required = false) int page,
      @RequestParam(value = "size", defaultValue = "20", required = false) int size,
      @RequestParam(value = "order", defaultValue = "ASC",
          required = false) Sort.Direction direction,
      @RequestParam(value = "sort", defaultValue = "id", required = false) String sortProperty) {
    Page<Job> slice = service.findAll(PageRequest.of(page, size, Sort.by(direction, sortProperty)));
    PagedModel<EntityModel<Job>> wrapped = PagedModel.wrap(slice, new PageMetadata(slice.getSize(),
        slice.getNumber(), slice.getTotalElements(), slice.getTotalPages()));

    wrapped.forEach(e -> e
        .add(linkTo(methodOn(JobController.class).getById(e.getContent().getId())).withSelfRel()));

    wrapped.add(linkTo(JobController.class).withSelfRel(),
        linkTo(EntryPointController.class).withRel("home"));

    return ResponseEntity.ok(wrapped);
  }

  /**
   * Return a {@link Job} (class). Requires an Id.
   * 
   * @param id of the {@link Job}
   * @return {@link ResponseEntity} with the {@link Job}
   */
  @GetMapping(value = GET_ROUTE)
  public ResponseEntity<?> getById(@PathVariable(value = "id", required = false) Integer id) {
    if (id == null) {
      throw new InvalidSearchException("Class id");
    }

    return ResponseEntity
        .ok(EntityModel.of(service.findById(id), linkTo(JobController.class).withRel("classes")));
  }

  /**
   * Return a {@link Job} (class) by it's name, if existing.
   * 
   * @param name of the {@link Job}
   * @return {@link ResponseEntity} with the {@link Job}
   */
  @GetMapping(value = SEARCH_ROUTE)
  public ResponseEntity<?> getByName(@RequestParam(value = "name", required = false) String name) {
    if (name == null) {
      throw new InvalidSearchException("Class name");
    }

    return ResponseEntity.ok(
        EntityModel.of(service.findByName(name), linkTo(JobController.class).withRel("classes")));
  }

  /**
   * @return {@link ResponseEntity} the totals of classes
   */
  @GetMapping(value = TOTALS_ROUTE)
  public ResponseEntity<Map<String, Integer>> count() {
    return new ResponseEntity<>(Collections.singletonMap("total", service.count()), HttpStatus.OK);
  }

  /**
   * Saves a new {@link Job} to the persistence unit. ID Must be null.
   * 
   * @param job to be saved
   * @return {@link ResponseEntity} with the persisted version of the {@link Job}
   */
  @PostMapping(value = INDEX_ROUTE)
  @Validated(OnCreate.class)
  public ResponseEntity<?> postNewJob(@RequestBody @Valid Job job) {
    Job action = service.save(job);
    return ResponseEntity
        .created(linkTo(JobController.class).slash(action.getId()).withSelfRel().toUri())
        .body(EntityModel.of(action).add(linkTo(JobController.class).withRel("classes")));
  }

  /**
   * Updates a new {@link Job}. If ID is null, it will cause a bad response.
   * 
   * @param alignment to be updated or created
   * @return {@link ResponseEntity} with the version saved.
   */
  @PutMapping(value = INDEX_ROUTE)
  @Validated(value = OnUpdate.class)
  public ResponseEntity<EntityModel<Job>> put(@RequestBody @Valid Job job) {
    // return message is dependent of having an ID or not.
    if (service.contains(job.getId())) {

      var response = service.update(job);
      return ResponseEntity.ok()
          .body(EntityModel.of(response,
              linkTo(JobController.class).slash(response.getId()).withSelfRel(),
              linkTo(JobController.class).withRel("classes")));
    } else {
      job.setId(null);
      var response = service.update(job);
      return ResponseEntity
          .created(linkTo(JobController.class).slash(response.getId()).withSelfRel().toUri())
          .body(EntityModel.of(response).add(linkTo(JobController.class).withRel("classes")));
    }
  }

  /**
   * Deletes an {@link Job}
   * 
   * @param id of the alignment to be deleted
   * @return {@link ResponseEntity} if successful
   * @throws IllegalArgumentException - in case the given entity is null.
   */
  @DeleteMapping(value = GET_ROUTE)
  public ResponseEntity<?> delete(@PathVariable(value = "id") Integer id) {
    Job delete = service.findById(id);
    service.delete(delete);

    return ResponseEntity
        .ok(EntityModel.of("Deleted!", linkTo(JobController.class).withRel("classes"),
            linkTo(EntryPointController.class).withRel("home")));
  }

}
