package com.gustavo.charMngt.web.errorHandling;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

/**
 * Class that will wrap some generic errors usually handled by Spring
 * 
 * @author Gustavo
 *
 */
public class ApiError {
  private HttpStatus status;
  private LocalDateTime time;
  private List<String> errors;


  public ApiError(HttpStatus status, String errors) {
    this.status = status;
    this.time = LocalDateTime.now();
    this.errors = List.of(errors);
  }


  public ApiError(HttpStatus status, List<String> errors) {
    this.status = status;
    this.errors = new ArrayList<>(errors);

    this.time = LocalDateTime.now();
  }

  public HttpStatus getStatus() {
    return status;
  }

  public LocalDateTime getTimestamp() {
    return this.time;
  }

  public List<String> getErrors() {
    return List.copyOf(errors);
  }
}
