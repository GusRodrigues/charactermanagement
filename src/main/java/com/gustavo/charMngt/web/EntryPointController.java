package com.gustavo.charMngt.web;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller that will handle the entry point for the API
 * 
 * @author Gustavo
 *
 */
@RestController
@RequestMapping(value = "/api", produces = {MediaType.APPLICATION_JSON_VALUE})
public class EntryPointController {

  @GetMapping({"", "/"})
  public ResponseEntity<RepresentationModel<?>> nodes() {
    RepresentationModel<?> model = new RepresentationModel<>();
    model.add(linkTo(AlignmentController.class).withRel("aligments"));
    model.add(linkTo(CharacterController.class).withRel("characters"));
    model.add(linkTo(JobController.class).withRel("classes"));
    model.add(linkTo(RaceController.class).withRel("races"));
    model.add(linkTo(SizeController.class).withRel("sizes"));

    return ResponseEntity.ok(model);
  }
}
