package com.gustavo.charMngt.web;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.Collections;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.PagedModel.PageMetadata;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gustavo.charMngt.model.Size;
import com.gustavo.charMngt.model.validation.OnCreate;
import com.gustavo.charMngt.model.validation.OnUpdate;
import com.gustavo.charMngt.service.SizeService;
import com.gustavo.charMngt.service.exceptions.InvalidSearchException;

/**
 * Controller managing API calls to query the size entity entry point should be through /api/*
 * 
 * @author Gustavo
 *
 */

@RestController
@Validated
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/api/sizes", produces = {MediaType.APPLICATION_JSON_VALUE})
public class SizeController {

  private static final String INDEX_ROUTE = "";
  private static final String GET_ROUTE = "/{id}";
  private static final String SEARCH_ROUTE = "/search";
  private static final String TOTALS_ROUTE = "/totals";

  private SizeService service;

  public SizeController(SizeService service) {
    this.service = service;
  }

  /**
   * @param page the page number
   * @param size how many items per page
   * @param direction ASCending or DESCending
   * @param sortProperty how to sort
   * @return {@link ResponseEntity} with a list of all sizes in the persistence unit
   */
  @GetMapping(value = INDEX_ROUTE)
  public ResponseEntity<?> getSizes(
      @RequestParam(value = "page", defaultValue = "0", required = false) int page,
      @RequestParam(value = "size", defaultValue = "20", required = false) int size,
      @RequestParam(value = "order", defaultValue = "ASC",
          required = false) Sort.Direction direction,
      @RequestParam(value = "sort", defaultValue = "id", required = false) String sortProperty) {

    // wrap the response in the Colection Model of Entity Model of Alignment Domain
    Page<Size> slice =
        service.findAll(PageRequest.of(page, size, Sort.by(direction, sortProperty)));

    PagedModel<EntityModel<Size>> wrapped = PagedModel.wrap(slice, new PageMetadata(slice.getSize(),
        slice.getNumber(), slice.getTotalElements(), slice.getTotalPages()));

    wrapped.forEach(e -> e
        .add(linkTo(methodOn(SizeController.class).getById(e.getContent().getId())).withSelfRel()));

    wrapped.add(linkTo(SizeController.class).withSelfRel(),
        linkTo(EntryPointController.class).withRel("home"));

    return ResponseEntity.ok(wrapped);
  }

  /**
   * Search for a {@link Size} with it's ID. It will throw an {@link EntityNotFoundException} if not
   * found.
   * 
   * @param id of the {@link Size}
   * @return {@link ResponseEntity} mapped with the size
   */
  @GetMapping(value = GET_ROUTE)
  public ResponseEntity<?> getById(@PathVariable Integer id) {
    if (id == null) {
      throw new InvalidSearchException("Size Id");
    }
    return ResponseEntity.ok(EntityModel.of(service.findById(id),
        linkTo(methodOn(SizeController.class).getSizes(0, 20, Sort.Direction.ASC, "id"))
            .withRel("sizes")));
  }

  /**
   * Search for an {@link Size} using the name. 
   * 
   * @param name the name of the {@link Size}
   * @return {@link ResponseEntity} with the Size, if found.
   */
  @GetMapping(value = SEARCH_ROUTE)
  public ResponseEntity<?> getByName(@RequestParam(value = "name", required = true) String name) {
    return ResponseEntity.ok(EntityModel.of(service.findByName(name),
        linkTo(methodOn(SizeController.class).getSizes(0, 20, Sort.Direction.ASC, "id"))
            .withRel("sizes")));
  }

  /**
   * 
   * @return {@link ResponseEntity} mapped with the total of sizes.
   */
  @GetMapping(value = TOTALS_ROUTE)
  public ResponseEntity<Map<String, Integer>> totals() {
    return new ResponseEntity<Map<String, Integer>>(
        Collections.singletonMap("total", service.count()), HttpStatus.OK);
  }

  /**
   * Saves a new {@link Size} to the persistence unit
   * 
   * @param size to be saved. Cannot have an ID
   * @return The {@link ResponseEntity} with the persisted version of the {@link Size}
   */
  @PostMapping(value = INDEX_ROUTE)
  @Validated(value = OnCreate.class)
  public ResponseEntity<?> postNewSize(@RequestBody @Valid Size size) {
    Size action = service.save(size);
    return ResponseEntity
        .created(linkTo(SizeController.class).slash(action.getId()).withSelfRel().toUri())
        .body(EntityModel.of(action).add(linkTo(SizeController.class).withRel("sizes")));

  }

  /**
   * Updates a new {@link Size}. If ID is null, it will cause a bad response.
   * 
   * @param alignment to be updated or created
   * @return {@link ResponseEntity} with the version saved.
   */
  @PutMapping(value = INDEX_ROUTE)
  @Validated(OnUpdate.class)
  public ResponseEntity<EntityModel<Size>> put(@RequestBody @Valid Size size) {
    // return message is dependent of having an ID or not.
    if (service.contains(size.getId())) {

      Size response = service.update(size);
      return ResponseEntity.ok()
          .body(EntityModel.of(response,
              linkTo(SizeController.class).slash(response.getId()).withSelfRel(),
              linkTo(SizeController.class).withRel("sizes")));
    } else {
      size.setId(null);

      var response = service.update(size);
      return ResponseEntity
          .created(linkTo(SizeController.class).slash(response.getId()).withSelfRel().toUri())
          .body(EntityModel.of(response).add(linkTo(SizeController.class).withRel("sizes")));
    }
  }

  /**
   * Deletes an {@link Size}
   * 
   * @param id of the alignment to be deleted
   * @return {@link ResponseEntity} if successful
   */
  @DeleteMapping(value = GET_ROUTE)
  public ResponseEntity<?> delete(@PathVariable(value = "id") Integer id) {
    var delete = service.findById(id);
    service.delete(delete);

    return ResponseEntity
        .ok(EntityModel.of("Deleted!", linkTo(SizeController.class).withRel("sizes"),
            linkTo(EntryPointController.class).withRel("home")));
  }
}
