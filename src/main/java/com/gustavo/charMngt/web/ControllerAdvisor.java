package com.gustavo.charMngt.web;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.gustavo.charMngt.service.exceptions.DuplicatedEntityException;
import com.gustavo.charMngt.service.exceptions.InvalidSearchException;
import com.gustavo.charMngt.service.exceptions.model.EntityNotFoundException;
import com.gustavo.charMngt.web.errorHandling.ApiError;

/**
 * Centralized class to handle exceptions thrown by the API
 * 
 * @author Gustavo
 *
 */
@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    List<String> errors = new ArrayList<String>();

    ex.getBindingResult().getFieldErrors()
        .forEach(e -> errors.add(String.format("%s : %s", e.getField(), e.getDefaultMessage())));

    var apiError = new ApiError(HttpStatus.BAD_REQUEST, errors);
    return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
  }

  @ExceptionHandler(DuplicatedEntityException.class)
  public ResponseEntity<?> handleEntityExists(DuplicatedEntityException ex) {
    List<String> errors = new ArrayList<>();

    errors.add(String.format("%s : %s", ex.getEntity(), ex.getMessage()));
    var apiError = new ApiError(HttpStatus.FORBIDDEN, errors);
    return ResponseEntity.status(apiError.getStatus()).body(apiError);
  }

  /**
   * Intercepts the call that resulted in a a Exception of {@link EntityNotFoundException} being
   * thrown.
   * 
   * @param ex The {@link EntityNotFoundException}
   * @return
   * @return 404 status with the map with timestamp and message
   */
  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<Map<String, Object>> handleEntityNotFoundException(
      EntityNotFoundException ex) {
    /*
     * java 8 Map<String, Object> map = new LinkedHashMap<>(); map.put("timestamp",
     * LocalDateTime.now()); map.put("message", ex.getMessage()); return new ResponseEntity<>(map,
     * HttpStatus.NOT_FOUND);
     */
    var now = LocalDateTime.now();
    Map<String, Object> map = Map.of("timestamp", now, "message", ex.getMessage());
    // java 9+
    return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
  }

  /**
   * Intercepts the call that resulted in a a Exception of {@link InvalidSearchException} being
   * thrown.
   * 
   * @param ex The {@link InvalidSearchException}
   * @return
   * @return 404 status with the map with timestamp and message
   */
  @ExceptionHandler({InvalidSearchException.class, MethodArgumentTypeMismatchException.class})
  public ResponseEntity<Map<String, Object>> handleInvalidSearches(RuntimeException ex) {
    if (ex instanceof MethodArgumentTypeMismatchException) {
      String cause = ((MethodArgumentTypeMismatchException) ex).getCause().toString();
      return ResponseEntity.badRequest().body(Map.of("timestamp", LocalDateTime.now(), "message",
          cause.substring(cause.indexOf(":") + 1).trim()));
    }
    // java 9+
    return ResponseEntity.badRequest()
        .body((Map.of("timestamp", LocalDateTime.now(), "message", ex.getMessage())));
  }

  @ExceptionHandler(ConstraintViolationException.class)
  public ResponseEntity<ApiError> handleConstraintViolationException(
      ConstraintViolationException ex) {

    ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getConstraintViolations().stream()
        .map(e -> e.getMessage()).collect(Collectors.toList()));
    return ResponseEntity.status(apiError.getStatus()).body(apiError);
  }
}
