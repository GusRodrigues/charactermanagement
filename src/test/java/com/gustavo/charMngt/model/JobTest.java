package com.gustavo.charMngt.model;

import static org.junit.jupiter.api.Assertions.*;
import java.util.*;
import org.junit.jupiter.api.*;

public class JobTest implements EntityTestCase<Job> {

  @Override
  public List<Job> entities() {
    return List.of(new Job(null, "Test"), new Job(null, "Another Test"),
        new Job(null, "Don't care"));
  }

  @Test
  @Override
  @DisplayName("Equality ignores ID")
  public void test_equalityIsHeld() {
    var item = new Job(2, "Test");
    assertEquals(item, entities().get(0));
  }

}
