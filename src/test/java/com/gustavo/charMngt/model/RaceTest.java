package com.gustavo.charMngt.model;

import static org.junit.jupiter.api.Assertions.*;
import java.util.*;
import org.junit.jupiter.api.*;

public class RaceTest implements EntityTestCase<Race> {

  @Override
  public List<Race> entities() {
    return List.of(new Race(null, "Test"), new Race(null, "anotherTest"),
        new Race(null, "not a test"));
  }

  @Override
  @DisplayName("Equality ignores ID")
  @Test
  public void test_equalityIsHeld() {
    Race id = new Race(1, "Test");
    assertEquals(id, entities().get(0));
  }

}
