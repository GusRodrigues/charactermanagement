package com.gustavo.charMngt.model;

import static org.junit.jupiter.api.Assertions.*;
import java.util.*;
import org.junit.jupiter.api.*;

public class SizeTest implements EntityTestCase<Size> {

  @Override
  public List<Size> entities() {
    return List.of(new Size(null, "Test", 8), new Size(null, "different Test", 8),
        new Size(null, "not a Test", 8));
  }

  @Override
  @Test
  @DisplayName("Equality ignores ID")
  public void test_equalityIsHeld() {
    Size id = new Size(1, "Test", 8);
    assertEquals(id, entities().get(0), "Equality must ignore ID");

  }


}
