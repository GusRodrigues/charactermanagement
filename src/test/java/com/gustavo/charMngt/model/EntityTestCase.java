package com.gustavo.charMngt.model;

import static org.junit.jupiter.api.Assertions.*;
import java.util.*;
import java.util.stream.*;
import org.junit.jupiter.api.*;

public interface EntityTestCase<T> {

  /**
   * 
   * @return Provides a List with at least 3 valid and different entities to be tested
   */
  List<T> entities();

  @Test
  @DisplayName("Different entities are not true")
  default void test_twoDifferentEntities() {
    Set<T> holder = new HashSet<>();
    holder.add(entities().get(new Random().nextInt(entities().size())));
    while (holder.size() < 2) {
      holder.add(entities().get(new Random().nextInt(entities().size())));
    }
    var items = holder.stream().collect(Collectors.toList());
    assertNotEquals(items.get(0), items.get(1), "Items must be different");
  }

  @Test
  @DisplayName("Equality is held for reflexive, symmetric and transitive")
  default void test_principlesOfEquality() {
    T first = entities().get(new Random().nextInt(entities().size()));
    T second = first;
    T third = second;

    assertTrue(Objects.equals(first, first), "reflexive equality is not being held");
    assertTrue(Objects.equals(first, second), "symetric equality is not being held");
    assertTrue((Objects.equals(third, second) == Objects.equals(first, third)),
        "transitive equality is not being held");
  }

  @Test
  void test_equalityIsHeld();

}
