package com.gustavo.charMngt.model;

import static org.junit.jupiter.api.Assertions.*;
import java.util.*;
import org.junit.jupiter.api.*;

/**
 * Test case for Alignment Entity
 * 
 * @author Gustavo
 *
 */
public class AlignmentTest implements EntityTestCase<Alignment> {

  @Override
  public List<Alignment> entities() {
    return List.of(new Alignment(null, null, null, "Test"),
        new Alignment(null, false, false, "Evil"), new Alignment(null, true, true, "Good"));
  }

  @Override
  @Test
  @DisplayName("Equality Ignores ID")
  public void test_equalityIsHeld() {
    var item = new Alignment(2, null, null, "Test");

    assertEquals(item, entities().get(0));
  }
}
