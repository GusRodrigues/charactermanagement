package com.gustavo.charMngt.model;

import static org.junit.jupiter.api.Assertions.*;
import java.util.*;
import org.junit.jupiter.api.*;

public class CharacterTest implements EntityTestCase<Character> {

  public static Character defaultChar() {
    var character = new Character();
    character.setId(1);
    character.setAlignment(new Alignment(1, true, true, "Crusader"));
    character.setJob(new Job(1, "Paladin"));
    character.setSize(new Size(1, "Medium", 8));
    character.setRace(new Race(1, "Human"));
    character.setName("Dummy");
    character.setPlayable(true);
    character.setExperiencePoint(0);
    character.setGender(false);
    character.setAttributes(new Attribute(10, 10, 10, 10, 10, 10));
    return character;
  }

  @DisplayName("Test representation of a paladin")
  @Test
  public void test_toStringFighter() {
    // Char [#1, Dummy, Human, Male, Playable, Paladin, 0, Crusader, Medium]
    // Char [STR 10, DEX 10, CON 10, INT 10, WIS 10, CHAR 10]

    String expected = "Char [#1, Dummy, Human, Male, Playable, Paladin, 0, Crusader, Medium]\n"
        + "Char [STR 10, DEX 10, CON 10, INT 10, WIS 10, CHAR 10]";
    String actual = defaultChar().toString();
    assertEquals(expected, actual, "To String is not equal");
  }

  @Override
  public List<Character> entities() {
    Character first = defaultChar(), second = defaultChar(), third = defaultChar();
    second.setName("Test");
    third.setName("Final test");
    return List.of(first, second, third);
  }

  @Override
  @Test
  @DisplayName("Equality Ignores ID")
  public void test_equalityIsHeld() {
    var test_noId = defaultChar();
    test_noId.setId(null);
    assertEquals(defaultChar(), test_noId, "Should ignore the ID");

  }
}
