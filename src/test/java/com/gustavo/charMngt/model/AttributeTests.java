package com.gustavo.charMngt.model;

import java.util.*;
import org.junit.jupiter.api.*;

public class AttributeTests implements EntityTestCase<Attribute> {

  @Override
  public List<Attribute> entities() {
    return List.of(new Attribute(10, 10, 10, 10, 10, 10), new Attribute(15, 15, 15, 15, 15, 15),
        new Attribute(2, 2, 2, 2, 2, 2));
  }

  @Override
  @DisplayName("Embedded entity has no id")
  public void test_equalityIsHeld() { /* No tests here */ }
}
