package com.gustavo.charMngt.web;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;
import java.util.*;
import javax.validation.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.springframework.data.domain.*;
import org.springframework.hateoas.*;
import org.springframework.hateoas.PagedModel.*;
import org.springframework.http.*;
import com.gustavo.charMngt.model.*;
import com.gustavo.charMngt.model.validation.*;
import com.gustavo.charMngt.service.*;
import com.gustavo.charMngt.service.exceptions.*;
import com.gustavo.charMngt.service.exceptions.model.*;

@ExtendWith(MockitoExtension.class)
public class AlignmentControllerTest {

  @InjectMocks
  private AlignmentController controller;

  @Mock
  private AlignmentService service;

  private PageRequest page = PageRequest.of(0, 20, Sort.by(Sort.Direction.ASC, "id"));

  @DisplayName("Test get all alignments")
  @Test
  public void test_allAlignments() {
    List<Alignment> alignments =
        List.of(new Alignment(1, true, true, "Crusader"), new Alignment(2, null, null, "Neutral"));
    Page<Alignment> alignmentPage = new PageImpl<>(alignments);

    Mockito.when(service.findAll(page)).thenReturn(alignmentPage);
    // create collection
    CollectionModel<EntityModel<Alignment>> wraper = PagedModel.wrap(alignments,
        new PageMetadata((long) alignmentPage.getSize(), (long) alignmentPage.getNumber(),
            alignmentPage.getTotalElements(), alignmentPage.getTotalPages()));
    // create links
    wraper.forEach(e -> e.add(
        linkTo(methodOn(AlignmentController.class).getById(e.getContent().getId())).withSelfRel()));

    wraper.add(linkTo(AlignmentController.class).withSelfRel(),
        linkTo(EntryPointController.class).withRel("home"));

    ResponseEntity<?> result = controller.getAll(page.getPageNumber(), page.getPageSize(),
        Sort.Direction.ASC, "id");
    assertEquals(result.getStatusCodeValue(), 200);
    assertEquals(result.getBody(), wraper);
  }

  @DisplayName("Test fetch by id")
  @Test
  public void test_searchById() {
    Alignment alignment = new Alignment(1, true, true, "Crusader");
    EntityModel<Alignment> model =
        EntityModel.of(alignment, linkTo(AlignmentController.class).withRel("alignments"));

    when(service.findById(anyInt())).thenReturn(alignment);

    ResponseEntity<?> result = controller.getById(1);
    assertEquals(result.getBody(), model);
    assertEquals(result.getStatusCodeValue(), 200);
  }

  @DisplayName("id not found results in Exception")
  @Test
  public void test_idNotFoundCauseException() {
    when(service.findById(anyInt())).thenThrow(new EntityNotFoundException(anyInt()));
    EntityNotFoundException capture =
        assertThrows(EntityNotFoundException.class, () -> controller.getById(1));
    assertEquals("Couldn't find the entity of id #0", capture.getMessage());
  }

  @DisplayName("Name not found results in Exception")
  @Test
  public void test_nameNotFoundCauseException() {
    when(service.findByName(anyString())).thenThrow(new EntityNotFoundException(anyString()));
    EntityNotFoundException capture =
        assertThrows(EntityNotFoundException.class, () -> controller.getByName("Crusadder"));
    assertEquals("Couldn't find the entity with name ", capture.getMessage());
  }

  @DisplayName("Test fetch by name")
  @Test
  public void test_searchByName() {
    Alignment alignment = new Alignment(1, true, true, "Crusader");
    EntityModel<Alignment> model =
        EntityModel.of(alignment, linkTo(AlignmentController.class).withRel("alignments"));

    when(service.findByName(anyString())).thenReturn(alignment);

    ResponseEntity<?> result = controller.getByName("Crusader");
    assertEquals(result.getBody(), model);
    assertEquals(result.getStatusCodeValue(), 200);
  }

  @DisplayName("Test fetch totals")
  @Test
  public void test_totals() {
    when(service.count()).thenReturn(1);

    ResponseEntity<?> result = controller.totals();
    assertEquals(result.getBody(), Collections.singletonMap("total", 1));
    assertEquals(result.getStatusCodeValue(), 200);
  }

  @DisplayName("Test that throws exception for alignment not found")
  @Test
  public void test_invalidId() {
    Mockito.when(service.findById(1)).thenThrow(EntityNotFoundException.class);
    assertThrows(EntityNotFoundException.class, () -> controller.getById(1));
  }

  @DisplayName("Search cannot have empty calls")
  @Test
  public void test_invalidIdCausesException() {
    assertThrows(InvalidSearchException.class, () -> controller.getById(null));
    assertThrows(IllegalArgumentException.class, () -> controller.getByName(""));
  }

  @Nested
  @DisplayName("POST")
  class PostCalls extends AbstractValidatorTest<Alignment> {

    @AfterEach
    public void after() {
      Mockito.reset(service);
    }

    @DisplayName("Valid post call")
    @Test
    public void test_ValidPostCall() {
      Alignment input = new Alignment(null, null, null, "Test");
      when(service.save(any(Alignment.class))).thenReturn(new Alignment(2, null, null, "Test"));
      assertTrue(validate(input, OnCreate.class).isEmpty());

      var result = controller.postNewEntity(input);
      assertEquals(201, result.getStatusCodeValue());
      assertTrue(result.getHeaders().containsKey("Location"));
      assertTrue(result.getHeaders().get("Location").contains("/api/alignments/2"));
    }


    @DisplayName("Validation of invalid Class for update")
    @Test
    public void test_validationForUpdate() {
      Alignment input = new Alignment(null, null, null, "Test");
      Set<ConstraintViolation<Alignment>> violations = super.validate(input, OnUpdate.class);
      assertFalse(violations.isEmpty());
      assertEquals(violations.size(), 1, "More violations than expected");
      assertEquals("{id.update}", violations.iterator().next().getMessage());
    }

    @DisplayName("Validation of invalid Class for update")
    @Test
    public void test_validationForInvalidClassCreation() {
      Alignment input = new Alignment(2, null, null, "Test");
      Set<ConstraintViolation<Alignment>> violations = validate(input, OnCreate.class);
      assertFalse(violations.isEmpty());
      assertEquals(violations.size(), 1, "More violations than expected");
      assertEquals("{id.create}", violations.iterator().next().getMessage());
    }

    @DisplayName("Validation of valid Class for update")
    @Test
    public void test_validationForWellFormedBean() {
      Alignment input = new Alignment(2, null, null, "Test");
      assertTrue(validate(input).isEmpty());
    }
  }

  @Nested
  @DisplayName("PUT")
  class PutTests extends AbstractValidatorTest<Alignment> {

    @AfterEach
    public void after() {
      Mockito.reset(service);
    }

    @DisplayName("Valid PUT Object")
    @Test
    public void test_validPutCall() {
      var input = new Alignment(2, null, null, "Dummy");
      var response = new Alignment(2, true, null, "Test");

      EntityModel<Alignment> model = EntityModel.of(response,
          linkTo(AlignmentController.class).slash(response.getId()).withSelfRel(),
          linkTo(AlignmentController.class).withRel("alignments"));

      assertTrue(validate(input, OnUpdate.class).isEmpty());
      when(service.contains(anyInt())).thenReturn(true);
      when(service.update(any(Alignment.class))).thenReturn(new Alignment(2, true, null, "Test"));
      ResponseEntity<EntityModel<Alignment>> result = controller.put(input);

      assertEquals(HttpStatus.OK, result.getStatusCode());
      assertEquals(model.getContent().toString(), result.getBody().getContent().toString());
      assertTrue(Objects.equals(model.getLinks(), result.getBody().getLinks()));

      verify(service).contains(anyInt());
      verify(service).update(any(Alignment.class));
      verifyNoMoreInteractions(service);
    }

    @DisplayName("Creates with PUT")
    @Test
    public void test_inexistingEntityCreatesNew() {
      var input = new Alignment(2, null, null, "Test");
      var response = new Alignment(3, null, null, "Test");
      assertTrue(validate(input, OnUpdate.class).isEmpty());


      when(service.contains(anyInt())).thenReturn(false);
      when(service.update(any(Alignment.class))).thenReturn(response);
      ResponseEntity<EntityModel<Alignment>> result = controller.put(input);

      verify(service).contains(anyInt());
      verify(service).update(any(Alignment.class));
      verifyNoMoreInteractions(service);

      assertEquals(HttpStatus.CREATED, result.getStatusCode());
      assertTrue(result.getHeaders().containsKey("Location"));
      assertTrue(result.getHeaders().get("Location").contains("/api/alignments/3"));
    }

    @DisplayName("Invalid update call for no id entity")
    @Test
    public void test_invalidUpdate() {
      var response = validate(new Alignment(null, null, null, "Test"), OnUpdate.class);

      assertFalse(response.isEmpty());
      assertTrue(response.size() == 1);
      assertEquals("{id.update}", response.iterator().next().getMessage());
      verifyNoInteractions(service);
    }
  }

  @Nested
  @DisplayName("Delete")
  class DeleteCalls {

    @DisplayName("Delete works")
    @Test
    public void test_deletionWorks() {
      var response =
          EntityModel.of("Deleted!", linkTo(AlignmentController.class).withRel("alignments"),
              linkTo(EntryPointController.class).withRel("home"));
      willDoNothing().given(service).delete(any(Alignment.class));

      given(service.findById(anyInt())).willReturn(new Alignment(1, true, true, "Trash"));

      ResponseEntity<?> result = controller.delete(1);

      assertEquals(HttpStatus.OK, result.getStatusCode());
      assertEquals(result.getBody(), response);

      then(service).should(times(1)).delete(any(Alignment.class));
      then(service).shouldHaveNoMoreInteractions();
    }

    @DisplayName("Delete will fail if not found")
    @Test
    public void test_deletionFails() {
      given(service.findById(anyInt())).willThrow(new EntityNotFoundException(5));

      assertThrows(EntityNotFoundException.class, () -> controller.delete(1));

      then(service).should().findById(anyInt());
      then(service).should(times(0)).delete(any(Alignment.class));
      then(service).shouldHaveNoMoreInteractions();
    }
  }
}
