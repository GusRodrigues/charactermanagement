package com.gustavo.charMngt.web;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;
import java.util.*;
import javax.validation.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.springframework.data.domain.*;
import org.springframework.hateoas.*;
import org.springframework.hateoas.PagedModel.*;
import org.springframework.http.*;
import com.gustavo.charMngt.model.*;
import com.gustavo.charMngt.model.validation.*;
import com.gustavo.charMngt.service.*;
import com.gustavo.charMngt.service.exceptions.*;
import com.gustavo.charMngt.service.exceptions.model.*;

@ExtendWith(MockitoExtension.class)
public class RaceControllerTest {

  @InjectMocks
  private RaceController controller;

  @Mock
  private RaceService service;

  private PageRequest page = PageRequest.of(0, 20, Sort.by(Sort.Direction.ASC, "id"));

  @DisplayName("Test get all races")
  @Test
  public void test_allRacesPage() {
    List<Race> races = List.of(new Race(1, "Human"), new Race(2, "Hobbit"));
    Page<Race> pageRaces = new PageImpl<>(races);
    Mockito.when(service.findAll(page)).thenReturn(pageRaces);

    // create collection
    CollectionModel<EntityModel<Race>> wraper =
        PagedModel.wrap(races, new PageMetadata(pageRaces.getSize(), pageRaces.getNumber(),
            pageRaces.getTotalElements(), pageRaces.getTotalPages()));

    wraper.forEach(e -> e
        .add(linkTo(methodOn(RaceController.class).getById(e.getContent().getId())).withSelfRel()));

    wraper.add(linkTo(RaceController.class).withSelfRel(),
        linkTo(EntryPointController.class).withRel("home"));


    ResponseEntity<?> result = controller.getRaces(0, 20, Sort.Direction.ASC, "id");

    assertEquals(result.getStatusCodeValue(), 200);
    assertEquals(result.getBody(), wraper);
  }

  @DisplayName("Test get by id")
  @Test
  public void test_searchFunctionality() {
    Race race = new Race(1, "Human");
    when(service.findById(anyInt())).thenReturn(race);
    EntityModel<Race> model = EntityModel.of(race, linkTo(RaceController.class).withRel("races"));
    ResponseEntity<?> result = controller.getById(1);

    assertEquals(result.getStatusCodeValue(), 200);
    assertEquals(result.getBody(), model);
  }

  @DisplayName("Test fetch by name")
  @Test
  public void test_searchByName() {
    Race race = new Race(1, "Human");
    when(service.findByName(anyString())).thenReturn(race);
    EntityModel<Race> model = EntityModel.of(race, linkTo(RaceController.class).withRel("races"));

    ResponseEntity<?> result = controller.getByName("Human");
    assertEquals(result.getBody(), model);
    assertEquals(result.getStatusCodeValue(), 200);
  }

  @DisplayName("Test fetch totals")
  @Test
  public void test_totals() {
    Mockito.when(service.count()).thenReturn(1);

    EntityModel<Map<String, Integer>> response =
        EntityModel.of(Collections.singletonMap("total", service.count()),
            linkTo(RaceController.class).withRel("races"),
            linkTo(EntryPointController.class).withRel("home"));


    ResponseEntity<?> result = controller.totals();
    assertEquals(result.getBody(), response);
    assertEquals(result.getStatusCodeValue(), 200);
  }

  @DisplayName("Test that throws exception for race not found")
  @Test
  public void test_invalidId() {
    Mockito.when(service.findById(anyInt())).thenThrow(EntityNotFoundException.class);

    assertThrows(EntityNotFoundException.class, () -> controller.getById(4));
  }

  @DisplayName("test that throws exception for invalid search parameters")
  @Test
  public void test_invalidSearch() {
    assertThrows(InvalidSearchException.class, () -> controller.getByName(null));
  }

  @Nested
  @DisplayName("POST")
  class PostCalls extends AbstractValidatorTest<Race> {

    @DisplayName("Valid post call")
    @Test
    public void test_ValidPostCall() {
      Race input = new Race(2, "Test");
      input.setId(null);
      when(service.save(any(Race.class))).thenReturn(new Race(2, "Test"));
      var result = controller.postNewRace(input);

      assertEquals(201, result.getStatusCodeValue());
      assertTrue(result.getHeaders().containsKey("Location"));
      assertTrue(result.getHeaders().get("Location").contains("/api/races/2"));
    }


    @DisplayName("Validation of invalid Class for update")
    @Test
    public void test_validationForUpdate() {
      Race input = new Race(null, "Test");
      Set<ConstraintViolation<Race>> violations = super.validate(input, OnUpdate.class);
      assertFalse(violations.isEmpty());
      assertEquals(violations.size(), 1, "More violations than expected");
      assertEquals("{id.update}", violations.iterator().next().getMessage());
    }

    @DisplayName("Validation of invalid Class for update")
    @Test
    public void test_validationForInvalidClassCreation() {
      Race input = new Race(2, "Test");
      Set<ConstraintViolation<Race>> violations = super.validate(input, OnCreate.class);
      assertFalse(violations.isEmpty());
      assertEquals(violations.size(), 1, "More violations than expected");
      assertEquals("{id.create}", violations.iterator().next().getMessage());
    }

    @DisplayName("Validation of valid Class for update")
    @Test
    public void test_validationForWellFormedBean() {
      Race input = new Race(2, "Test");
      Set<ConstraintViolation<Race>> violations = super.validate(input);
      assertTrue(violations.isEmpty());
    }
  }

  @Nested
  @DisplayName("PUT")
  class PutTests extends AbstractValidatorTest<Size> {

    @AfterEach
    public void after() {
      Mockito.reset(service);
    }

    @DisplayName("Valid PUT Object")
    @Test
    public void test_validPutCall() {
      var input = new Race(2, "Test");
      var response = new Race(2, "Test");

      EntityModel<Race> model = EntityModel.of(response,
          linkTo(RaceController.class).slash(response.getId()).withSelfRel(),
          linkTo(RaceController.class).withRel("races"));

      assertTrue(validate(input, OnUpdate.class).isEmpty());
      when(service.contains(anyInt())).thenReturn(true);
      when(service.update(any(Race.class))).thenReturn(response);
      ResponseEntity<EntityModel<Race>> result = controller.put(input);

      assertEquals(HttpStatus.OK, result.getStatusCode());
      assertEquals(model.getContent().toString(), result.getBody().getContent().toString());
      assertTrue(Objects.equals(model.getLinks(), result.getBody().getLinks()));

      verify(service).contains(anyInt());
      verify(service).update(any(Race.class));
      verifyNoMoreInteractions(service);
    }

    @DisplayName("Creates with PUT")
    @Test
    public void test_inexistingEntityCreatesNew() {
      var input = new Race(2, "Test");
      var response = new Race(3, "Test");
      assertTrue(validate(input, OnUpdate.class).isEmpty());


      when(service.contains(anyInt())).thenReturn(false);
      when(service.update(any(Race.class))).thenReturn(response);
      ResponseEntity<EntityModel<Race>> result = controller.put(input);

      verify(service).contains(anyInt());
      verify(service).update(any(Race.class));
      verifyNoMoreInteractions(service);

      assertEquals(HttpStatus.CREATED, result.getStatusCode());
      assertTrue(result.getHeaders().containsKey("Location"));
      assertTrue(result.getHeaders().get("Location").contains("/api/races/3"));
    }

    @DisplayName("Invalid update call for no id entity")
    @Test
    public void test_invalidUpdate() {
      var response = validate(new Race(null, null), OnUpdate.class);

      assertFalse(response.isEmpty());
      assertTrue(response.size() == 1);
      assertEquals("{id.update}", response.iterator().next().getMessage());
      verifyNoInteractions(service);
    }
  }

  @Nested
  @DisplayName("Delete")
  class DeleteCalls {

    @DisplayName("Delete works")
    @Test
    public void test_deletionWorks() {
      var response = EntityModel.of("Deleted!", linkTo(RaceController.class).withRel("races"),
          linkTo(EntryPointController.class).withRel("home"));
      willDoNothing().given(service).delete(any(Race.class));

      given(service.findById(anyInt())).willReturn(new Race(1, "Trash"));

      ResponseEntity<?> result = controller.delete(1);

      assertEquals(HttpStatus.OK, result.getStatusCode());
      assertEquals(result.getBody(), response);

      then(service).should(times(1)).delete(any(Race.class));
      then(service).shouldHaveNoMoreInteractions();
    }

    @DisplayName("Delete will fail if not found")
    @Test
    public void test_deletionFails() {
      given(service.findById(anyInt())).willThrow(new EntityNotFoundException(5));

      assertThrows(EntityNotFoundException.class, () -> controller.delete(1));

      then(service).should().findById(anyInt());
      then(service).should(times(0)).delete(any(Race.class));
      then(service).shouldHaveNoMoreInteractions();
    }
  }
}
