package com.gustavo.charMngt.web;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;
import java.util.*;
import javax.validation.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.springframework.data.domain.*;
import org.springframework.hateoas.*;
import org.springframework.hateoas.PagedModel.*;
import org.springframework.http.*;
import com.gustavo.charMngt.model.*;
import com.gustavo.charMngt.model.validation.*;
import com.gustavo.charMngt.service.*;
import com.gustavo.charMngt.service.exceptions.*;
import com.gustavo.charMngt.service.exceptions.model.*;

@ExtendWith(MockitoExtension.class)
public class SizeControllerTest {
  @InjectMocks
  private SizeController controller;

  @Mock
  private SizeService service;

  private PageRequest page = PageRequest.of(0, 20, Sort.by(Sort.Direction.ASC, "id"));

  @DisplayName("get all returns a response entity with a list")
  @Test
  public void test_listOfSizes() {
    List<Size> sizes =
        List.of(new Size(1, "Tiny", 4), new Size(2, "Small", 6), new Size(3, "Medium", 8));
    Page<Size> pageSize = new PageImpl<>(sizes);
    
    given(service.findAll(page)).willReturn(pageSize);

    // create collection
    CollectionModel<EntityModel<Size>> wraper =
        PagedModel.wrap(sizes, new PageMetadata(pageSize.getSize(), pageSize.getNumber(),
            pageSize.getTotalElements(), pageSize.getTotalPages()));

    // create links
    wraper.forEach(e -> e
        .add(linkTo(methodOn(SizeController.class).getById(e.getContent().getId())).withSelfRel()));

    wraper.add(linkTo(SizeController.class).withSelfRel(),
        linkTo(EntryPointController.class).withRel("home"));

    ResponseEntity<?> result = controller.getSizes(0, 20, Sort.Direction.ASC, "id");

    assertEquals(result.getStatusCodeValue(), 200);
    assertEquals(result.getBody(), wraper);
  }

  @DisplayName("Test fetch by by ID")
  @Test
  public void test_sizeById() {
    Size size = new Size(1, "Tiny", 4);
    Mockito.when(service.findById(anyInt())).thenReturn(size);

    EntityModel<Size> model =
        EntityModel.of(size, linkTo(methodOn(SizeController.class).getSizes(page.getPageNumber(),
            page.getPageSize(), Sort.Direction.ASC, "id")).withRel("sizes"));

    ResponseEntity<?> result = controller.getById(anyInt());
    assertEquals(result.getStatusCodeValue(), 200);
    assertEquals(result.getBody(), model);
  }

  @DisplayName("Find by ID cause exception to be handled")
  @Test
  public void test_invalidID() {
    Mockito.when(service.findById(1)).thenThrow(new EntityNotFoundException(1));

    assertThrows(EntityNotFoundException.class, () -> controller.getById(1));

  }

  @DisplayName("Find by Name returns a response entity with the size of that name")
  @Test
  public void test_sizeByName() {
    Size size = new Size(1, "Tiny", 4);
    Mockito.when(service.findByName(anyString())).thenReturn(size);
    EntityModel<Size> model =
        EntityModel.of(size, linkTo(methodOn(SizeController.class).getSizes(page.getPageNumber(),
            page.getPageSize(), Sort.Direction.ASC, "id")).withRel("sizes"));

    ResponseEntity<?> result = controller.getByName("Tiny");
    assertEquals(result.getStatusCodeValue(), 200);
    assertEquals(result.getBody(), model);
  }

  @DisplayName("Find by Name cause exception to be handled")
  @Test
  public void test_invalidName() {
    Mockito.when(service.findByName(anyString())).thenThrow(EntityNotFoundException.class);
    assertThrows(EntityNotFoundException.class, () -> controller.getByName("Test"));
  }

  @DisplayName("Search cannot have empty calls")
  @Test
  public void test_invalidIdCausesException() {
    assertThrows(InvalidSearchException.class, () -> controller.getById(null));
    assertThrows(IllegalArgumentException.class, () -> controller.getByName(""));
  }

  @DisplayName("Provides the totals")
  @Test
  public void test_CanCountTotalEntities() {
    Mockito.when(service.count()).thenReturn(5);

    ResponseEntity<Map<String, Integer>> result = controller.totals();
    assertEquals(result.getBody(), Collections.singletonMap("total", 5));
    assertEquals(result.getStatusCodeValue(), 200);
  }

  @Nested
  @DisplayName("POST")
  class PostCalls extends AbstractValidatorTest<Size> {

    @DisplayName("Valid post call")
    @Test
    public void test_ValidPostCall() {
      Size input = new Size(2, "Test", 20);
      input.setId(null);
      when(service.save(any(Size.class))).thenReturn(new Size(2, "Test", 20));
      var result = controller.postNewSize(input);

      assertEquals(201, result.getStatusCodeValue());
      assertTrue(result.getHeaders().containsKey("Location"));
      assertTrue(result.getHeaders().get("Location").contains("/api/sizes/2"));
    }

    @DisplayName("Validation of invalid Class for update")
    @Test
    public void test_validationForUpdate() {
      Size input = new Size(null, "Test", 20);
      Set<ConstraintViolation<Size>> violations = super.validate(input, OnUpdate.class);
      assertFalse(violations.isEmpty());
      assertEquals(violations.size(), 1, "More violations than expected");
      assertEquals("{id.update}", violations.iterator().next().getMessage());
    }

    @DisplayName("Validation of invalid Class for update")
    @Test
    public void test_validationForInvalidClassCreation() {
      Size input = new Size(2, "Test", 20);
      Set<ConstraintViolation<Size>> violations = super.validate(input, OnCreate.class);
      assertFalse(violations.isEmpty());
      assertEquals(violations.size(), 1, "More violations than expected");
      assertEquals("{id.create}", violations.iterator().next().getMessage());
    }

    @DisplayName("Validation of valid Class for update")
    @Test
    public void test_validationForWellFormedBean() {
      Size input = new Size(2, "Test", 20);
      Set<ConstraintViolation<Size>> violations = super.validate(input);
      assertTrue(violations.isEmpty());
    }
  }

  @Nested
  @DisplayName("PUT")
  class PutTests extends AbstractValidatorTest<Size> {

    @AfterEach
    public void after() {
      Mockito.reset(service);
    }

    @DisplayName("Valid PUT Object")
    @Test
    public void test_validPutCall() {
      var input = new Size(2, "Test", 20);
      var response = new Size(2, "Test", 8);

      EntityModel<Size> model = EntityModel.of(response,
          linkTo(SizeController.class).slash(response.getId()).withSelfRel(),
          linkTo(SizeController.class).withRel("sizes"));

      assertTrue(validate(input, OnUpdate.class).isEmpty());
      when(service.contains(anyInt())).thenReturn(true);
      when(service.update(any(Size.class))).thenReturn(response);
      ResponseEntity<EntityModel<Size>> result = controller.put(input);

      assertEquals(HttpStatus.OK, result.getStatusCode());
      assertEquals(model.getContent().toString(), result.getBody().getContent().toString());
      assertTrue(Objects.equals(model.getLinks(), result.getBody().getLinks()));

      verify(service).contains(anyInt());
      verify(service).update(any(Size.class));
      verifyNoMoreInteractions(service);
    }

    @DisplayName("Creates with PUT")
    @Test
    public void test_inexistingEntityCreatesNew() {
      var input = new Size(2, "Test", 20);
      var response = new Size(3, "Test", 20);
      assertTrue(validate(input, OnUpdate.class).isEmpty());

      when(service.contains(anyInt())).thenReturn(false);
      when(service.update(any(Size.class))).thenReturn(response);
      ResponseEntity<EntityModel<Size>> result = controller.put(input);

      verify(service).contains(anyInt());
      verify(service).update(any(Size.class));
      verifyNoMoreInteractions(service);

      assertEquals(HttpStatus.CREATED, result.getStatusCode());
      assertTrue(result.getHeaders().containsKey("Location"));
      assertTrue(result.getHeaders().get("Location").contains("/api/sizes/3"));
    }

    @DisplayName("Invalid update call for no id entity")
    @Test
    public void test_invalidUpdate() {
      var response = validate(new Size(null, null, null), OnUpdate.class);

      assertFalse(response.isEmpty());
      assertTrue(response.size() == 1);
      assertEquals("{id.update}", response.iterator().next().getMessage());
      verifyNoInteractions(service);
    }
  }

  @Nested
  @DisplayName("Delete")
  class DeleteCalls {

    @DisplayName("Delete works")
    @Test
    public void test_deletionWorks() {
      var response = EntityModel.of("Deleted!", linkTo(SizeController.class).withRel("sizes"),
          linkTo(EntryPointController.class).withRel("home"));
      willDoNothing().given(service).delete(any(Size.class));

      given(service.findById(anyInt())).willReturn(new Size(1, "Trash", 8));

      ResponseEntity<?> result = controller.delete(1);

      assertEquals(HttpStatus.OK, result.getStatusCode());
      assertEquals(result.getBody(), response);

      then(service).should(times(1)).delete(any(Size.class));
      then(service).shouldHaveNoMoreInteractions();
    }

    @DisplayName("Delete will fail if not found")
    @Test
    public void test_deletionFails() {
      given(service.findById(anyInt())).willThrow(new EntityNotFoundException(5));

      assertThrows(EntityNotFoundException.class, () -> controller.delete(1));

      then(service).should().findById(anyInt());
      then(service).should(times(0)).delete(any(Size.class));
      then(service).shouldHaveNoMoreInteractions();
    }
  }
}
