package com.gustavo.charMngt.web;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;
import java.util.*;
import javax.validation.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.springframework.data.domain.*;
import org.springframework.hateoas.*;
import org.springframework.hateoas.PagedModel.*;
import org.springframework.http.*;
import com.gustavo.charMngt.model.*;
import com.gustavo.charMngt.model.Character;
import com.gustavo.charMngt.model.validation.*;
import com.gustavo.charMngt.service.*;
import com.gustavo.charMngt.service.exceptions.model.*;

@ExtendWith(MockitoExtension.class)
public class CharacterControllerTest {
  @Mock
  private CharacterService service;

  @InjectMocks
  private CharacterController controller;

  private PageRequest page = PageRequest.of(0, 20, Sort.by(Sort.Direction.ASC, "id"));

  private static Character defaultChar() {
    Character character = new Character();
    character.setId(1);
    character.setAlignment(new Alignment(1, true, true, "Crusader"));
    character.setJob(new Job(1, "Paladin"));
    character.setSize(new Size(1, "Medium", 8));
    character.setRace(new Race(1, "Human"));
    character.setName("Dummy");
    character.setPlayable(true);
    character.setExperiencePoint(0);
    character.setGender(false);
    character.setAttributes(new Attribute(10, 10, 10, 10, 10, 10));
    return character;
  }

  @DisplayName("Display how many entities saved")
  @Test
  public void test_Totals() {
    when(service.count()).thenReturn(5);

    ResponseEntity<Map<String, Integer>> result = controller.totals();
    assertEquals(result.getBody(), Collections.singletonMap("total", 5));
  }

  @DisplayName("Display an entity")
  @Test
  public void test_CanReturnByID() {
    when(service.findById(anyInt())).thenReturn(defaultChar());
    ResponseEntity<EntityModel<Character>> result = controller.getById(1);
    assertEquals(200, result.getStatusCodeValue());
    assertEquals(1, result.getBody().getContent().getId());
    assertEquals("Dummy", result.getBody().getContent().getName());
    assertEquals("Paladin", result.getBody().getContent().getJob().getName());
    assertEquals(false, result.getBody().getContent().getGender());
    Links links = result.getBody().getLinks();

    assertEquals(links.getLink("characters").get().getRel(), LinkRelation.of("characters"));
  }

  @DisplayName("Can fetch a page")
  @Test
  public void test_PageOfChars() {
    List<Character> charList = List.of(defaultChar());
    Page<Character> charPage = new PageImpl<>(charList);

    CollectionModel<EntityModel<Character>> wrapped =
        PagedModel.wrap(charList, new PageMetadata(charPage.getSize(), charPage.getNumber(),
            charPage.getTotalElements(), charPage.getTotalPages()));

    wrapped.forEach(
        e -> e.add(linkTo(CharacterController.class).slash(e.getContent().getId()).withSelfRel(),
            linkTo(CharacterController.class).slash(e.getContent().getId()).slash("alignment")
                .withRel("alignment"),
            linkTo(CharacterController.class).slash(e.getContent().getId()).slash("class")
                .withRel("class"),
            linkTo(CharacterController.class).slash(e.getContent().getId()).slash("race")
                .withRel("race"),
            linkTo(CharacterController.class).slash(e.getContent().getId()).slash("size")
                .withRel("size")));


    wrapped.add(linkTo(CharacterController.class).withSelfRel(),
        linkTo(EntryPointController.class).withRel("home"));

    when(service.findAll(page)).thenReturn(charPage);

    ResponseEntity<PagedModel<EntityModel<Character>>> result =
        controller.getAll(0, 20, Sort.Direction.ASC, "id");
    assertEquals(200, result.getStatusCodeValue());
    assertEquals(wrapped, result.getBody());
  }

  @Nested
  @DisplayName("POST")
  class PostCalls extends AbstractValidatorTest<Character> {

    @DisplayName("Valid post call")
    @Test
    public void test_ValidPostCall() {
      var input = defaultChar();
      input.setId(null);
      var violations = super.validate(input, OnCreate.class);
      assertTrue(violations.isEmpty());

      when(service.save(any(Character.class))).thenReturn(defaultChar());
      var result = controller.postNewChar(defaultChar());
      assertEquals(201, result.getStatusCodeValue());
      assertTrue(result.getHeaders().containsKey("Location"));
      assertTrue(result.getHeaders().get("Location").contains("/api/characters/1"));
    }


    @DisplayName("Validation of invalid Class for update")
    @Test
    public void test_validationForUpdate() {
      Character input = defaultChar();
      input.setId(null);
      Set<ConstraintViolation<Character>> violations = super.validate(input, OnUpdate.class);
      assertFalse(violations.isEmpty());
      assertEquals(violations.size(), 1, "More violations than expected");
      assertEquals("{id.update}", violations.iterator().next().getMessage());
    }

    @DisplayName("Validation of invalid Class for creation")
    @Test
    public void test_validationForInvalidClassCreation() {
      Character input = defaultChar();
      Set<ConstraintViolation<Character>> violations = super.validate(input, OnCreate.class);
      assertFalse(violations.isEmpty());
      assertEquals(violations.size(), 1, "More violations than expected");
      assertEquals("{id.create}", violations.iterator().next().getMessage());
    }

    @DisplayName("Validation of valid Class for update")
    @Test
    public void test_validationForWellFormedBean() {
      Character input = defaultChar();
      Set<ConstraintViolation<Character>> violations = super.validate(input);
      assertTrue(violations.isEmpty());
    }
  }

  @Nested
  @DisplayName("PUT tests")
  class PutCalls extends AbstractValidatorTest<Character> {

    @AfterEach
    public void after() {
      Mockito.reset(service);
    }

    @DisplayName("Valid PUT Object")
    @Test
    public void test_validPutCall() {
      var input = defaultChar();
      assertTrue(validate(input, OnUpdate.class).isEmpty());

      var response = defaultChar();
      EntityModel<Character> model = EntityModel.of(response,
          linkTo(CharacterController.class).slash(response.getId()).withSelfRel(),
          linkTo(CharacterController.class).withRel("characters"));

      when(service.contains(anyInt())).thenReturn(true);
      when(service.update(any(Character.class))).thenReturn(response);

      ResponseEntity<EntityModel<Character>> result = controller.put(input);

      assertEquals(HttpStatus.OK, result.getStatusCode());
      assertEquals(model.getContent().toString(), result.getBody().getContent().toString());
      assertTrue(Objects.equals(model.getLinks(), result.getBody().getLinks()));

      verify(service).contains(anyInt());
      verify(service).update(any(Character.class));
      verifyNoMoreInteractions(service);
    }

    @DisplayName("Creates with PUT")
    @Test
    public void test_inexistingEntityCreatesNew() {
      var input = defaultChar();
      var response = defaultChar();
      response.setId(3);
      assertTrue(validate(input, OnUpdate.class).isEmpty());


      when(service.contains(anyInt())).thenReturn(false);
      when(service.update(any(Character.class))).thenReturn(response);
      ResponseEntity<EntityModel<Character>> result = controller.put(input);

      verify(service).contains(anyInt());
      verify(service).update(any(Character.class));
      verifyNoMoreInteractions(service);

      assertEquals(HttpStatus.CREATED, result.getStatusCode());
      assertTrue(result.getHeaders().containsKey("Location"));
      assertTrue(result.getHeaders().get("Location").contains("/api/characters/3"));
    }

    @DisplayName("Invalid update call for no id entity")
    @Test
    public void test_invalidUpdate() {
      Character toValidate = defaultChar();
      toValidate.setId(null);
      var response = validate(toValidate, OnUpdate.class);

      assertFalse(response.isEmpty());
      assertTrue(response.size() == 1);
      assertEquals("{id.update}", response.iterator().next().getMessage());
      verifyNoInteractions(service);
    }
  }

  @Nested
  @DisplayName("Delete")
  class DeleteCalls {

    @DisplayName("Delete works")
    @Test
    public void test_deletionWorks() {
      var response =
          EntityModel.of("Deleted!", linkTo(CharacterController.class).withRel("characters"),
              linkTo(EntryPointController.class).withRel("home"));
      willDoNothing().given(service).delete(any(Character.class));

      given(service.findById(anyInt())).willReturn(defaultChar());

      ResponseEntity<?> result = controller.delete(1);

      assertEquals(HttpStatus.OK, result.getStatusCode());
      assertEquals(result.getBody(), response);

      then(service).should(times(1)).delete(any(Character.class));
      then(service).shouldHaveNoMoreInteractions();
    }

    @DisplayName("Delete will fail if not found")
    @Test
    public void test_deletionFails() {
      given(service.findById(anyInt())).willThrow(new EntityNotFoundException(5));

      assertThrows(EntityNotFoundException.class, () -> controller.delete(1));

      then(service).should().findById(anyInt());
      then(service).should(times(0)).delete(any(Character.class));
      then(service).shouldHaveNoMoreInteractions();
    }
  }

  // TODO Review the search functionality
  @Nested
  @Disabled
  @DisplayName("Access by ID subtypes")
  class CharSubTypes {
    //
    // @DisplayName("Test alignment from id")
    // @Test
    // public void test_IDAlignment() {
    // when(service.findById(anyInt())).thenReturn(defaultChar());
    //
    // ResponseEntity<EntityModel<Alignment>> result = controller.getAlignmentByCharId(1);
    // EntityModel<Alignment> body = result.getBody();
    // Links links = body.getLinks();
    //
    // assertEquals(result.getStatusCodeValue(), 200);
    // assertEquals(body.getContent().getId(), 1);
    // assertEquals(Optional.of(body.getContent().getGood()).get(), true);
    // assertEquals(Optional.of(body.getContent().getLawful()).get(), true);
    // assertEquals(body.getContent().getName(), "Crusader");
    //
    // assertTrue(links.getLink("self").get().getHref().contains("/api/characters/1"));
    // assertEquals(links.getLink("self").get().getRel(), LinkRelation.of("self"));
    // assertEquals(links.getLink("characters").get().getRel(), LinkRelation.of("characters"));
    // assertEquals(links.getLink("alignment").get().getRel(), LinkRelation.of("alignment"));
    // assertTrue(links.getLink("alignment").get().getHref().contains("/api/alignments/1"));
    // assertEquals(links.getLink("alignments").get().getRel(), LinkRelation.of("alignments"));
    // }
    //
    // @DisplayName("Test Class from id")
    // @Test
    // public void test_IDClass() {
    // when(service.findById(anyInt())).thenReturn(defaultChar());
    //
    // ResponseEntity<EntityModel<Job>> result = controller.getJobByCharId(1);
    // EntityModel<Job> body = result.getBody();
    // Links links = body.getLinks();
    //
    // assertEquals(result.getStatusCodeValue(), 200);
    // assertEquals(body.getContent().getId(), 1);
    // assertEquals(body.getContent().getName(), "Paladin");
    //
    // assertTrue(links.getLink("self").get().getHref().contains("/api/characters/1"));
    // assertEquals(links.getLink("self").get().getRel(), LinkRelation.of("self"));
    // assertEquals(links.getLink("characters").get().getRel(), LinkRelation.of("characters"));
    // assertEquals(links.getLink("class").get().getRel(), LinkRelation.of("class"));
    // assertTrue(links.getLink("class").get().getHref().contains("/api/classes/1"));
    // assertEquals(links.getLink("classes").get().getRel(), LinkRelation.of("classes"));
    // }
    //
    // @DisplayName("Test Race from id")
    // @Test
    // public void test_IDRace() {
    // when(service.findById(anyInt())).thenReturn(defaultChar());
    //
    // ResponseEntity<EntityModel<Race>> result = controller.getRaceByCharId(1);
    // EntityModel<Race> body = result.getBody();
    // Links links = body.getLinks();
    //
    // assertEquals(result.getStatusCodeValue(), 200);
    // assertEquals(body.getContent().getId(), 1);
    // assertEquals(body.getContent().getRace(), "Human");
    // ;
    //
    // assertEquals(links.getLink("self").get().getRel(), LinkRelation.of("self"));
    // assertTrue(links.getLink("self").get().getHref().contains("/api/characters/1"));
    // assertEquals(links.getLink("characters").get().getRel(), LinkRelation.of("characters"));
    // assertEquals(links.getLink("race").get().getRel(), LinkRelation.of("race"));
    // assertTrue(links.getLink("race").get().getHref().contains("/api/races/1"));
    // assertEquals(links.getLink("races").get().getRel(), LinkRelation.of("races"));
    // }
    //
    // @DisplayName("Test Size from id")
    // @Test
    // public void test_IDSize() {
    // when(service.findById(anyInt())).thenReturn(defaultChar());
    //
    // ResponseEntity<EntityModel<Size>> result = controller.getSizeByCharId(1);
    // EntityModel<Size> body = result.getBody();
    // Links links = body.getLinks();
    //
    // assertEquals(result.getStatusCodeValue(), 200);
    // assertEquals(body.getContent().getId(), 1);
    // assertEquals(body.getContent().getName(), "Medium");
    // assertEquals(body.getContent().getHitDice(), 8);
    //
    // assertEquals(links.getLink("self").get().getRel(), LinkRelation.of("self"));
    // assertTrue(links.getLink("self").get().getHref().contains("/api/characters/1"));
    // assertEquals(links.getLink("characters").get().getRel(), LinkRelation.of("characters"));
    // assertEquals(links.getLink("size").get().getRel(), LinkRelation.of("size"));
    // assertTrue(links.getLink("size").get().getHref().contains("/api/sizes/1"));
    // assertEquals(links.getLink("sizes").get().getRel(), LinkRelation.of("sizes"));
    // }
  }

  @Nested
  @Disabled
  @DisplayName("Search by race")
  class SearchByRace {
    //
    // @DisplayName("Generate a list of chars by race id")
    // @Test
    // public void test_ListOfCharByRaceId() {
    // Mockito.when(service.findByRace(Optional.of(1),
    // Optional.empty())).thenReturn(List.of(defaultChar()));
    //
    // ResponseEntity<Map<String, List<Character>>> result = controller.getByRace(Optional.of(1),
    // Optional.empty());
    // assertEquals(200, result.getStatusCodeValue());
    // assertEquals(1, result.getBody().get("characters").size());
    // assertEquals(1, result.getBody().get("characters").get(0).getId());
    // assertEquals("Dummy", result.getBody().get("characters").get(0).getName());
    // assertEquals("Paladin", result.getBody().get("characters").get(0).getJob().getName());
    // }
    //
    // @DisplayName("Generate a list of chars by race name")
    // @Test
    // public void test_ListOfCharByRaceName() {
    // Mockito.when(service.findByRace(Optional.empty(),
    // Optional.of("Human"))).thenReturn(List.of(defaultChar()));
    //
    // ResponseEntity<Map<String, List<Character>>> result = controller.getByRace(Optional.empty(),
    // Optional.of("Human"));
    // assertEquals(200, result.getStatusCodeValue());
    // assertEquals(1, result.getBody().get("characters").size());
    // assertEquals(1, result.getBody().get("characters").get(0).getId());
    // assertEquals("Dummy", result.getBody().get("characters").get(0).getName());
    // assertEquals("Paladin", result.getBody().get("characters").get(0).getJob().getName());
    // }
    //
    // @DisplayName("No race Id or Name cause exception being thrown")
    // @Test
    // public void test_invalidSearchByRaceShouldCauseInvalidSearchExceptionThrown() {
    // assertThrows(InvalidSearchException.class, () -> controller.getByRace(Optional.empty(),
    // Optional.empty()));
    // }
  }

  @Nested
  @Disabled
  @DisplayName("Search by Size")
  class SearchBySize {
    //
    // @DisplayName("Generate a list of chars by Size id")
    // @Test
    // public void test_ListOfCharBySizeId() {
    // Mockito.when(service.findBySize(Optional.of(1), Optional.empty(), Optional.empty()))
    // .thenReturn(List.of(defaultChar()));
    //
    // ResponseEntity<Map<String, List<Character>>> result = controller.getBySize(Optional.of(1),
    // Optional.empty(),
    // Optional.empty());
    // assertEquals(200, result.getStatusCodeValue());
    // assertEquals(1, result.getBody().get("characters").size());
    // assertEquals(1, result.getBody().get("characters").get(0).getSize().getId());
    // assertEquals("Medium", result.getBody().get("characters").get(0).getSize().getName());
    // assertEquals(8, result.getBody().get("characters").get(0).getSize().getHitDice());
    // }
    //
    // @DisplayName("Generate a list of chars by size name")
    // @Test
    // public void test_ListOfCharBySizeName() {
    // Mockito.when(service.findBySize(Optional.empty(), Optional.of("Medium"), Optional.empty()))
    // .thenReturn(List.of(defaultChar()));
    //
    // ResponseEntity<Map<String, List<Character>>> result = controller.getBySize(Optional.empty(),
    // Optional.of("Medium"), Optional.empty());
    // assertEquals(200, result.getStatusCodeValue());
    // assertEquals(1, result.getBody().get("characters").size());
    // assertEquals(1, result.getBody().get("characters").get(0).getSize().getId());
    // assertEquals("Medium", result.getBody().get("characters").get(0).getSize().getName());
    // assertEquals(8, result.getBody().get("characters").get(0).getSize().getHitDice());
    // }
    //
    // @DisplayName("Generate a list of chars by size hit dice")
    // @Test
    // public void test_ListOfCharBySizeHitDice() {
    // Mockito.when(service.findBySize(Optional.empty(), Optional.empty(), Optional.of(8)))
    // .thenReturn(List.of(defaultChar()));
    //
    // ResponseEntity<Map<String, List<Character>>> result = controller.getBySize(Optional.empty(),
    // Optional.empty(), Optional.of(8));
    // assertEquals(200, result.getStatusCodeValue());
    // assertEquals(1, result.getBody().get("characters").size());
    // assertEquals(1, result.getBody().get("characters").get(0).getSize().getId());
    // assertEquals("Medium", result.getBody().get("characters").get(0).getSize().getName());
    // assertEquals(8, result.getBody().get("characters").get(0).getSize().getHitDice());
    // }
    //
    // @DisplayName("No size Id, or Name, or hit dice cause exception being thrown")
    // @Test
    // public void test_invalidSearchByJobShouldCauseInvalidSearchExceptionThrown() {
    // assertThrows(InvalidSearchException.class,
    // () -> controller.getBySize(Optional.empty(), Optional.empty(), Optional.empty()));
    // }
  }

  @Nested
  @Disabled
  @DisplayName("Search by Job")
  class SearchByJob {
    //
    // @DisplayName("Generate a list of chars by size id")
    // @Test
    // public void test_ListOfCharByJobId() {
    // Mockito.when(service.findByJob(Optional.of(1),
    // Optional.empty())).thenReturn(List.of(defaultChar()));
    //
    // ResponseEntity<Map<String, List<Character>>> result = controller.getByJob(Optional.of(1),
    // Optional.empty());
    // assertEquals(200, result.getStatusCodeValue());
    // assertEquals(1, result.getBody().get("characters").size());
    // assertEquals(1, result.getBody().get("characters").get(0).getId());
    // assertEquals("Dummy", result.getBody().get("characters").get(0).getName());
    // assertEquals("Paladin", result.getBody().get("characters").get(0).getJob().getName());
    // }
    //
    // @DisplayName("Generate a list of chars by race name")
    // @Test
    // public void test_ListOfCharByJobName() {
    // Mockito.when(service.findByJob(Optional.empty(),
    // Optional.of("Human"))).thenReturn(List.of(defaultChar()));
    //
    // ResponseEntity<Map<String, List<Character>>> result = controller.getByJob(Optional.empty(),
    // Optional.of("Human"));
    // assertEquals(200, result.getStatusCodeValue());
    // assertEquals(1, result.getBody().get("characters").size());
    // assertEquals(1, result.getBody().get("characters").get(0).getId());
    // assertEquals("Dummy", result.getBody().get("characters").get(0).getName());
    // assertEquals("Paladin", result.getBody().get("characters").get(0).getJob().getName());
    // }
    //
    // @DisplayName("No job Id or Name cause exception being thrown")
    // @Test
    // public void test_invalidSearchByJobShouldCauseInvalidSearchExceptionThrown() {
    // assertThrows(InvalidSearchException.class, () -> controller.getByRace(Optional.empty(),
    // Optional.empty()));
    // }
  }

  @Nested
  @DisplayName("Search by Alignment")
  class SearchByAlignment {
    //
    // @DisplayName("Generate a list of chars by alignment id")
    // @Test
    // public void test_ListOfCharByAlignmentId() {
    // Mockito.when(service.findByAlignment(Optional.of(1),
    // Optional.empty())).thenReturn(List.of(defaultChar()));
    //
    // ResponseEntity<Map<String, List<Character>>> result =
    // controller.getByAlignment(Optional.of(1),
    // Optional.empty());
    // assertEquals(200, result.getStatusCodeValue());
    // assertEquals(1, result.getBody().get("characters").size());
    // assertEquals(1, result.getBody().get("characters").get(0).getAlignment().getId());
    // assertEquals("Crusader", result.getBody().get("characters").get(0).getAlignment().getName());
    // }
    //
    // @DisplayName("Generate a list of chars by Alignment name")
    // @Test
    // public void test_ListOfCharByAlignmentName() {
    // Mockito.when(service.findByAlignment(Optional.empty(), Optional.of("Crusader")))
    // .thenReturn(List.of(defaultChar()));
    //
    // ResponseEntity<Map<String, List<Character>>> result =
    // controller.getByAlignment(Optional.empty(),
    // Optional.of("Crusader"));
    // assertEquals(200, result.getStatusCodeValue());
    // assertEquals(1, result.getBody().size());
    // assertEquals(1, result.getBody().get("characters").get(0).getAlignment().getId());
    // assertEquals("Crusader", result.getBody().get("characters").get(0).getAlignment().getName());
    // }
    //
    // @DisplayName("No matching results cause empty list")
    // @Test
    // public void test_NoMatchByAlignmentIdReturnEmptyList() {
    // Mockito.when(service.findByAlignment(Optional.of(1),
    // Optional.empty())).thenReturn(Collections.emptyList());
    //
    // ResponseEntity<Map<String, List<Character>>> result =
    // controller.getByAlignment(Optional.of(1),
    // Optional.empty());
    // assertEquals(200, result.getStatusCodeValue());
    // assertTrue(result.getBody().get("characters").isEmpty());
    // }
    //
    // @DisplayName("No job Id or Name cause exception being thrown")
    // @Test
    // public void test_invalidSearchByJobShouldCauseInvalidSearchExceptionThrown() {
    // assertThrows(InvalidSearchException.class, () -> controller.getByRace(Optional.empty(),
    // Optional.empty()));
    // }
  }
}
