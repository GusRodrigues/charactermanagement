package com.gustavo.charMngt.web;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;
import java.util.*;
import javax.validation.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.springframework.context.annotation.*;
import org.springframework.data.domain.*;
import org.springframework.hateoas.*;
import org.springframework.hateoas.PagedModel.*;
import org.springframework.http.*;
import org.springframework.validation.beanvalidation.*;
import com.gustavo.charMngt.model.*;
import com.gustavo.charMngt.model.validation.*;
import com.gustavo.charMngt.service.*;
import com.gustavo.charMngt.service.exceptions.*;
import com.gustavo.charMngt.service.exceptions.model.*;

@ExtendWith(MockitoExtension.class)
@Import(LocalValidatorFactoryBean.class)
public class JobControllerTest {


  @InjectMocks
  private JobController controller;

  @Mock
  private JobService service;

  private List<Job> jobs = List.of(new Job(1, "Fighter"), new Job(2, "Wizard"));

  private Job job = new Job(1, "Fighter");

  private PageRequest page = PageRequest.of(0, 20, Sort.by(Sort.Direction.ASC, "id"));

  @DisplayName("Test get all classes")
  @Test
  public void test_getAllClasses() {
    Page<Job> classPage = new PageImpl<>(jobs);
    when(service.findAll(page)).thenReturn(classPage);

    CollectionModel<EntityModel<Job>> wraper =
        PagedModel.wrap(jobs, new PageMetadata(classPage.getSize(), classPage.getNumber(),
            classPage.getTotalElements(), classPage.getTotalPages()));
    wraper.forEach(e -> e
        .add(linkTo(methodOn(JobController.class).getById(e.getContent().getId())).withSelfRel()));

    wraper.add(linkTo(JobController.class).withSelfRel(),
        linkTo(EntryPointController.class).withRel("home"));

    ResponseEntity<?> result = controller.getClasses(0, 20, Sort.Direction.ASC, "id");

    assertEquals(result.getStatusCodeValue(), 200);
    assertEquals(result.getBody(), wraper);
  }

  @DisplayName("Test fetch by id")
  @Test
  public void test_searchById() {
    EntityModel<Job> model = EntityModel.of(job);
    model.add(linkTo(JobController.class).withRel("classes"));
    when(service.findById(anyInt())).thenReturn(job);
    ResponseEntity<?> result = controller.getById(1);

    assertEquals(result.getStatusCodeValue(), 200);
    assertEquals(result.getBody(), model);
  }

  @DisplayName("Test fetch by name")
  @Test
  public void test_searchByName() {
    EntityModel<Job> model = EntityModel.of(job);
    model.add(linkTo(JobController.class).withRel("classes"));
    when(service.findByName(anyString())).thenReturn(job);

    ResponseEntity<?> result = controller.getByName("Fighter");
    assertEquals(result.getBody(), model);
    assertEquals(result.getStatusCodeValue(), 200);
  }

  @DisplayName("Test fetch totals")
  @Test
  public void test_totals() {
    Mockito.when(service.count()).thenReturn(1);

    ResponseEntity<Map<String, Integer>> result = controller.count();
    assertEquals(result.getBody(), Collections.singletonMap("total", 1));
    assertEquals(result.getStatusCodeValue(), 200);
  }

  @DisplayName("Test that throws exception for race not found")
  @Test
  public void test_invalidIdCausesException() {
    when(service.findById(anyInt())).thenThrow(EntityNotFoundException.class);
    assertThrows(EntityNotFoundException.class, () -> controller.getById(1));
  }

  @DisplayName("test that throws exception for invalid search parameters")
  @Test
  public void test_invalidSearch() {
    assertThrows(IllegalArgumentException.class, () -> controller.getByName("mark"));
    assertThrows(InvalidSearchException.class, () -> controller.getByName(null));
  }

  @Nested
  @DisplayName("POST")
  class PostCalls extends AbstractValidatorTest<Job> {

    @DisplayName("Valid post call")
    @Test
    public void test_ValidPostCall() {
      Job input = new Job(2, "Test");
      input.setId(null);
      when(service.save(any(Job.class))).thenReturn(new Job(2, "Test"));
      var result = controller.postNewJob(input);

      assertEquals(201, result.getStatusCodeValue());
      assertTrue(result.getHeaders().containsKey("Location"));
      assertTrue(result.getHeaders().get("Location").contains("/api/classes/2"));
    }


    @DisplayName("Validation of invalid Class for update")
    @Test
    public void test_validationForUpdate() {
      Job input = new Job(null, "Test");
      Set<ConstraintViolation<Job>> violations = super.validate(input, OnUpdate.class);
      assertFalse(violations.isEmpty());
      assertEquals(violations.size(), 1, "More violations than expected");
      assertEquals("{id.update}", violations.iterator().next().getMessage());
    }

    @DisplayName("Validation of invalid Class for update")
    @Test
    public void test_validationForInvalidClassCreation() {
      Job input = new Job(2, "Test");
      Set<ConstraintViolation<Job>> violations = super.validate(input, OnCreate.class);
      assertFalse(violations.isEmpty());
      assertEquals(violations.size(), 1, "More violations than expected");
      assertEquals("{id.create}", violations.iterator().next().getMessage());
    }

    @DisplayName("Validation of valid Class for update")
    @Test
    public void test_validationForWellFormedBean() {
      Job input = new Job(2, "Test");
      Set<ConstraintViolation<Job>> violations = super.validate(input);
      assertTrue(violations.isEmpty());
    }
  }


  @Nested
  @DisplayName("PUT")
  class PutTests extends AbstractValidatorTest<Size> {

    @AfterEach
    public void after() {
      Mockito.reset(service);
    }

    @DisplayName("Valid PUT Object")
    @Test
    public void test_validPutCall() {
      var input = new Job(2, "Test");
      var response = new Job(2, "Test");

      EntityModel<Job> model = EntityModel.of(response,
          linkTo(JobController.class).slash(response.getId()).withSelfRel(),
          linkTo(JobController.class).withRel("classes"));

      assertTrue(validate(input, OnUpdate.class).isEmpty());
      when(service.contains(anyInt())).thenReturn(true);
      when(service.update(any(Job.class))).thenReturn(response);
      ResponseEntity<EntityModel<Job>> result = controller.put(input);

      assertEquals(HttpStatus.OK, result.getStatusCode());
      assertEquals(model.getContent().toString(), result.getBody().getContent().toString());
      assertTrue(Objects.equals(model.getLinks(), result.getBody().getLinks()));

      verify(service).contains(anyInt());
      verify(service).update(any(Job.class));
      verifyNoMoreInteractions(service);
    }

    @DisplayName("Creates with PUT")
    @Test
    public void test_inexistingEntityCreatesNew() {
      var input = new Job(2, "Test");
      var response = new Job(3, "Test");
      assertTrue(validate(input, OnUpdate.class).isEmpty());


      when(service.contains(anyInt())).thenReturn(false);
      when(service.update(any(Job.class))).thenReturn(response);
      ResponseEntity<EntityModel<Job>> result = controller.put(input);

      verify(service).contains(anyInt());
      verify(service).update(any(Job.class));
      verifyNoMoreInteractions(service);

      assertEquals(HttpStatus.CREATED, result.getStatusCode());
      assertTrue(result.getHeaders().containsKey("Location"));
      assertTrue(result.getHeaders().get("Location").contains("/api/classes/3"));
    }

    @DisplayName("Invalid update call for no id entity")
    @Test
    public void test_invalidUpdate() {
      var response = validate(new Race(null, null), OnUpdate.class);

      assertFalse(response.isEmpty());
      assertTrue(response.size() == 1);
      assertEquals("{id.update}", response.iterator().next().getMessage());
      verifyNoInteractions(service);
    }
  }

  @Nested
  @DisplayName("Delete")
  class DeleteCalls {

    @DisplayName("Delete works")
    @Test
    public void test_deletionWorks() {
      var response = EntityModel.of("Deleted!", linkTo(JobController.class).withRel("classes"),
          linkTo(EntryPointController.class).withRel("home"));
      willDoNothing().given(service).delete(any(Job.class));

      given(service.findById(anyInt())).willReturn(new Job(1, "Trash"));

      ResponseEntity<?> result = controller.delete(1);

      assertEquals(HttpStatus.OK, result.getStatusCode());
      assertEquals(result.getBody(), response);

      then(service).should(times(1)).delete(any(Job.class));
      then(service).shouldHaveNoMoreInteractions();
    }

    @DisplayName("Delete will fail if not found")
    @Test
    public void test_deletionFails() {
      given(service.findById(anyInt())).willThrow(new EntityNotFoundException(5));

      assertThrows(EntityNotFoundException.class, () -> controller.delete(1));

      then(service).should().findById(anyInt());
      then(service).should(times(0)).delete(any(Job.class));
      then(service).shouldHaveNoMoreInteractions();
    }
  }
}
