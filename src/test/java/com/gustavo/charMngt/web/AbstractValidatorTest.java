package com.gustavo.charMngt.web;

import java.util.*;
import javax.validation.*;
import org.junit.jupiter.api.*;
import com.gustavo.charMngt.model.validation.*;

public abstract class AbstractValidatorTest<T> {

  private static ValidatorFactory validatorFactory;
  private static Validator validator;

  @BeforeAll
  public static void createValidator() {
    validatorFactory = Validation.buildDefaultValidatorFactory();
    validator = validatorFactory.getValidator();

  }

  @AfterAll
  public static void close() {
    validatorFactory.close();
  }

  /**
   * Default is set to OnUpdate - Needs ID Value
   * 
   * @param <T> The type
   * @param validate to be validated
   * @return A {@link Set} of {@link ConstraintViolation}, if any.
   */
  public static <T> Set<ConstraintViolation<T>> validate(T validate) {
    return validator.validate(validate, OnUpdate.class);
  }

  /**
   * Default is set to OnUpdate - Needs ID Value
   * 
   * @param <T> The type
   * @param validate to be validated
   * @param classes The group classes
   * @return A {@link Set} of {@link ConstraintViolation}, if any.
   */
  public static <T> Set<ConstraintViolation<T>> validate(T validate, Class<?>... classes) {
    return validator.validate(validate, classes);
  }
}
