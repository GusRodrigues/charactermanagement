package com.gustavo.charMngt.web;

import static org.junit.jupiter.api.Assertions.*;
import java.util.*;
import org.junit.jupiter.api.*;
import org.springframework.hateoas.*;
import org.springframework.http.*;

public class EntryPointControllerTest {

  private EntryPointController controller;

  @BeforeEach
  public void init() {
    this.controller = new EntryPointController();
  }

  @DisplayName("Test entry point")
  @Test
  public void test_EntryPoint() {

    List<String> links =
        List.of("/api/alignments", "/api/characters", "/api/classes", "/api/races", "/api/sizes");
    ResponseEntity<RepresentationModel<?>> response = controller.nodes();
    RepresentationModel<?> actual = response.getBody();

    assertTrue(actual.getLink("aligments").get().getHref().equalsIgnoreCase(links.get(0)));
    assertTrue(actual.getLink("characters").get().getHref().equalsIgnoreCase(links.get(1)));
    assertTrue(actual.getLink("classes").get().getHref().equalsIgnoreCase(links.get(2)));
    assertTrue(actual.getLink("races").get().getHref().equalsIgnoreCase(links.get(3)));
    assertTrue(actual.getLink("sizes").get().getHref().equalsIgnoreCase(links.get(4)));
    assertFalse(actual.getLinks().isEmpty());
    assertTrue(actual.getLinks().hasSize(5));
    assertEquals(response.getStatusCodeValue(), 200);
  }
}
