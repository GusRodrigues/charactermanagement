package com.gustavo.charMngt.web.integration;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.contains;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.*;
import org.junit.jupiter.api.*;
import org.mockito.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.data.domain.*;
import org.springframework.http.*;
import org.springframework.test.web.servlet.*;
import com.gustavo.charMngt.model.*;
import com.gustavo.charMngt.service.*;
import com.gustavo.charMngt.service.exceptions.model.*;
import com.gustavo.charMngt.web.*;

@WebMvcTest(RaceController.class)
public class RaceControllerIT implements JsonWriter {

  private static final String URI = "/api/races";

  @Autowired
  private MockMvc mock;

  @MockBean
  private RaceService service;

  private PageRequest page = PageRequest.of(0, 20, Sort.by(Sort.Direction.ASC, "id"));

  public static List<Race> allRacesList() {
    return List.of(new Race(1, "Dwarf"), new Race(2, "Elve"), new Race(3, "Hobbit"),
        new Race(4, "Human"));
  }

  @AfterEach
  public void after() {
    Mockito.reset(service);
  }

  @DisplayName("Show all races")
  @Test
  public void allRaces() throws Exception {
    Page<Race> racePage = new PageImpl<>(allRacesList());
    when(service.findAll(page)).thenReturn(racePage);

    mock.perform(get(URI).contentType(MediaType.APPLICATION_JSON)) // call get /races
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$._embedded.races.*", hasSize(4)))
        .andExpect(jsonPath("$._links.self.*", hasSize(1)))
        .andExpect(jsonPath("$.page.size", is(4)))
        .andExpect(jsonPath("$.page.totalElements", is(4)))
        .andExpect(jsonPath("$.page.totalPages", is(1)))
        .andExpect(jsonPath("$.page.number", is(0)));
  }

  @DisplayName("Fetch race by id")
  @Test
  public void raceById() throws Exception {
    when(service.findById(anyInt())).thenReturn(new Race(1, "Dwarf"));

    mock.perform(get(URI + "/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.race").value("Dwarf"));
  }

  @DisplayName("Fetch race by race name")
  @Test
  public void raceByName() throws Exception {
    when(service.findByName(anyString())).thenReturn(new Race(3, "Hobbit"));

    mock.perform(get(URI + "/search?race=Hobbit").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.race").value("Hobbit")).andExpect(jsonPath("$._links.*", hasSize(1)))
        .andExpect(jsonPath("$._links.races.href", is("http://localhost/api/races")));

  }

  @DisplayName("Returns 404 if Id not found")
  @Test
  public void invalidRaceId() throws Exception {
    when(service.findById(anyInt())).thenThrow(new EntityNotFoundException(1));

    mock.perform(get(URI + "/1").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }

  @DisplayName("Returns 400 if Id is invalid")
  @Test
  public void invalidId() throws Exception {
    mock.perform(get(URI + "/a").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }

  @DisplayName("Invalid search structure causes 400")
  @Test
  public void invalidSearchStructure() throws Exception {
    when(service.findByName(anyString())).thenThrow(new EntityNotFoundException(""));
    mock.perform(get(URI + "/search?mark=").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }

  @Nested
  @DisplayName("Post tests")
  class PostTest {

    @DisplayName("Invalid object return 400")
    @Test
    public void test_invalidPost() {
      Race invalid = new Race(null, "names");

      List.of(" ", "", "  ").stream().forEach(e -> {
        invalid.setRace(e);
        try {
          mock.perform(post(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(invalid)))
              .andExpect(jsonPath("$.errors[0]", is("race : must not be null or empty")))
              .andExpect(status().isBadRequest());
        } catch (Exception e1) {
          e1.printStackTrace();
        }
      });
      verifyNoInteractions(service);
    }

    @DisplayName("Objects is totally invalid results in 400")
    @Test
    public void test_invalidSizePost() throws Exception {
      Race invalid = new Race(1, null);
      mock.perform(post(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(invalid)))
          .andExpect(jsonPath("$.errors.*", hasSize(1)))
          .andExpect(jsonPath("$.errors[*]", contains("race : must not be null or empty")))
          .andExpect(status().isBadRequest());
      verifyNoInteractions(service);
    }

    @DisplayName("Objects with ID results in 400")
    @Test
    public void test_invalidPostExistingAlignment() throws Exception {
      Race invalid = new Race(1, "test");
      mock.perform(post(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(invalid)))
          .andExpect(jsonPath("$.errors[*]", containsInAnyOrder("id needs to be null")))
          .andExpect(status().isBadRequest());
      verifyNoInteractions(service);
    }

    @DisplayName("Valid Post results in 201")
    @Test
    public void test_validPostCreation() throws Exception {
      when(service.save(any(Race.class))).thenReturn(new Race(2, "test"));

      mock.perform(
          post(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(new Race(null, "test"))))
          .andExpect(status().isCreated())
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(jsonPath("$.race", is("test"))).andExpect(header().exists("Location"))
          .andExpect(redirectedUrl("http://localhost/api/races/2"));

      verify(service).save(any(Race.class));
      verifyNoMoreInteractions(service);
    }
  }

  @Nested
  @DisplayName("Put tests")
  class PutTest {

    @DisplayName("Valid Post results OK for update")
    @Test
    public void test_validPutUpdate() throws Exception {
      Race valid = new Race(2, "test");
      when(service.contains(anyInt())).thenReturn(true);
      when(service.update(any(Race.class))).thenReturn(valid);

      mock.perform(put(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(valid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isOk()).andExpect(jsonPath("$.race", is("test")))
          .andExpect(jsonPath("$._links.*", hasSize(2)));

      verify(service).update(any(Race.class));
      verify(service).contains(anyInt());
      verifyNoMoreInteractions(service);

    }

    @DisplayName("Valid Post results in CREATED for update without entity")
    @Test
    public void test_validPutCreate() throws Exception {
      Race valid = new Race(2, "Updated");
      when(service.contains(anyInt())).thenReturn(false);
      when(service.update(any(Race.class))).thenReturn(new Race(5, "Updated"));

      mock.perform(put(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(valid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isCreated()).andExpect(jsonPath("$.race", is("Updated")))
          .andExpect(header().exists("Location"))
          .andExpect(redirectedUrl("http://localhost/api/races/5"))
          .andExpect(jsonPath("$._links.*", hasSize(1)));

      verify(service).update(any(Race.class));
      verify(service).contains(anyInt());
      verifyNoMoreInteractions(service);

    }

    @DisplayName("invalid Put object results in BADREQUEST")
    @Test
    public void test_invalidPutCauses400() throws Exception {
      Race invalid = new Race(null, null);

      mock.perform(put(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(invalid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.errors[0]", is("race : must not be null or empty")));

      verifyNoInteractions(service);
      verifyNoMoreInteractions(service);

    }

    @DisplayName("update ignore ID if new is created")
    @Test
    public void update_ignoresId() throws Exception {
      Race valid = new Race(3, "Test");

      when(service.contains(anyInt())).thenReturn(false);
      when(service.update(any(Race.class))).thenReturn(new Race(5, "Test"));

      mock.perform(put(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(valid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isCreated()).andExpect(jsonPath("$.race", is("Test")))
          .andExpect(header().exists("Location"))
          .andExpect(redirectedUrl("http://localhost/api/races/5"))
          .andExpect(jsonPath("$._links.*", hasSize(1)));

      verify(service).contains(anyInt());
      verify(service).update(any(Race.class));
      verifyNoMoreInteractions(service);
    }
  }

  @Nested
  @DisplayName("Deletion tests")
  class DeleteTest {

    @DisplayName("Can Delete valid entity")
    @Test
    public void test_validDeletion() throws Exception {
      var valid = new Race(1, "Test");
      given(service.findById(1)).willReturn(valid);
      willDoNothing().given(service).delete(any(Race.class));

      mock.perform(delete(URI + "/1").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk())
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(jsonPath("$._links.*", hasSize(2)))
          .andExpect(jsonPath("$._links.home.*", hasSize(1)))
          .andExpect(jsonPath("$.content", is("Deleted!")));

      then(service).should().findById(anyInt());
      then(service).should().delete(any(Race.class));
      then(service).shouldHaveNoMoreInteractions();
    }

    @DisplayName("Can't delete job that doesn't exists")
    @Test
    public void test_invalidDeletion() throws Exception {
      given(service.findById(anyInt())).willThrow(new EntityNotFoundException(1));


      mock.perform(delete(URI + "/1").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isNotFound())
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(jsonPath("$.message", is("Couldn't find the entity of id #1")));

      then(service).should().findById(anyInt());
      then(service).shouldHaveNoMoreInteractions();
    }
  }
}
