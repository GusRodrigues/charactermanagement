package com.gustavo.charMngt.web.integration;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import java.util.*;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.data.domain.*;
import org.springframework.http.*;
import org.springframework.test.web.servlet.*;
import com.gustavo.charMngt.model.*;
import com.gustavo.charMngt.service.*;
import com.gustavo.charMngt.service.exceptions.model.*;
import com.gustavo.charMngt.web.*;

@WebMvcTest(JobController.class)
public class JobControllerIT implements JsonWriter {

  private final static String URI = "/api/classes";

  @Autowired
  private MockMvc mock;

  @MockBean
  private JobService service;

  private static List<Job> allJobs() {
    return List.of(new Job(1, "Barbarian"), new Job(2, "Fighter"), new Job(3, "Monk"),
        new Job(4, "Ranger"));
  }

  private PageRequest page = PageRequest.of(0, 20, Sort.by(Sort.Direction.ASC, "id"));

  @AfterEach
  public void after() {
    reset(service);
  }

  @DisplayName("List all classes")
  @Test
  public void testGameClass() throws Exception {
    Page<Job> classesPage = new PageImpl<>(allJobs());
    when(service.findAll(page)).thenReturn(classesPage);

    mock.perform(get(URI).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$._embedded.classes.*", hasSize(4)))
        .andExpect(jsonPath("$._links.*", hasSize(2))).andExpect(jsonPath("$.page.size", is(4)))
        .andExpect(jsonPath("$.page.totalElements", is(4)))
        .andExpect(jsonPath("$.page.totalPages", is(1)))
        .andExpect(jsonPath("$.page.number", is(0)));
  }

  @DisplayName("Display a valid class from id")
  @Test
  public void test_validIdReturnClass() throws Exception {
    when(service.findById(anyInt())).thenReturn(new Job(1, "Barbarian"));

    mock.perform(get(URI + "/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.name").value("Barbarian"));
  }

  @DisplayName("Display a valid class from a name")
  @Test
  public void test_validNameReturnClass() throws Exception {
    when(service.findByName(anyString())).thenReturn(new Job(1, "Barbarian"));

    mock.perform(get(URI + "/search?name=Barbarian").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.name").value("Barbarian"));
  }

  @DisplayName("Invalid ID cause 404")
  @Test
  public void test_invalidIdCause404() throws Exception {
    // mocks an invalid ID.
    when(service.findById(anyInt())).thenThrow(new EntityNotFoundException(1));

    mock.perform(get(URI + "/1").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }

  @DisplayName("Invalid Name cause 404")
  @Test
  public void test_invalidNameCause404() throws Exception {
    // mocks an invalid ID.
    when(service.findByName(anyString())).thenThrow(new EntityNotFoundException(1));

    mock.perform(get(URI + "/search?name=1").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }

  @DisplayName("Invalid land page cause 400")
  @Test
  public void test_invalidGetCause404() throws Exception {
    mock.perform(get(URI + "/asdf").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }

  @Nested
  @DisplayName("Post tests")
  class PostTest {

    @DisplayName("Invalid object return 400")
    @Test
    public void test_invalidPost() {
      Job invalid = new Job(null, "names");

      List.of(" ", "", "  ").stream().forEach(e -> {
        invalid.setName(e);
        try {
          mock.perform(
              post(URI + "").contentType(MediaType.APPLICATION_JSON).content(asJson(invalid)))
              .andExpect(jsonPath("$.errors[0]", is("name : must not be null or empty")))
              .andExpect(status().isBadRequest());
        } catch (Exception e1) {
          e1.printStackTrace();
        }

      });
      verifyNoInteractions(service);
    }

    @DisplayName("Objects with ID results in 400")
    @Test
    public void test_invalidPostExistingAlignment() throws Exception {
      Job invalid = new Job(1, "test");
      mock.perform(post(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(invalid)))
          .andExpect(jsonPath("$.errors[*]", containsInAnyOrder("id needs to be null")))
          .andExpect(status().isBadRequest());
      verifyNoInteractions(service);
    }

    @DisplayName("Valid Post results in 201")
    @Test
    public void test_validPostCreation() throws Exception {
      Job valid = new Job(null, "test");
      when(service.save(any(Job.class))).thenReturn(new Job(2, "test"));

      mock.perform(post(URI + "").contentType(MediaType.APPLICATION_JSON).content(asJson(valid)))
          .andExpect(status().isCreated())
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(jsonPath("$.name", is("test"))).andExpect(header().exists("Location"))
          .andExpect(redirectedUrl("http://localhost/api/classes/2"));

      verify(service).save(any(Job.class));
      verifyNoMoreInteractions(service);

    }
  }


  @Nested
  @DisplayName("Put tests")
  class PutTest {

    @DisplayName("Valid Post results OK for update")
    @Test
    public void test_validPutUpdate() throws Exception {
      var valid = new Job(2, "test");
      when(service.contains(anyInt())).thenReturn(true);
      when(service.update(any(Job.class))).thenReturn(valid);

      mock.perform(put(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(valid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isOk()).andExpect(jsonPath("$.name", is("test")))
          .andExpect(jsonPath("$._links.*", hasSize(2)));

      verify(service).update(any(Job.class));
      verify(service).contains(anyInt());
      verifyNoMoreInteractions(service);

    }

    @DisplayName("Valid Post results in CREATED for update without entity")
    @Test
    public void test_validPutCreate() throws Exception {
      var valid = new Job(2, "Updated");
      when(service.contains(anyInt())).thenReturn(false);
      when(service.update(any(Job.class))).thenReturn(new Job(5, "Updated"));

      mock.perform(put(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(valid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isCreated()).andExpect(jsonPath("$.name", is("Updated")))
          .andExpect(header().exists("Location"))
          .andExpect(redirectedUrl("http://localhost/api/classes/5"))
          .andExpect(jsonPath("$._links.*", hasSize(1)));

      verify(service).update(any(Job.class));
      verify(service).contains(anyInt());
      verifyNoMoreInteractions(service);

    }

    @DisplayName("invalid Put object results in BADREQUEST")
    @Test
    public void test_invalidPutCauses400() throws Exception {
      var invalid = new Job(null, null);

      mock.perform(put(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(invalid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.errors[0]", is("name : must not be null or empty")));

      verifyNoInteractions(service);
      verifyNoMoreInteractions(service);

    }

    @DisplayName("update ignore ID if new is created")
    @Test
    public void update_ignoresId() throws Exception {
      var valid = new Job(3, "Test");

      when(service.contains(anyInt())).thenReturn(false);
      when(service.update(any(Job.class))).thenReturn(new Job(5, "Test"));

      mock.perform(put(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(valid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isCreated()).andExpect(jsonPath("$.name", is("Test")))
          .andExpect(header().exists("Location"))
          .andExpect(redirectedUrl("http://localhost/api/classes/5"))
          .andExpect(jsonPath("$._links.*", hasSize(1)));

      verify(service).contains(anyInt());
      verify(service).update(any(Job.class));
      verifyNoMoreInteractions(service);
    }
  }

  @Nested
  @DisplayName("Deletion tests")
  class DeleteTest {

    @DisplayName("Can Delete valid entity")
    @Test
    public void test_validDeletion() throws Exception {
      var valid = new Job(1, "Test");
      given(service.findById(1)).willReturn(valid);
      willDoNothing().given(service).delete(any(Job.class));

      mock.perform(delete(URI + "/1").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk())
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(jsonPath("$._links.*", hasSize(2)))
          .andExpect(jsonPath("$._links.home.*", hasSize(1)))
          .andExpect(jsonPath("$.content", is("Deleted!")));

      then(service).should().findById(anyInt());
      then(service).should().delete(any(Job.class));
      then(service).shouldHaveNoMoreInteractions();
    }

    @DisplayName("Can't delete job that doesn't exists")
    @Test
    public void test_invalidDeletion() throws Exception {
      given(service.findById(anyInt())).willThrow(new EntityNotFoundException(1));


      mock.perform(delete(URI + "/1").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isNotFound())
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(jsonPath("$.message", is("Couldn't find the entity of id #1")));

      then(service).should().findById(anyInt());
      then(service).shouldHaveNoMoreInteractions();
    }
  }
}
