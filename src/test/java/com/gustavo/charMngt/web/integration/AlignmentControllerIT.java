package com.gustavo.charMngt.web.integration;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import java.util.*;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.data.domain.*;
import org.springframework.http.*;
import org.springframework.test.web.servlet.*;
import com.gustavo.charMngt.model.*;
import com.gustavo.charMngt.service.*;
import com.gustavo.charMngt.service.exceptions.model.*;
import com.gustavo.charMngt.web.*;


@WebMvcTest(AlignmentController.class)
public class AlignmentControllerIT implements JsonWriter {

  private final static String URI = "/api/alignments";

  @Autowired
  private MockMvc mock;

  @MockBean
  private AlignmentService service;

  private PageRequest page = PageRequest.of(0, 20, Sort.by(Sort.Direction.ASC, "id"));

  @AfterEach
  public void after() {
    reset(service);
  }

  @DisplayName("Display all alignment")
  @Test
  public void test_ControllerDisplayAll() throws Exception {
    when(service.findAll(page)).thenReturn(new PageImpl<>(
        List.of(new Alignment(1, true, true, "Crusader"), new Alignment(2, null, true, "Judge"))));

    mock.perform(get(URI).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$._embedded.alignments.*", hasSize(2)))
        .andExpect(jsonPath("$._links.self.*", hasSize(1)))
        .andExpect(jsonPath("$.page.size", is(2)))
        .andExpect(jsonPath("$.page.totalElements", is(2)))
        .andExpect(jsonPath("$.page.totalPages", is(1)))
        .andExpect(jsonPath("$.page.number", is(0)));

  }

  @DisplayName("Display an alignment by name")
  @Test
  public void test_ControllerCanFindByName() throws Exception {
    when(service.findByName(anyString())).thenReturn(new Alignment(1, true, true, "Crusader"));

    mock.perform(
        get("/api/alignments/search?name=Crusader").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.name", is("Crusader")))
        .andExpect(jsonPath("$._links.*", hasSize(1)));
  }

  @DisplayName("Display an alignment by ID")
  @Test
  public void test_ControllerCanFindById() throws Exception {
    when(service.findById(1)).thenReturn(new Alignment(1, true, true, "Crusader"));

    mock.perform(get("/api/alignments/1").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.name", is("Crusader")))
        .andExpect(jsonPath("$._links.*", hasSize(1)));
  }

  @DisplayName("invalid ID return 404")
  @Test
  public void test_invalidIdGenerate404() throws Exception {
    when(service.findById(anyInt())).thenThrow(new EntityNotFoundException(1));

    mock.perform(get("/api/alignments/1").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.message", is("Couldn't find the entity of id #1")));
  }

  @DisplayName("invalid name return 404")
  @Test
  public void test_invalidNameGenerate404() throws Exception {
    when(service.findByName(anyString())).thenThrow(new EntityNotFoundException("Cthulhu"));

    mock.perform(get("/api/alignments/search?name=Cthulhu").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.message", is("Couldn't find the entity with name Cthulhu")));
  }

  @DisplayName("Controller provide totals")
  @Test
  public void test_ControllerWillFailWithInvalid() throws Exception {
    when(service.count()).thenReturn(1);

    mock.perform(get("/api/alignments/totals").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.total", is(1)));
  }


  @Nested
  @DisplayName("Post tests")
  class PostTest {


    @DisplayName("Invalid object return 400")
    @Test
    public void test_invalidPost() {
      var invalid = new Alignment(null, null, null, null);

      List.of(" ", "", "  ").stream().forEach(e -> {
        invalid.setName(e);
        try {
          mock.perform(post(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(invalid)))
              .andExpect(jsonPath("$.errors[0]", is("name : must not be null or empty")))
              .andExpect(status().isBadRequest());
        } catch (Exception e1) {
          e1.printStackTrace();
        }

      });
      verifyNoInteractions(service);
    }

    @DisplayName("Objects with ID results in 400")
    @Test
    public void test_invalidPostExistingAlignment() throws Exception {
      Alignment invalid = new Alignment(1, null, null, "test");
      mock.perform(post(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(invalid)))
          .andExpect(jsonPath("$.errors", containsInAnyOrder("id needs to be null")))
          .andExpect(status().isBadRequest());
      verifyNoInteractions(service);
    }

    @DisplayName("Valid Post results in 201")
    @Test
    public void test_validPostCreation() throws Exception {
      Alignment valid = new Alignment(null, null, null, "Test");
      when(service.save(any(Alignment.class))).thenReturn(new Alignment(2, null, null, "Test"));

      mock.perform(post(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(valid)))
          .andExpect(status().isCreated())
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(jsonPath("$.name", is("Test"))).andExpect(header().exists("Location"))
          .andExpect(redirectedUrl("http://localhost/api/alignments/2"));

      verify(service).save(any(Alignment.class));
      verifyNoMoreInteractions(service);

    }
  }

  @Nested
  @DisplayName("Put tests")
  class PutTest {

    @DisplayName("Valid Post results OK for update")
    @Test
    public void test_validPutUpdate() throws Exception {
      Alignment valid = new Alignment(2, null, null, "test");
      when(service.contains(anyInt())).thenReturn(true);
      when(service.update(any(Alignment.class)))
          .thenReturn(new Alignment(2, null, null, "Updated"));

      mock.perform(put(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(valid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isOk()).andExpect(jsonPath("$.lawful", nullValue()))
          .andExpect(jsonPath("$.good", nullValue()))
          .andExpect(jsonPath("$.name", not(valid.getName())))
          .andExpect(jsonPath("$._links.*", hasSize(2)));

      verify(service).update(any(Alignment.class));
      verify(service).contains(anyInt());
      verifyNoMoreInteractions(service);

    }

    @DisplayName("Valid Post results in CREATED for update without entity")
    @Test
    public void test_validPutCreate() throws Exception {
      Alignment valid = new Alignment(2, null, null, "test");
      when(service.contains(anyInt())).thenReturn(false);
      when(service.update(any(Alignment.class)))
          .thenReturn(new Alignment(5, null, null, "Updated"));

      mock.perform(put(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(valid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isCreated()).andExpect(jsonPath("$.lawful", nullValue()))
          .andExpect(jsonPath("$.good", nullValue())).andExpect(jsonPath("$.name", is("Updated")))
          .andExpect(header().exists("Location"))
          .andExpect(redirectedUrl("http://localhost/api/alignments/5"))
          .andExpect(jsonPath("$._links.*", hasSize(1)));

      verify(service).update(any(Alignment.class));
      verify(service).contains(anyInt());
      verifyNoMoreInteractions(service);

    }

    @DisplayName("invalid Put object results in BADREQUEST")
    @Test
    public void test_invalidPutCauses400() throws Exception {
      Alignment invalid = new Alignment(null, null, null, null);

      mock.perform(put(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(invalid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.errors[0]", is("name : must not be null or empty")));

      verifyNoInteractions(service);
      verifyNoMoreInteractions(service);

    }

    @DisplayName("update ignore ID if new is created")
    @Test
    public void update_ignoresId() throws Exception {
      Alignment valid = new Alignment(3, null, null, "Test");

      when(service.contains(anyInt())).thenReturn(false);
      when(service.update(any(Alignment.class))).thenReturn(new Alignment(5, null, null, "Test"));

      mock.perform(put(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(valid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isCreated()).andExpect(jsonPath("$.lawful", nullValue()))
          .andExpect(jsonPath("$.good", nullValue())).andExpect(jsonPath("$.name", is("Test")))
          .andExpect(header().exists("Location"))
          .andExpect(redirectedUrl("http://localhost/api/alignments/5"))
          .andExpect(jsonPath("$._links.*", hasSize(1)));

      verify(service).contains(anyInt());
      verify(service).update(any(Alignment.class));
      verifyNoMoreInteractions(service);
    }
  }
}
