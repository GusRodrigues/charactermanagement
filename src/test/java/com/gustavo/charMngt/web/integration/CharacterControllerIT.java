package com.gustavo.charMngt.web.integration;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import java.util.*;
import org.junit.jupiter.api.*;
import org.mockito.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.http.*;
import org.springframework.test.web.servlet.*;
import com.gustavo.charMngt.model.*;
import com.gustavo.charMngt.model.Character;
import com.gustavo.charMngt.service.*;
import com.gustavo.charMngt.service.exceptions.model.*;
import com.gustavo.charMngt.web.*;

@WebMvcTest(controllers = CharacterController.class)
public class CharacterControllerIT implements JsonWriter {

  private final static String URI = "/api/characters";

  @Autowired
  private MockMvc mock;

  @MockBean
  private CharacterService service;

  @AfterEach
  public void after() {
    reset(service);
  }

  private static final Character defaultChar() {
    Character character = new Character();
    character.setId(1);
    character.setAlignment(new Alignment(1, true, true, "Crusader"));
    character.setJob(new Job(1, "Paladin"));
    character.setSize(new Size(1, "Medium", 8));
    character.setRace(new Race(1, "Human"));
    character.setName("Dummy");
    character.setPlayable(true);
    character.setExperiencePoint(0);
    character.setGender(false);
    character.setAttributes(new Attribute(10, 10, 10, 10, 10, 10));
    return character;
  }

  @DisplayName("Generate a total count")
  @Test
  public void test_responseHasTotalCount() throws Exception {
    Mockito.when(service.count()).thenReturn(5);
    mock.perform(get(URI + "/totals").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andExpect((content().contentType(MediaType.APPLICATION_JSON)))
        .andExpect(jsonPath("$.total", is(5)));
  }

  @Nested
  @DisplayName("Get {id}")
  class Get {

    @DisplayName("Generates a valid character")
    @Test
    public void test_responseHasACharacter() throws Exception {
      Character playable = defaultChar();

      Mockito.when(service.findById(anyInt())).thenReturn(playable);

      mock.perform(get(URI + "/1").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk()).andExpect((content().contentType(MediaType.APPLICATION_JSON)))
          .andExpect(jsonPath("$.name", is("Dummy"))).andExpect(jsonPath("$.playable", is(true)))
          .andExpect(jsonPath("$.gender", is(false)))
          .andExpect(jsonPath("$.attributes.*", hasSize(6)))
          .andExpect(jsonPath("$.attributes.strength", is(10)))
          .andExpect(jsonPath("$.attributes.dexterity", is(10)))
          .andExpect(jsonPath("$.attributes.constitution", is(10)))
          .andExpect(jsonPath("$.attributes.intelligence", is(10)))
          .andExpect(jsonPath("$.attributes.wisdom", is(10)))
          .andExpect(jsonPath("$.attributes.charisma", is(10)));
    }

    @DisplayName("Shows the aligment of a char")
    @Test
    public void test_aCharacterHasAligment() throws Exception {
      Character playable = defaultChar();

      Mockito.when(service.findById(anyInt())).thenReturn(playable);

      mock.perform(get(URI + "/1/alignment").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk()).andExpect((content().contentType(MediaType.APPLICATION_JSON)))
          .andExpect(jsonPath("$.lawful", is(true))).andExpect(jsonPath("$.good", is(true)))
          .andExpect(jsonPath("$.name", is("Crusader")))
          .andExpect(jsonPath("$._links.*", hasSize(4)));
    }

    @DisplayName("Shows the class of a char")
    @Test
    public void test_aCharacterHasClass() throws Exception {
      Character playable = defaultChar();

      Mockito.when(service.findById(anyInt())).thenReturn(playable);

      mock.perform(get(URI + "/1/class").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk()).andExpect((content().contentType(MediaType.APPLICATION_JSON)))
          .andExpect(jsonPath("$.name", is("Paladin")))
          .andExpect(jsonPath("$._links.*", hasSize(4)));
    }

    @DisplayName("Shows the race of a char")
    @Test
    public void test_aCharacterHasRace() throws Exception {
      Character playable = defaultChar();

      Mockito.when(service.findById(anyInt())).thenReturn(playable);

      mock.perform(get(URI + "/1/race").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk()).andExpect((content().contentType(MediaType.APPLICATION_JSON)))
          .andExpect(jsonPath("$.race", is("Human"))).andExpect(jsonPath("$._links.*", hasSize(4)));
    }

    @DisplayName("Shows the size of a char")
    @Test
    public void test_aCharacterHasSize() throws Exception {
      Character playable = defaultChar();

      Mockito.when(service.findById(anyInt())).thenReturn(playable);

      mock.perform(get(URI + "/1/size").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk()).andExpect((content().contentType(MediaType.APPLICATION_JSON)))
          .andExpect(jsonPath("$.hitDice", is(8))).andExpect(jsonPath("$.name", is("Medium")))
          .andExpect(jsonPath("$._links.*", hasSize(4)));
    }

    @DisplayName("Invalid id return 404")
    @Test
    public void test_invalidIdCause404() throws Exception {
      when(service.findById(anyInt())).thenThrow(new EntityNotFoundException(1));

      mock.perform(get(URI + "/1").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isNotFound())
          .andExpect((content().contentType(MediaType.APPLICATION_JSON)))
          .andExpect(jsonPath("$.message", is("Couldn't find the entity of id #1")));
    }

    @DisplayName("Bad land page cause 400")
    @Test
    public void test_invalidIdCause400() throws Exception {
      mock.perform(get(URI + "/invalid").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isBadRequest());

      mock.perform(get(URI + "/as").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isBadRequest());
    }
  }

  @Nested
  @DisplayName("Post calls")
  class Post {
    @DisplayName("Valid char generate 201 and redirect")
    @Test
    public void test_postValidCharacter() throws Exception {
      var valid = defaultChar();
      valid.setId(null);
      when(service.save(any(Character.class))).thenReturn(defaultChar());
      mock.perform(post(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(valid)))
          .andExpect(status().isCreated()).andExpect(jsonPath("$._links.*", hasSize(1)))
          .andExpect(header().exists("Location"))
          .andExpect(redirectedUrl("http://localhost/api/characters/1"));

      verify(service).save(any(Character.class));
      verifyNoMoreInteractions(service);
    }

    @DisplayName("Invalid char generates 400 redirect")
    @Test
    public void test_postInvalidCharacter() throws Exception {
      var invalid = new Character();
      mock.perform(post(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(invalid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.errors.*",
              containsInAnyOrder("race : cannot be null", "gender : cannot be null",
                  "playable : cannot be null", "alignment : cannot be null", "job : cannot be null",
                  "name : must not be null or empty", "experiencePoint : cannot be null",
                  "size : cannot be null", "attributes : cannot be null")));

      verifyNoMoreInteractions(service);
    }

    @DisplayName("Valid char with ID generates 400 redirect with invalid attributes")
    @Test
    public void test_postValidCharacterWithIdCausesError() throws Exception {
      var invalid = defaultChar();
      invalid.setAttributes(null);

      mock.perform(post(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(invalid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isBadRequest()).andExpect(jsonPath("$.errors.*",
              // lazy load result in multiple errors...
              containsInAnyOrder("attributes : cannot be null")));

      verifyNoMoreInteractions(service);
    }
  }

  @Nested
  @DisplayName("Put tests")
  class Put {

    @DisplayName("Valid Put return a 200 OK")
    @Test
    public void test_validCauses200() throws Exception {
      var valid = defaultChar();
      when(service.contains(anyInt())).thenReturn(true);
      valid.setName("updated");
      when(service.update(any(Character.class))).thenReturn(valid);

      mock.perform(put(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(valid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isOk()).andExpect(jsonPath("$.name", is("updated")))
          .andExpect(jsonPath("$._links.*", hasSize(2)));

      verify(service).update(any(Character.class));
      verify(service).contains(anyInt());
      verifyNoMoreInteractions(service);
    }

    @DisplayName("Valid PUT causes a 201 when no entity existed before")
    @Test
    public void test_validCreationCauses201() throws Exception {
      var valid = defaultChar();
      valid.setName("created");
      valid.setId(5);

      when(service.contains(anyInt())).thenReturn(false);
      when(service.update(any(Character.class))).thenReturn(valid);

      mock.perform(put(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(valid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isCreated()).andExpect(jsonPath("$.name", is("created")))
          .andExpect(jsonPath("$._links.*", hasSize(1))).andExpect(header().exists("Location"))
          .andExpect(redirectedUrl("http://localhost/api/characters/5"));

      verify(service).update(any(Character.class));
      verify(service).contains(anyInt());
      verifyNoMoreInteractions(service);
    }

    @DisplayName("Invalid entity causes 400")
    @Test
    public void test_InvalidCharacterResultsIn400() throws Exception {
      var invalid = new Character();
      mock.perform(put(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(invalid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.errors.*",
              containsInAnyOrder("race : cannot be null", "gender : cannot be null",
                  "playable : cannot be null", "alignment : cannot be null", "job : cannot be null",
                  "name : must not be null or empty", "experiencePoint : cannot be null",
                  "size : cannot be null", "attributes : cannot be null")));

      verifyNoMoreInteractions(service);
    }

    @DisplayName("Invalid attributes causes 400")
    @Test
    public void test_InvalidCharAttributesResultsIn400() throws Exception {
      var invalid = defaultChar();
      invalid.setAttributes(new Attribute(-1, -1, -1, -1, -1, -1));

      mock.perform(put(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(invalid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.errors.*",
              containsInAnyOrder("attributes.wisdom : must be greater than or equal to 0",
                  "attributes.constitution : must be greater than or equal to 0",
                  "attributes.dexterity : must be greater than or equal to 0",
                  "attributes.charisma : must be greater than or equal to 0",
                  "attributes.intelligence : must be greater than or equal to 0",
                  "attributes.strength : must be greater than or equal to 0")));

      then(service).shouldHaveNoInteractions();
    }

  }

  @Nested
  @DisplayName("Deletion tests")
  class DeleteTest {

    @DisplayName("Can Delete valid entity")
    @Test
    public void test_validDeletion() throws Exception {
      var valid = defaultChar();
      given(service.findById(1)).willReturn(valid);
      willDoNothing().given(service).delete(any(Character.class));

      mock.perform(delete(URI + "/1").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk())
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(jsonPath("$._links.*", hasSize(2)))
          .andExpect(jsonPath("$._links.home.*", hasSize(1)))
          .andExpect(jsonPath("$.content", is("Deleted!")));

      then(service).should().findById(anyInt());
      then(service).should().delete(any(Character.class));
      then(service).shouldHaveNoMoreInteractions();
    }

    @DisplayName("Can't delete job that doesn't exists")
    @Test
    public void test_invalidDeletion() throws Exception {
      given(service.findById(anyInt())).willThrow(new EntityNotFoundException(1));


      mock.perform(delete(URI + "/1").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isNotFound())
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(jsonPath("$.message", is("Couldn't find the entity of id #1")));

      then(service).should().findById(anyInt());
      then(service).shouldHaveNoMoreInteractions();
    }
  }


  @Disabled
  @Nested
  @DisplayName("Search by race")
  class SearchByRace {
    @DisplayName("Search by race ID")
    @Test
    public void test_searchAllCharByRaceID() throws Exception {

      Mockito.when(service.findByRace(Optional.of(1), Optional.empty()))
          .thenReturn(List.of(defaultChar()));

      mock.perform(get(URI + "/race?id=1").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk()).andExpect((content().contentType(MediaType.APPLICATION_JSON)))
          .andExpect(jsonPath("$.characters.*", hasSize(1)))
          .andExpect(jsonPath("$.characters[0].id", is(1)))
          .andExpect(jsonPath("$.characters[0].race.id", is(1)))
          .andExpect(jsonPath("$.characters[0].race.race", is("Human")));
    }

    @DisplayName("Search by race name")
    @Test
    public void test_searchAllCharByRaceName() throws Exception {
      Mockito.when(service.findByRace(Optional.empty(), Optional.of("Human")))
          .thenReturn(List.of(defaultChar()));

      mock.perform(get(URI + "/race?name=Human").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk()).andExpect((content().contentType(MediaType.APPLICATION_JSON)))
          .andExpect(jsonPath("$.characters.*", hasSize(1)))
          .andExpect(jsonPath("$.characters[0].id", is(1)))
          .andExpect(jsonPath("$.characters[0].race.id", is(1)))
          .andExpect(jsonPath("$.characters[0].race.race", is("Human")));
    }

    @DisplayName("Search return empty list if no character has that race")
    @Test
    public void test_noCharacterWasFoundResultInEmptyList() throws Exception {
      Mockito.when(service.findByRace(Optional.empty(), Optional.of("Human")))
          .thenReturn(Collections.emptyList());
      Mockito.when(service.findByRace(Optional.of(1), Optional.empty()))
          .thenReturn(Collections.emptyList());

      mock.perform(get(URI + "/race?name=Human").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk()).andExpect((content().contentType(MediaType.APPLICATION_JSON)))
          .andExpect(jsonPath("$.characters.*", hasSize(0)));

      mock.perform(get(URI + "/race?id=1").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk()).andExpect((content().contentType(MediaType.APPLICATION_JSON)))
          .andExpect(jsonPath("$.characters.*", hasSize(0)));
    }

    @DisplayName("Malformed search causes 400")
    @Test
    public void test_MalformedSearchCause400() throws Exception {

      mock.perform(get(URI + "/race").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isBadRequest())
          .andExpect((content().contentType(MediaType.APPLICATION_JSON))).andExpect(jsonPath(
              "$.message", is("Invalid arguments provided to search for Character by race.")));
    }

  }

  @Disabled
  @Nested
  @DisplayName("Search by job")
  class SearchByJob {
    @DisplayName("Search by job ID")
    @Test
    public void test_searchAllCharByJobId() throws Exception {
      Mockito.when(service.findByJob(Optional.of(1), Optional.empty()))
          .thenReturn(List.of(defaultChar()));

      mock.perform(get(URI + "/class?id=1").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk()).andExpect((content().contentType(MediaType.APPLICATION_JSON)))
          .andExpect(jsonPath("$.characters.*", hasSize(1)))
          .andExpect(jsonPath("$.characters[0].id", is(1)))
          .andExpect(jsonPath("$.characters[0].job.id", is(1)))
          .andExpect(jsonPath("$.characters[0].job.name", is("Paladin")));
    }

    @DisplayName("Search by job name")
    @Test
    public void test_searchAllCharByName() throws Exception {
      Mockito.when(service.findByJob(Optional.empty(), Optional.of("Paladin")))
          .thenReturn(List.of(defaultChar()));

      mock.perform(get(URI + "/class?name=Paladin").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk()).andExpect((content().contentType(MediaType.APPLICATION_JSON)))
          .andExpect(jsonPath("$.characters.*", hasSize(1)))
          .andExpect(jsonPath("$.characters[0].id", is(1)))
          .andExpect(jsonPath("$.characters[0].job.id", is(1)))
          .andExpect(jsonPath("$.characters[0].job.name", is("Paladin")));
    }

    @DisplayName("Search return no character with the job")
    @Test
    public void test_noCharacterWasFoundResultInEmptyList() throws Exception {
      Mockito.when(service.findByJob(Optional.empty(), Optional.of("Paladin")))
          .thenReturn(Collections.emptyList());

      mock.perform(get(URI + "/class?name=Paladin").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk()).andExpect((content().contentType(MediaType.APPLICATION_JSON)))
          .andExpect(jsonPath("$.characters.*", hasSize(0)));
    }

    @DisplayName("Malformed search causes 400")
    @Test
    public void test_MalformedSearchCause400() throws Exception {

      mock.perform(get(URI + "/class").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isBadRequest())
          .andExpect((content().contentType(MediaType.APPLICATION_JSON))).andExpect(jsonPath(
              "$.message", is("Invalid arguments provided to search for Character by class.")));
    }

  }

  @Disabled
  @Nested
  @DisplayName("Search by size")
  class SearchBySize {
    @DisplayName("Search by size ID")
    @Test
    public void test_searchAllCharByJobId() throws Exception {
      Mockito.when(service.findBySize(Optional.of(1), Optional.empty(), Optional.empty()))
          .thenReturn(List.of(defaultChar()));

      mock.perform(get(URI + "/size?id=1").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk()).andExpect((content().contentType(MediaType.APPLICATION_JSON)))

          .andExpect(jsonPath("$.characters.*", hasSize(1)))
          .andExpect(jsonPath("$.characters[0].size.id", is(1)))
          .andExpect(jsonPath("$.characters[0].size.hitDice", is(8)))
          .andExpect(jsonPath("$.characters[0].size.name", is("Medium")));
    }

    @DisplayName("Search by size name")
    @Test
    public void test_searchAllCharByName() throws Exception {
      Mockito.when(service.findBySize(Optional.empty(), Optional.of("Medium"), Optional.empty()))
          .thenReturn(List.of(defaultChar()));

      mock.perform(get(URI + "/size?name=Medium").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk()).andExpect((content().contentType(MediaType.APPLICATION_JSON)))
          .andExpect(jsonPath("$.characters.*", hasSize(1)))
          .andExpect(jsonPath("$.characters[0].size.id", is(1)))
          .andExpect(jsonPath("$.characters[0].size.hitDice", is(8)))
          .andExpect(jsonPath("$.characters[0].size.name", is("Medium")));
    }

    @DisplayName("Search by size hit dice")
    @Test
    public void test_searchAllCharByHitDice() throws Exception {
      Mockito.when(service.findBySize(Optional.empty(), Optional.empty(), Optional.of(8)))
          .thenReturn(List.of(defaultChar()));

      mock.perform(get(URI + "/size?dice=8").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk()).andExpect((content().contentType(MediaType.APPLICATION_JSON)))
          .andExpect(jsonPath("$.characters.*", hasSize(1)))
          .andExpect(jsonPath("$.characters[0].size.id", is(1)))
          .andExpect(jsonPath("$.characters[0].size.hitDice", is(8)))
          .andExpect(jsonPath("$.characters[0].size.name", is("Medium")));
    }

    @DisplayName("Search by all size param")
    @Test
    public void test_searchAllParam() throws Exception {
      Mockito.when(service.findBySize(Optional.of(1), Optional.of("Medium"), Optional.of(8)))
          .thenReturn(List.of(defaultChar()));

      mock.perform(
          get(URI + "/size?id=1&name=Medium&dice=8").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk()).andExpect((content().contentType(MediaType.APPLICATION_JSON)))
          .andExpect(jsonPath("$.*", hasSize(1)))
          .andExpect(jsonPath("$.characters[0].size.id", is(1)))
          .andExpect(jsonPath("$.characters[0].size.hitDice", is(8)))
          .andExpect(jsonPath("$.characters[0].size.name", is("Medium")));
    }

    @DisplayName("Search return no character with the size")
    @Test
    public void test_noCharacterWasFoundResultInEmptyList() throws Exception {
      Mockito.when(service.findBySize(Optional.empty(), Optional.of("Weird"), Optional.empty()))
          .thenReturn(Collections.emptyList());

      mock.perform(get(URI + "/size?name=Weird").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk()).andExpect((content().contentType(MediaType.APPLICATION_JSON)))
          .andExpect(jsonPath("$.characters.*", hasSize(0)));
    }

    @DisplayName("Malformed search causes 400")
    @Test
    public void test_MalformedSearchCause400() throws Exception {
      mock.perform(get(URI + "/size").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isBadRequest())
          .andExpect((content().contentType(MediaType.APPLICATION_JSON))).andExpect(jsonPath(
              "$.message", is("Invalid arguments provided to search for Character by size.")));
    }
  }

  @Disabled
  @Nested
  @DisplayName("Search by Alignment")
  class SearchByAlignment {
    @DisplayName("Search by alignment ID")
    @Test
    public void test_searchAllCharByAlignmentId() throws Exception {
      Mockito.when(service.findByAlignment(Optional.of(1), Optional.empty()))
          .thenReturn(List.of(defaultChar()));

      mock.perform(get(URI + "/alignment?id=1").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk()).andExpect((content().contentType(MediaType.APPLICATION_JSON)))
          .andExpect(jsonPath("$.characters.*", hasSize(1)))
          .andExpect(jsonPath("$.characters[0].size.id", is(1)))
          .andExpect(jsonPath("$.characters[0].size.hitDice", is(8)))
          .andExpect(jsonPath("$.characters[0].size.name", is("Medium")));
    }

    @DisplayName("Search by alignment name")
    @Test
    public void test_searchAllCharByAlignmentName() throws Exception {
      Mockito.when(service.findByAlignment(Optional.empty(), Optional.of("Crusader")))
          .thenReturn(List.of(defaultChar()));

      mock.perform(get(URI + "/alignment?name=Crusader").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk()).andExpect((content().contentType(MediaType.APPLICATION_JSON)))
          .andExpect(jsonPath("$.characters.*", hasSize(1)))
          .andExpect(jsonPath("$.characters[0].size.id", is(1)))
          .andExpect(jsonPath("$.characters[0].size.hitDice", is(8)))
          .andExpect(jsonPath("$.characters[0].size.name", is("Medium")));
    }

    @DisplayName("Search by all size param")
    @Test
    public void test_searchAllParam() throws Exception {
      Mockito.when(service.findByAlignment(Optional.of(1), Optional.of("Crusader")))
          .thenReturn(List.of(defaultChar()));

      mock.perform(
          get(URI + "/alignment?id=1&name=Crusader").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk()).andExpect((content().contentType(MediaType.APPLICATION_JSON)))
          .andExpect(jsonPath("$.characters.*", hasSize(1)))
          .andExpect(jsonPath("$.characters[0].size.id", is(1)))
          .andExpect(jsonPath("$.characters[0].size.hitDice", is(8)))
          .andExpect(jsonPath("$.characters[0].size.name", is("Medium")));
    }

    @DisplayName("Search return no character with the size")
    @Test
    public void test_noCharacterWasFoundResultInEmptyList() throws Exception {
      Mockito.when(service.findByAlignment(Optional.of(1), Optional.empty()))
          .thenReturn(Collections.emptyList());

      mock.perform(get(URI + "/alignment?id=1").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk()).andDo(print())
          .andExpect((content().contentType(MediaType.APPLICATION_JSON)))
          .andExpect(jsonPath("$.characters.*", hasSize(0)));
    }

    @DisplayName("Malformed search causes 400")
    @Test
    public void test_MalformedSearchCause400() throws Exception {
      mock.perform(get(URI + "/alignment").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isBadRequest())
          .andExpect((content().contentType(MediaType.APPLICATION_JSON))).andExpect(jsonPath(
              "$.message", is("Invalid arguments provided to search for Character by alignment.")));
    }
  }
}
