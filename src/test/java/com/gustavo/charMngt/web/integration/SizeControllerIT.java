package com.gustavo.charMngt.web.integration;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import java.util.*;
import org.junit.jupiter.api.*;
import org.mockito.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.data.domain.*;
import org.springframework.http.*;
import org.springframework.test.web.servlet.*;
import com.gustavo.charMngt.model.*;
import com.gustavo.charMngt.service.*;
import com.gustavo.charMngt.service.exceptions.model.*;
import com.gustavo.charMngt.web.*;

@WebMvcTest(SizeController.class)
public class SizeControllerIT implements JsonWriter {
  private final static String URI = "/api/sizes";

  @Autowired
  private MockMvc mock;

  @MockBean
  private SizeService service;

  private PageRequest page = PageRequest.of(0, 20, Sort.by(Sort.Direction.ASC, "id"));

  @AfterEach
  public void after() {
    reset(service);
  }


  @DisplayName("Test /api/sizes")
  @Test
  public void test_allSizesEntryPoint() throws Exception {

    PageImpl<Size> sizesPage = new PageImpl<>(
        List.of(new Size(1, "Tiny", 4), new Size(2, "Small", 6), new Size(3, "Medium", 8)));

    when(service.findAll(page)).thenReturn(sizesPage);

    mock.perform(get(URI).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$._embedded.sizes.*", hasSize(3)))
        .andExpect(jsonPath("$._links.self.*", hasSize(1)))
        .andExpect(jsonPath("$.page.size", is(3)))
        .andExpect(jsonPath("$.page.totalElements", is(3)))
        .andExpect(jsonPath("$.page.totalPages", is(1)))
        .andExpect(jsonPath("$.page.number", is(0)));
    verify(service).findAll(page);
  }

  @DisplayName("Test /api/sizes/totals")
  @Test
  public void test_ProvideTheTotals() throws Exception {
    Mockito.when(service.count()).thenReturn(3);
    mock.perform(get(URI + "/totals").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.total", is(3)));
    verify(service).count();
  }

  @DisplayName("Test /api/sizes/{id}")
  @Test
  public void test_validIdGenerateValidObject() throws Exception {
    Size size = new Size(1, "Tiny", 4);
    Mockito.when(service.findById(anyInt())).thenReturn(size);

    mock.perform(get(URI + "/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.name", is("Tiny"))).andExpect(jsonPath("$.hitDice", is(4)))
        .andExpect(jsonPath("$._links.*", hasSize(1)));
    verify(service).findById(anyInt());
  }

  @DisplayName("invalid search return 404")
  @Test
  public void test_invalidIdGenerate404() throws Exception {
    Mockito.when(service.findById(anyInt())).thenThrow(new EntityNotFoundException(anyInt()));

    mock.perform(get(URI + "/1").contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
    verify(service).findById(anyInt());
  }

  @DisplayName("valid search by name")
  @Test
  public void test_validNameGenerateValidObject() throws Exception {
    Size size = new Size(1, "Tiny", 4);
    Mockito.when(service.findByName(anyString())).thenReturn(size);

    mock.perform(get(URI + "/search?name=Tiny").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.name", is("Tiny"))).andExpect(jsonPath("$.hitDice", is(4)));
    verify(service).findByName(anyString());
  }

  @DisplayName("invalid name return 404")
  @Test
  public void test_invalidNameGenerate404() throws Exception {
    Mockito.when(service.findByName(anyString())).thenThrow(new EntityNotFoundException("Cthulhu"));

    mock.perform(get(URI + "/search?name=Cthulhu").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound())
        .andExpect(jsonPath("$.message", is("Couldn't find the entity with name Cthulhu")));
    verify(service).findByName(anyString());
  }


  @DisplayName("malformed query returns 400")
  @Test
  public void test_malformedQueryCause400() throws Exception {

    mock.perform(get(URI + "/malformed").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
    verifyNoInteractions(service);
  }

  @Nested
  @DisplayName("Post tests")
  class PostTest {


    @DisplayName("Invalid object return 400")
    @Test
    public void test_invalidPost() {
      Size invalid = new Size(null, "names", 20);

      List.of(" ", "", "  ").stream().forEach(e -> {
        invalid.setName(e);
        try {
          mock.perform(post(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(invalid)))
              .andExpect(jsonPath("$.errors[0]", is("name : must not be null or empty")))
              .andExpect(status().isBadRequest());
        } catch (Exception e1) {
          e1.printStackTrace();
        }
        verifyNoInteractions(service);
      });
    }

    @DisplayName("Objects with ID results in 400")
    @Test
    public void test_invalidPostExistingAlignment() throws Exception {
      Size invalid = new Size(1, "test", 20);

      mock.perform(post(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(invalid)))
          .andExpect(jsonPath("$.errors[*]", containsInAnyOrder("id needs to be null")))
          .andExpect(status().isBadRequest());
    }

    @DisplayName("Objects is totally invalid results in 400")
    @Test
    public void test_invalidSizePost() throws Exception {
      Size invalid = new Size(1, null, null);
      mock.perform(post(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(invalid)))
          .andExpect(jsonPath("$.errors.*", hasSize(2)))
          .andExpect(jsonPath("$.errors[*]",
              containsInAnyOrder("name : must not be null or empty", "hitDice : need a value")))
          .andExpect(status().isBadRequest());
      verifyNoInteractions(service);
    }

    @DisplayName("Valid Post results in 201")
    @Test
    public void test_validPostCreation() throws Exception {
      Size valid = new Size(null, "test", 20);
      when(service.save(any(Size.class))).thenReturn(new Size(2, "test", 20));

      mock.perform(post(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(valid)))
          .andExpect(status().isCreated())
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(jsonPath("$.name", is("test"))).andExpect(jsonPath("$.hitDice", is(20)))
          .andExpect(header().exists("Location"))
          .andExpect(redirectedUrl("http://localhost/api/sizes/2"));

      verify(service).save(any(Size.class));
      verifyNoMoreInteractions(service);

    }
  }



  @Nested
  @DisplayName("Put tests")
  class PutTest {

    @DisplayName("Valid Post results OK for update")
    @Test
    public void test_validPutUpdate() throws Exception {
      Size valid = new Size(2, "test", 8);
      when(service.contains(anyInt())).thenReturn(true);
      when(service.update(any(Size.class))).thenReturn(new Size(2, "Updated", 8));

      mock.perform(put(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(valid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isOk()).andExpect(jsonPath("$.name", is("Updated")))
          .andExpect(jsonPath("$.hitDice", is(8)))
          .andExpect(jsonPath("$.name", not(valid.getName())))
          .andExpect(jsonPath("$._links.*", hasSize(2)))
          .andExpect(jsonPath("$._links.self.href", is("http://localhost/api/sizes/2")));

      verify(service).update(any(Size.class));
      verify(service).contains(anyInt());
      verifyNoMoreInteractions(service);

    }

    @DisplayName("Valid Post results in CREATED for update without entity")
    @Test
    public void test_validPutCreate() throws Exception {
      Size valid = new Size(2, "Updated", 8);
      when(service.contains(anyInt())).thenReturn(false);
      when(service.update(any(Size.class))).thenReturn(new Size(5, "Updated", 8));

      mock.perform(put(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(valid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isCreated()).andExpect(jsonPath("$.hitDice", is(8)))
          .andExpect(jsonPath("$.name", is("Updated"))).andExpect(header().exists("Location"))
          .andExpect(redirectedUrl("http://localhost/api/sizes/5"))
          .andExpect(jsonPath("$._links.*", hasSize(1)));

      verify(service).update(any(Size.class));
      verify(service).contains(anyInt());
      verifyNoMoreInteractions(service);

    }

    @DisplayName("invalid Put object results in BADREQUEST")
    @Test
    public void test_invalidPutCauses400() throws Exception {
      Size invalid = new Size(null, null, null);

      mock.perform(put(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(invalid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isBadRequest()).andExpect(jsonPath("$.errors.*",
              containsInAnyOrder("name : must not be null or empty", "hitDice : need a value")));

      verifyNoInteractions(service);
      verifyNoMoreInteractions(service);

    }

    @DisplayName("update ignore ID if new is created")
    @Test
    public void update_ignoresId() throws Exception {
      Size valid = new Size(3, "Test", 20);

      when(service.contains(anyInt())).thenReturn(false);
      when(service.update(any(Size.class))).thenReturn(new Size(5, "Test", 20));

      mock.perform(put(URI).contentType(MediaType.APPLICATION_JSON).content(asJson(valid)))
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(status().isCreated()).andExpect(jsonPath("$.name", is("Test")))
          .andExpect(jsonPath("$.hitDice", is(20))).andExpect(header().exists("Location"))
          .andExpect(redirectedUrl("http://localhost/api/sizes/5"))
          .andExpect(jsonPath("$._links.*", hasSize(1)));

      verify(service).contains(anyInt());
      verify(service).update(any(Size.class));
      verifyNoMoreInteractions(service);
    }
  }


  @Nested
  @DisplayName("Deletion tests")
  class DeleteTest {

    @DisplayName("Can Delete valid entity")
    @Test
    public void test_validDeletion() throws Exception {
      var valid = new Size(1, "Test", 8);
      given(service.findById(1)).willReturn(valid);
      willDoNothing().given(service).delete(any(Size.class));

      mock.perform(delete(URI + "/1").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk())
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(jsonPath("$._links.*", hasSize(2)))
          .andExpect(jsonPath("$._links.home.*", hasSize(1)))
          .andExpect(jsonPath("$.content", is("Deleted!")));

      then(service).should().findById(anyInt());
      then(service).should().delete(any(Size.class));
      then(service).shouldHaveNoMoreInteractions();
    }

    @DisplayName("Can't delete job that doesn't exists")
    @Test
    public void test_invalidDeletion() throws Exception {
      given(service.findById(anyInt())).willThrow(new EntityNotFoundException(1));


      mock.perform(delete(URI + "/1").contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isNotFound())
          .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
          .andExpect(jsonPath("$.message", is("Couldn't find the entity of id #1")));

      then(service).should().findById(anyInt());
      then(service).shouldHaveNoMoreInteractions();
    }
  }
}
