package com.gustavo.charMngt.web.integration;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.http.*;
import org.springframework.test.web.servlet.*;
import com.gustavo.charMngt.web.*;

@WebMvcTest(controllers = EntryPointController.class)
public class EntryPointIntegrationTest {

  @Autowired
  private MockMvc mock;

  @DisplayName("Entrypoint return links")
  @Test
  public void test_entryPont() throws Exception {
    mock.perform(get("/api").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$._links.*", hasSize(5)));
  }

  @DisplayName("Invalid path")
  @Test
  public void test_invalidEntry() throws Exception {
    mock.perform(get("/apis").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }
}
