package com.gustavo.charMngt.web.integration;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;

/**
 * Convert an entity <T> to JSON using Jackson Object Mapper
 * 
 * @author Gustavo
 *
 */
public interface JsonWriter {
  /**
   * Converts T to JSON
   * 
   * @param <T>
   * @param alignment to be converted
   * @return the JSON representation
   */
  default <T> String asJson(T convert) {
    try {
      var mapper = new ObjectMapper();
      mapper.disable(MapperFeature.USE_ANNOTATIONS);
      return mapper.writeValueAsString(convert);
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
  }
}
