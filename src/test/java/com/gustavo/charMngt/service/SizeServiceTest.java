package com.gustavo.charMngt.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import java.util.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.springframework.data.domain.*;
import com.gustavo.charMngt.model.*;
import com.gustavo.charMngt.repository.*;
import com.gustavo.charMngt.service.exceptions.model.*;

@ExtendWith(MockitoExtension.class)
public class SizeServiceTest {

  @Mock
  private SizeRepository repository;

  @InjectMocks
  private SizeService service;

  private PageRequest page = PageRequest.of(0, 20, Sort.by(Sort.Direction.ASC, "id"));

  private static List<Size> allSizes() {
    return List.of(new Size(1, "Tiny", 4), new Size(2, "Small", 6), new Size(3, "Medium", 8));
  }

  @DisplayName("Return all sizes known")
  @Test
  public void test_FindAllSize() {
    when(repository.findAll(page)).thenReturn(new PageImpl<>(allSizes()));

    Page<Size> response = service.findAll(page);
    assertTrue(response.getSize() == 3, "Size of the page is no match");
    int i = 0;
    for (Size size : response) {
      assertEquals(size.getId(), allSizes().get(i).getId());
      assertSame(size.getName(), allSizes().get(i).getName());
      i++;
    }
  }


  @DisplayName("Return a size by Id")
  @Test
  public void test_FindById() {
    when(repository.findById(anyInt())).thenReturn(Optional.of(allSizes().get(0)));

    Size result = service.findById(1);
    assertEquals(allSizes().get(0).getHitDice(), result.getHitDice());
    assertEquals(allSizes().get(0).getId(), result.getId());
    assertEquals(allSizes().get(0).getName(), result.getName());
  }

  @DisplayName("Invalid ID causes size not found exception to being thrown")
  @Test
  public void test_invalidId() {
    when(repository.findById(anyInt())).thenReturn(Optional.empty());
    assertThrows(EntityNotFoundException.class, () -> service.findById(1));
  }

  @DisplayName("Return a size by Name")
  @Test
  public void test_FindByName() {
    when(repository.findByNameIgnoreCase(anyString())).thenReturn(Optional.of(allSizes().get(0)));

    Size result = service.findByName("Tiny");

    verify(repository).findByNameIgnoreCase("Tiny");
    assertEquals(allSizes().get(0).getHitDice(), result.getHitDice());
    assertEquals(allSizes().get(0).getId(), result.getId());
    assertEquals(allSizes().get(0).getName(), result.getName());
  }

  @DisplayName("Invalid name causes Size not found exception to be thrown")
  @Test
  public void test_invalidName() {
    when(repository.findByNameIgnoreCase(anyString())).thenReturn(Optional.empty());

    assertThrows(EntityNotFoundException.class, () -> service.findByName("Test"));
    verify(repository).findByNameIgnoreCase("Test");
  }

  @DisplayName("Get the total of sizes known in the game")
  @Test
  public void test_ServiceCanCount() {
    when(repository.count()).thenReturn(2l);

    int result = service.count();

    assertEquals(2, result);
  }


  @DisplayName("Saves a valid Size")
  @Test
  public void test_saveEntity() {
    Size dummy = new Size(null, "Test", 20);
    Size saved = new Size(2, "Test", 20);

    when(repository.save(any(Size.class))).thenReturn(saved);
    Size result = service.save(dummy);
    verify(repository).save(dummy);
    assertTrue(Objects.equals(saved, result));
    assertNotNull(result.getId());
    assertEquals(result.getId(), 2);
  }


  @DisplayName("Updates an entity")
  @Test
  public void test_UpdateEntity() {
    Size dummy = new Size(2, "Test", 20);
    Size desired = new Size(2, "Dummy", 20);
    when(repository.existsById(anyInt())).thenReturn(true);
    when(repository.save(any(Size.class))).thenReturn(desired);
    Size response = service.update(desired);

    assertEquals(desired, response);
    assertNotEquals(dummy, response);
    assertTrue(service.contains(1));
    verify(repository).save(any(Size.class));
    verify(repository).existsById(anyInt());
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Calls to updates a non existing entity")
  @Test
  public void test_CreatesThroughPut() {
    Size save = new Size(null, "Test", 20);
    Size answer = new Size(3, "Test", 20);
    when(repository.save(any(Size.class))).thenReturn(answer);
    Size response = service.update(save);

    assertEquals(answer, response);
    assertEquals(save, response);
    assertNotNull(response.getId());
    verify(repository).save(any(Size.class));
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Check if contains return true for existing entity")
  @Test
  public void test_ExistsReturnsTrue() {
    when(repository.existsById(anyInt())).thenReturn(true);
    assertTrue(service.contains(1));
    verify(repository).existsById(anyInt());
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Check if contains return false for non existing entity")
  @Test
  public void test_ExistsReturnsFalse() {
    when(repository.existsById(anyInt())).thenReturn(false);
    assertFalse(service.contains(1));
    verify(repository).existsById(anyInt());
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Delete an entity")
  @Test
  public void test_delete() {
    service.delete(new Size(1, "Test", 8));

    verify(repository).delete(any(Size.class));
    verifyNoMoreInteractions(repository);
  }
}
