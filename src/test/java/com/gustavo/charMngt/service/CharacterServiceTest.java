package com.gustavo.charMngt.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import java.util.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import com.gustavo.charMngt.model.*;
import com.gustavo.charMngt.model.Character;
import com.gustavo.charMngt.repository.*;
import com.gustavo.charMngt.service.exceptions.*;
import com.gustavo.charMngt.service.exceptions.model.*;

@ExtendWith(MockitoExtension.class)
public class CharacterServiceTest {
  @Mock
  private CharacterRepository repository;
  @InjectMocks
  private CharacterService service;

  private static Character defaultChar() {
    Character character = new Character();
    character.setId(1);
    character.setAlignment(new Alignment(1, true, true, "Crusader"));
    character.setJob(new Job(1, "Paladin"));
    character.setSize(new Size(1, "Medium", 8));
    character.setRace(new Race(1, "Human"));
    character.setName("Dummy");
    character.setPlayable(true);
    character.setExperiencePoint(0);
    character.setGender(false);
    character.setAttributes(new Attribute(10, 10, 10, 10, 10, 10));
    return character;
  }

  @DisplayName("Service can count entities")
  @Test
  public void test_count() {
    Mockito.when(repository.count()).thenReturn(5l);
    int result = service.count();
    assertEquals(5, result);
  }

  @DisplayName("Service can retrieve a character")
  @Test
  public void test_ReturnCharacter() {
    Mockito.when(repository.findById(1)).thenReturn(Optional.of(defaultChar()));
    Character actual = service.findById(1);
    assertEquals(defaultChar().getId(), actual.getId());
    assertEquals(defaultChar().getName(), actual.getName());
  }

  @DisplayName("Service throws exception if character not found")
  @Test
  public void test_IfNotFoundCausesException() {
    Mockito.when(repository.findById(anyInt())).thenReturn(Optional.empty());
    assertThrows(EntityNotFoundException.class, () -> service.findById(1));
  }

  @Nested
  @DisplayName("PUT tests")
  class PutTest {

    @DisplayName("Updates an entity")
    @Test
    public void test_UpdateEntity() {
      var dummy = defaultChar();
      when(repository.existsById(anyInt())).thenReturn(true);

      when(repository.save(any(Character.class))).thenReturn(defaultChar());
      var response = service.update(dummy);

      assertEquals(dummy.getId(), response.getId());

      assertTrue(service.contains(1));

      verify(repository).save(any(Character.class));
      verify(repository).existsById(anyInt());
      verifyNoMoreInteractions(repository);
    }

    @DisplayName("Calls to updates a non existing entity")
    @Test
    public void test_CreatesThroughPut() {
      var save = defaultChar();
      var answer = defaultChar();
      answer.setId(5);

      when(repository.save(any(Character.class))).thenReturn(answer);
      var response = service.update(save);

      assertEquals(answer.getId(), response.getId());
      assertNotEquals(save.getId(), response.getId());

      verify(repository).save(any(Character.class));
      verifyNoMoreInteractions(repository);
    }

  }

  @DisplayName("Check if contains return true for existing entity")
  @Test
  public void test_ExistsReturnsTrue() {
    when(repository.existsById(anyInt())).thenReturn(true);
    assertTrue(service.contains(1));
    verify(repository).existsById(anyInt());
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Check if contains return false for non existing entity")
  @Test
  public void test_ExistsReturnsFalse() {
    when(repository.existsById(anyInt())).thenReturn(false);
    assertFalse(service.contains(1));
    verify(repository).existsById(anyInt());
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Delete an entity")
  @Test
  public void test_delete() {
    service.delete(defaultChar());

    verify(repository).delete(any(Character.class));
    verifyNoMoreInteractions(repository);
  }

  @Nested
  @DisplayName("Search by race")
  class SearchByRace {

    @DisplayName("Can search by the race id")
    @Test
    public void test_canSearchByRaceId() {
      Mockito.when(repository.findAllByRaceId(1)).thenReturn(List.of(defaultChar()));
      Mockito.when(repository.findAllByRaceId(2)).thenReturn(Collections.emptyList());
      List<Character> result = service.findByRace(Optional.of(1), Optional.empty());
      List<Character> emptyResult = service.findByRace(Optional.of(2), Optional.empty());
      assertTrue(result.size() == 1);
      assertFalse(result.isEmpty());
      assertTrue(emptyResult.isEmpty());
    }

    @DisplayName("Can search by the race name")
    @Test
    public void test_canSearchByRaceName() {
      Mockito.when(repository.findAllByRaceRace("Human")).thenReturn(List.of(defaultChar()));
      Mockito.when(repository.findAllByRaceRace("Test")).thenReturn(Collections.emptyList());
      List<Character> result = service.findByRace(Optional.empty(), Optional.of("Human"));
      List<Character> emptyResult = service.findByRace(Optional.empty(), Optional.of("Test"));
      assertTrue(result.size() == 1);
      assertFalse(result.isEmpty());
      assertTrue(emptyResult.isEmpty());
    }

    @DisplayName("Search favors id over name")
    @Test
    public void test_searchFavorsIdOverName() {
      Mockito.when(repository.findAllByRaceId(1)).thenReturn(List.of(defaultChar()));
      List<Character> result = service.findByRace(Optional.of(1), Optional.of("Human"));
      Mockito.verify(repository).findAllByRaceId(1);
      assertTrue(result.size() == 1);
      assertFalse(result.isEmpty());

    }

    @DisplayName("No char found with the race result in empty list")
    @Test
    public void test_searchResultInEmptyListIfNoMatch() {
      Mockito.when(repository.findAllByRaceId(1)).thenReturn(Collections.emptyList());
      List<Character> result = service.findByRace(Optional.of(1), Optional.empty());
      Mockito.verify(repository).findAllByRaceId(1);
      assertTrue(result.isEmpty());

      Mockito.when(repository.findAllByRaceRace("Empty")).thenReturn(Collections.emptyList());
      result = service.findByRace(Optional.empty(), Optional.of("Empty"));
      Mockito.verify(repository).findAllByRaceRace("Empty");
      assertTrue(result.isEmpty());
    }

    @DisplayName("Empty search cause invalidSearchException")
    @Test
    public void test_invalidRaceSearchCauseException() {
      assertThrows(InvalidSearchException.class,
          () -> service.findByRace(Optional.empty(), Optional.empty()));
    }
  }

  @Nested
  @DisplayName("Search by class/job")
  class SearchByJob {

    @DisplayName("Search by class id")
    @Test
    public void test_searchByClassId() {
      Mockito.when(repository.findAllByJobId(1)).thenReturn(List.of(defaultChar()));

      List<Character> result = service.findByJob(Optional.of(1), Optional.empty());
      Mockito.verify(repository).findAllByJobId(1);
      assertFalse(result.isEmpty());
      assertTrue(result.get(0).getId() == 1);
    }

    @DisplayName("Search by class name")
    @Test
    public void test_searchByClassName() {
      Mockito.when(repository.findAllByJobNameIgnoreCase(anyString()))
          .thenReturn(List.of(defaultChar()));

      List<Character> result = service.findByJob(Optional.empty(), Optional.of("Paladin"));
      Mockito.verify(repository).findAllByJobNameIgnoreCase("Paladin");
      assertFalse(result.isEmpty());
      assertTrue(result.get(0).getId() == 1);
      assertTrue(result.get(0).getJob().getName().equalsIgnoreCase("Paladin"));
    }

    @DisplayName("No char found with the class result in empty list")
    @Test
    public void test_searchResultInEmptyListIfNoMatch() {
      Mockito.when(repository.findAllByJobId(anyInt())).thenReturn(Collections.emptyList());
      List<Character> result = service.findByJob(Optional.of(1), Optional.empty());
      Mockito.verify(repository).findAllByJobId(1);
      assertTrue(result.isEmpty());

      Mockito.when(repository.findAllByJobNameIgnoreCase(anyString()))
          .thenReturn(Collections.emptyList());
      result = service.findByJob(Optional.empty(), Optional.of("Empty"));
      Mockito.verify(repository).findAllByJobNameIgnoreCase("Empty");
      assertTrue(result.isEmpty());
    }

    @DisplayName("Empty search cause invalidSearchException")
    @Test
    public void test_invalidRaceSearchCauseException() {
      assertThrows(InvalidSearchException.class,
          () -> service.findByJob(Optional.empty(), Optional.empty()));
    }
  }


  @Nested
  @DisplayName("Search by Size")
  class SearchBySize {

    @DisplayName("Search by size id")
    @Test
    public void test_searchByClassId() {
      Mockito.when(repository.findAllBySizeId(1)).thenReturn(List.of(defaultChar()));

      List<Character> result =
          service.findBySize(Optional.of(1), Optional.empty(), Optional.empty());
      Mockito.verify(repository).findAllBySizeId(1);
      assertFalse(result.isEmpty());
      assertTrue(result.get(0).getSize().getId() == 1);
    }

    @DisplayName("Search by size name")
    @Test
    public void test_searchBySizeName() {
      Mockito.when(repository.findAllBySizeNameIgnoreCase(anyString()))
          .thenReturn(List.of(defaultChar()));

      List<Character> result =
          service.findBySize(Optional.empty(), Optional.of("Medium"), Optional.empty());
      Mockito.verify(repository).findAllBySizeNameIgnoreCase(anyString());
      assertFalse(result.isEmpty());
      assertTrue(result.get(0).getSize().getId() == 1);
      assertTrue(result.get(0).getSize().getName().equalsIgnoreCase("Medium"));
    }

    @DisplayName("Search by size dice")
    @Test
    public void test_searchBySizeDice() {
      Mockito.when(repository.findAllBySizeHitDice(8)).thenReturn(List.of(defaultChar()));

      List<Character> result =
          service.findBySize(Optional.empty(), Optional.empty(), Optional.of(8));
      Mockito.verify(repository).findAllBySizeHitDice(8);
      assertFalse(result.isEmpty());
      assertTrue(result.get(0).getSize().getId() == 1);
      assertTrue(result.get(0).getSize().getHitDice().equals(8));
    }

    @DisplayName("No char found with the class result in empty list")
    @Test
    public void test_searchResultInEmptyListIfNoMatch() {
      Mockito.when(repository.findAllBySizeId(1)).thenReturn(Collections.emptyList());
      List<Character> result =
          service.findBySize(Optional.of(1), Optional.empty(), Optional.empty());
      Mockito.verify(repository).findAllBySizeId(1);
      assertTrue(result.isEmpty());

      Mockito.when(repository.findAllBySizeNameIgnoreCase(anyString()))
          .thenReturn(Collections.emptyList());
      result = service.findBySize(Optional.empty(), Optional.of("Empty"), Optional.empty());
      Mockito.verify(repository).findAllBySizeNameIgnoreCase(anyString());
      assertTrue(result.isEmpty());


      Mockito.when(repository.findAllBySizeHitDice(8)).thenReturn(Collections.emptyList());
      result = service.findBySize(Optional.empty(), Optional.empty(), Optional.of(8));
      Mockito.verify(repository).findAllBySizeNameIgnoreCase(anyString());
      assertTrue(result.isEmpty());
    }

    @DisplayName("Search favors id over other")
    @Test
    public void test_searchFavorsId() {
      Mockito.when(repository.findAllBySizeId(1)).thenReturn(List.of(defaultChar()));

      List<Character> result =
          service.findBySize(Optional.of(1), Optional.of("Dummy"), Optional.of(8));
      Mockito.verify(repository).findAllBySizeId(1);
      assertFalse(result.isEmpty());
      assertTrue(result.get(0).getSize().getId() == 1);

      result = service.findBySize(Optional.of(1), Optional.empty(), Optional.of(8));
      Mockito.verify(repository, times(2)).findAllBySizeId(1);
      assertFalse(result.isEmpty());
      assertTrue(result.get(0).getSize().getId() == 1);
    }

    @DisplayName("Search favors name over hit dice")
    @Test
    public void test_searchFavorsNameOverHitDice() {
      Mockito.when(repository.findAllBySizeNameIgnoreCase(anyString()))
          .thenReturn(List.of(defaultChar()));

      List<Character> result =
          service.findBySize(Optional.empty(), Optional.of("Dummy"), Optional.of(8));
      Mockito.verify(repository).findAllBySizeNameIgnoreCase(anyString());
      assertFalse(result.isEmpty());
      assertTrue(result.get(0).getSize().getId() == 1);
      assertTrue(result.get(0).getSize().getName().equalsIgnoreCase("Medium"));
    }

    @DisplayName("Empty search cause invalidSearchException")
    @Test
    public void test_invalidRaceSearchCauseException() {
      assertThrows(InvalidSearchException.class,
          () -> service.findBySize(Optional.empty(), Optional.empty(), Optional.empty()));
    }
  }

  @Nested
  @DisplayName("Search by Alignment")
  class SearchByAlignment {

    @DisplayName("Search by Alignment ID")
    @Test
    public void test_searchByAlignmentId() {
      Mockito.when(repository.findAllByAlignmentId(1)).thenReturn(List.of(defaultChar()));

      List<Character> result = service.findByAlignment(Optional.of(1), Optional.empty());
      Mockito.verify(repository).findAllByAlignmentId(1);
      assertFalse(result.isEmpty());
      assertTrue(result.get(0).getAlignment().getId() == 1);
    }

    @DisplayName("Search by alignment name")
    @Test
    public void test_searchByAlignmentName() {
      Mockito.when(repository.findAllByAlignmentNameIgnoreCase(anyString()))
          .thenReturn(List.of(defaultChar()));

      List<Character> result = service.findByAlignment(Optional.empty(), Optional.of("Crusader"));
      Mockito.verify(repository).findAllByAlignmentNameIgnoreCase(anyString());
      assertFalse(result.isEmpty());
      assertTrue(result.get(0).getAlignment().getId() == 1);
      assertTrue(result.get(0).getAlignment().getName().equalsIgnoreCase("Crusader"));
    }

    @DisplayName("No char found with the alignment result in empty list")
    @Test
    public void test_searchResultInEmptyListIfNoMatch() {
      Mockito.when(repository.findAllByAlignmentId(anyInt())).thenReturn(Collections.emptyList());
      List<Character> result = service.findByAlignment(Optional.of(1), Optional.empty());
      Mockito.verify(repository).findAllByAlignmentId(anyInt());
      assertTrue(result.isEmpty());

      Mockito.when(repository.findAllByAlignmentNameIgnoreCase(anyString()))
          .thenReturn(Collections.emptyList());
      result = service.findByAlignment(Optional.empty(), Optional.of("Empty"));
      Mockito.verify(repository).findAllByAlignmentNameIgnoreCase(anyString());
      assertTrue(result.isEmpty());
    }

    @DisplayName("Search favors id over other")
    @Test
    public void test_searchFavorsId() {
      Mockito.when(repository.findAllByAlignmentId(anyInt())).thenReturn(List.of(defaultChar()));

      List<Character> result = service.findByAlignment(Optional.of(1), Optional.of("Dummy"));
      Mockito.verify(repository).findAllByAlignmentId(anyInt());
      assertFalse(result.isEmpty());
      assertTrue(result.get(0).getAlignment().getId() == 1);

      result = service.findByAlignment(Optional.of(1), Optional.empty());
      Mockito.verify(repository, times(2)).findAllByAlignmentId(anyInt());
      assertFalse(result.isEmpty());
      assertTrue(result.get(0).getSize().getId() == 1);
    }

    @DisplayName("Empty search cause invalidSearchException")
    @Test
    public void test_invalidRaceSearchCauseException() {
      assertThrows(InvalidSearchException.class,
          () -> service.findByAlignment(Optional.empty(), Optional.empty()));
    }
  }

}
