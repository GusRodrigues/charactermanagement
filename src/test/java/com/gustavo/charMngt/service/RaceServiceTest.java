package com.gustavo.charMngt.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import java.util.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.springframework.data.domain.*;
import com.gustavo.charMngt.model.*;
import com.gustavo.charMngt.repository.*;
import com.gustavo.charMngt.service.exceptions.model.*;

@ExtendWith(MockitoExtension.class)
public class RaceServiceTest {
  @Mock
  private RaceRepository repository;

  @InjectMocks
  private RaceService service;

  private PageRequest page = PageRequest.of(0, 20, Sort.by(Sort.Direction.ASC, "id"));

  /**
   * A list with all the Races
   * 
   * @return
   */
  public static List<Race> allRacesList() {
    return List.of(new Race(1, "Dwarf"), new Race(2, "Elve"), new Race(3, "Half-Elf"),
        new Race(4, "Half-Orc"), new Race(5, "Human"), new Race(6, "Hobbit"), new Race(7, "Orc"));
  }

  @DisplayName("Return the list")
  @Test
  public void test_listOfRaces() {
    when(repository.findAll(page)).thenReturn(new PageImpl<>(allRacesList()));
    List<Race> races = service.findAll(page).toList();
    assertTrue(races.size() == 7, "Size does not match");
  }

  @DisplayName("Valid ID returns Race")
  @Test
  public void test_RaceIdIsValid() {
    when(repository.findById(anyInt())).thenReturn(Optional.of(allRacesList().get(0)));
    Race test = service.findById(1);
    verify(repository).findById(1);
    assertNotNull(test, "race is empty");
  }

  @DisplayName("Invalid ID Throws RaceNotFoundException")
  @Test
  public void test_RaceIdIsInvalid() {
    when(repository.findById(anyInt())).thenThrow(new EntityNotFoundException(10));
    assertThrows(EntityNotFoundException.class, () -> {
      service.findById(10);
    }, "Race should be invalid");
    verify(repository).findById(10);
  }

  @DisplayName("Return the race if race name is valid")
  @Test
  public void test_FindRaceByValidName() {
    when(repository.findByRaceIgnoreCase(anyString()))
        .thenReturn(Optional.of(new Race(6, "Hobbit")));
    Race test = service.findByName("Hobbit");
    verify(repository).findByRaceIgnoreCase("Hobbit");
    assertEquals(6, test.getId());
    assertTrue(test.getRace().equalsIgnoreCase("Hobbit"));
  }

  @DisplayName("Inexistent Race Throws RaceNotFoundException")
  @Test
  public void test_FindRaceByInvalidName() {
    when(repository.findByRaceIgnoreCase(anyString()))
        .thenThrow(new EntityNotFoundException("Ranger"));
    assertThrows(EntityNotFoundException.class, () -> service.findByName("Ranger"),
        "Race should be invalid");
    verify(repository).findByRaceIgnoreCase("Ranger");
  }

  @DisplayName("Counts the total number of races stored")
  @Test
  public void test_RaceCount() {
    when(repository.count()).thenReturn(5l);
    int totalRaces = service.count();
    assertEquals(5, totalRaces);
  }

  @DisplayName("Saves a valid Race")
  @Test
  public void test_saveEntity() {
    Race dummy = new Race(null, "Test");
    Race expected = new Race(2, "Test");
    when(repository.save(any(Race.class))).thenReturn(expected);
    Race result = service.save(dummy);
    verify(repository).save(dummy);
    assertTrue(Objects.equals(expected, result));
    assertNotNull(result.getId());
    assertEquals(result.getId(), 2);
  }


  @DisplayName("Updates an entity")
  @Test
  public void test_UpdateEntity() {
    Race dummy = new Race(2, "Test");
    Race desired = new Race(2, "Dummy");
    when(repository.existsById(anyInt())).thenReturn(true);
    when(repository.save(any(Race.class))).thenReturn(desired);
    Race response = service.update(desired);

    assertEquals(desired, response);
    assertNotEquals(dummy, response);
    assertTrue(service.contains(1));
    verify(repository).save(any(Race.class));
    verify(repository).existsById(anyInt());
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Calls to updates a non existing entity")
  @Test
  public void test_CreatesThroughPut() {
    Race save = new Race(null, "Test");
    Race answer = new Race(3, "Test");
    when(repository.save(any(Race.class))).thenReturn(answer);
    Race response = service.update(save);

    assertEquals(answer, response);
    assertEquals(save, response);
    assertNotNull(response.getId());
    verify(repository).save(any(Race.class));
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Check if contains return true for existing entity")
  @Test
  public void test_ExistsReturnsTrue() {
    when(repository.existsById(anyInt())).thenReturn(true);
    assertTrue(service.contains(1));
    verify(repository).existsById(anyInt());
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Check if contains return false for non existing entity")
  @Test
  public void test_ExistsReturnsFalse() {
    when(repository.existsById(anyInt())).thenReturn(false);
    assertFalse(service.contains(1));
    verify(repository).existsById(anyInt());
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Delete an entity")
  @Test
  public void test_delete() {
    service.delete(new Race(3, "Test"));

    verify(repository).delete(any(Race.class));
    verifyNoMoreInteractions(repository);
  }
}
