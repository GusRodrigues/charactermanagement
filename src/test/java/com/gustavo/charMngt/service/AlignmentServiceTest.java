package com.gustavo.charMngt.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import java.util.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.springframework.data.domain.*;
import com.gustavo.charMngt.model.*;
import com.gustavo.charMngt.repository.*;
import com.gustavo.charMngt.service.exceptions.model.*;

@ExtendWith(MockitoExtension.class)
public class AlignmentServiceTest {

  private PageRequest page = PageRequest.of(0, 20, Sort.by(Sort.Direction.ASC, "id"));
  @Mock
  private AlignmentRepository repository;
  @InjectMocks
  private AlignmentService service;

  @DisplayName("Returns a list with all alignments")
  @Test
  public void test_ReturnsAListWithAllAlighments() {
    when(repository.findAll(page))
        .thenReturn(new PageImpl<Alignment>(List.of(new Alignment(1, true, true, "Test"),
            new Alignment(2, true, null, "Test"), new Alignment(3, true, false, "Test"))));
    when(repository.count()).thenReturn(3l);
    List<Alignment> result = service.findAll(page).toList();
    assertTrue(result.size() == 3);
    assertTrue(result.size() == service.count());
    verify(repository).count();
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Retrieve a neutral alignment")
  @Test
  public void testNeutralAlignmentRetrieval() {
    when(repository.findByNameIgnoreCase(anyString()))
        .thenReturn(Optional.of(new Alignment(1, null, null, "Neutral")));
    Alignment result = service.findByName("Neutral");

    verify(repository).findByNameIgnoreCase("Neutral");
    assertEquals("Neutral", result.getName());
    assertTrue(Optional.ofNullable(result.getGood()).isEmpty());
    assertTrue(Optional.ofNullable(result.getLawful()).isEmpty());
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Retrieve by Id")
  @Test
  public void test_AlignmentById() {
    when(repository.findById(anyInt()))
        .thenReturn(Optional.of(new Alignment(1, true, true, "Crusader")));

    Alignment result = service.findById(1);

    verify(repository).findById(1);
    assertEquals("Crusader", result.getName());
    assertTrue(Optional.of(result.getGood()).get().booleanValue());
    assertTrue(Optional.of(result.getLawful()).get().booleanValue());
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Can count the total of alignments")
  @Test
  public void test_CanCountTheTotalOfAlignments() {
    when(repository.count()).thenReturn(9l);
    int result = service.count();
    assertTrue(result == 9);
    verify(repository).count();
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Invalid ID Throw exception")
  @Test
  public void test_InvalidIdCausesException() {
    when(repository.findById(anyInt())).thenThrow(EntityNotFoundException.class);
    assertThrows(EntityNotFoundException.class, () -> service.findById(-1));
    verify(repository).findById(anyInt());
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Invalid Name Throw exception")
  @Test
  public void test_InvalidNameCausesException() {
    when(repository.findByNameIgnoreCase(anyString())).thenThrow(EntityNotFoundException.class);
    assertThrows(EntityNotFoundException.class, () -> service.findByName(""));
    verify(repository).findByNameIgnoreCase("");
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Saves a valid alignment")
  @Test
  public void test_saveEntity() {
    Alignment dummy = new Alignment(null, null, null, "Test");
    Alignment expected = new Alignment(2, null, null, "Test");
    when(repository.save(any(Alignment.class))).thenReturn(expected);
    Alignment result = service.save(dummy);
    verify(repository).save(any(Alignment.class));
    assertTrue(Objects.equals(expected, result));
    assertNotNull(result.getId());
    assertEquals(result.getId(), 2);
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Updates an entity")
  @Test
  public void test_UpdateEntity() {
    Alignment expected = new Alignment(2, null, null, "Test");
    when(repository.save(any(Alignment.class))).thenReturn(expected);
    Alignment response = service.update(expected);

    assertEquals(expected, response);
    verify(repository).save(any(Alignment.class));
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Calls to updates a non existing entity")
  @Test
  public void test_CreatesThroughPut() {
    Alignment expected = new Alignment(null, null, null, "Test");
    when(repository.save(any(Alignment.class))).thenReturn(expected);
    Alignment response = service.update(expected);

    assertEquals(expected, response);
    verify(repository).save(any(Alignment.class));
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Check if contains return true for existing entity")
  @Test
  public void test_ExistsReturnsTrue() {
    when(repository.existsById(anyInt())).thenReturn(true);
    assertTrue(service.contains(1));
    verify(repository).existsById(anyInt());
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Check if contains return false for non existing entity")
  @Test
  public void test_ExistsReturnsFalse() {
    when(repository.existsById(anyInt())).thenReturn(false);
    assertFalse(service.contains(1));
    verify(repository).existsById(anyInt());
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Delete an entity")
  @Test
  public void test_delete() {
    service.delete(new Alignment(1, null, null, "Test"));

    verify(repository).delete(any(Alignment.class));
    verifyNoMoreInteractions(repository);
  }
}
