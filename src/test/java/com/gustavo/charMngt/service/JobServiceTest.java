package com.gustavo.charMngt.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.*;
import java.util.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.springframework.data.domain.*;
import com.gustavo.charMngt.model.*;
import com.gustavo.charMngt.repository.*;
import com.gustavo.charMngt.service.exceptions.model.*;

@ExtendWith(MockitoExtension.class)
public class JobServiceTest {
  @Mock
  private JobRepository repository;
  @InjectMocks
  private JobService service;

  private PageRequest page = PageRequest.of(0, 20, Sort.by(Sort.Direction.ASC, "id"));

  private static List<Job> allJobs() {
    return List.of(new Job(1, "Barbarian"), new Job(2, "Fighter"), new Job(3, "Monk"),
        new Job(4, "Ranger"));
  }

  @DisplayName("Test the return of a list of jobs")
  @Test
  public void test_JobsList() {
    when(repository.findAll(page)).thenReturn(new PageImpl<>(allJobs()));

    List<Job> list = service.findAll(page).toList();
    assertTrue(list.size() == 4, "contains 4 jobs");
  }

  @DisplayName("Test valid id return of a job")
  @Test
  public void test_IdReturnJob() {
    Job expected = new Job(2, "Fighter");
    when(repository.findById(anyInt())).thenReturn(Optional.of(expected));
    Job actual = service.findById(1);
    assertEquals(expected.getId(), actual.getId());
    assertEquals(expected.getName(), actual.getName());
  }

  @DisplayName("Test valid name return a Job")
  @Test
  public void test_NameReturnsAJob() {
    Job expected = new Job(1, "Barbarian");
    when(repository.findByNameIgnoreCase(anyString())).thenReturn(Optional.of(expected));
    Job actual = service.findByName("Barbarian");
    verify(repository).findByNameIgnoreCase("Barbarian");
    assertEquals(expected.getId(), actual.getId());
    assertEquals(expected.getName(), actual.getName());
  }

  @DisplayName("Invalid ID cause exception to be thrown")
  @Test
  public void test_InvalidIdCauseException() {
    when(repository.findById(anyInt())).thenReturn(Optional.empty());
    assertThrows(EntityNotFoundException.class, () -> {
      service.findById(1);
    }, "Should have thrown an JobNotFoundException");
    verify(repository).findById(1);
  }

  @DisplayName("Invalid class name cause exception to be thrown")
  @Test
  public void test_InvalidJobNameCauseException() {
    when(repository.findByNameIgnoreCase(anyString())).thenReturn(Optional.empty());
    assertThrows(EntityNotFoundException.class, () -> {
      service.findByName("Barb");
    }, "Should have thrown an EntityNotFoundException");
    verify(repository).findByNameIgnoreCase("Barb");
  }

  @DisplayName("Can provide the total classes known in the game")
  @Test
  public void test_CountTotalClasses() {
    when(repository.count()).thenReturn(5l);
    int totals = service.count();
    assertEquals(5, totals);
  }

  @DisplayName("Saves a valid class")
  @Test
  public void test_saveEntity() {
    Job dummy = new Job(null, "Test");
    Job expected = new Job(2, "Test");
    when(repository.save(any(Job.class))).thenReturn(expected);
    Job result = service.save(dummy);
    verify(repository).save(dummy);
    assertTrue(Objects.equals(expected, result));
    assertNotNull(result.getId());
    assertEquals(result.getId(), 2);
  }


  @DisplayName("Updates an entity")
  @Test
  public void test_UpdateEntity() {
    Job dummy = new Job(2, "Test");
    Job desired = new Job(2, "Dummy");
    when(repository.existsById(anyInt())).thenReturn(true);
    when(repository.save(any(Job.class))).thenReturn(desired);
    Job response = service.update(desired);

    assertEquals(desired, response);
    assertNotEquals(dummy, response);
    assertTrue(service.contains(1));
    verify(repository).save(any(Job.class));
    verify(repository).existsById(anyInt());
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Calls to updates a non existing entity")
  @Test
  public void test_CreatesThroughPut() {
    Job save = new Job(null, "Test");
    Job answer = new Job(3, "Test");
    when(repository.save(any(Job.class))).thenReturn(answer);
    Job response = service.update(save);

    assertEquals(answer, response);
    assertEquals(save, response);
    assertNotNull(response.getId());
    verify(repository).save(any(Job.class));
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Check if contains return true for existing entity")
  @Test
  public void test_ExistsReturnsTrue() {
    when(repository.existsById(anyInt())).thenReturn(true);
    assertTrue(service.contains(1));
    verify(repository).existsById(anyInt());
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Check if contains return false for non existing entity")
  @Test
  public void test_ExistsReturnsFalse() {
    when(repository.existsById(anyInt())).thenReturn(false);
    assertFalse(service.contains(1));
    verify(repository).existsById(anyInt());
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Delete an entity")
  @Test
  public void test_delete() {
    service.delete(new Job(3, "Test"));

    verify(repository).delete(any(Job.class));
    verifyNoMoreInteractions(repository);
  }

  @DisplayName("Test void")
  @Test
  public void test_voidTest() {
    doThrow(IllegalArgumentException.class).when(repository).delete(null);
    assertThrows(IllegalArgumentException.class, () -> service.delete(null), "Must fail on null");
    then(repository).should(times(1)).delete(null);;

    verifyNoMoreInteractions(repository);
  }
}
